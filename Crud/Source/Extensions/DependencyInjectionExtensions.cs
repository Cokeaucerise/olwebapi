using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using OLWebApi.Core.Server;
using OLWebApi.Crud.Controller;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Interactor;
using OLWebApi.Crud.Repository;

namespace OLWebApi.Crud.Server.Extensions
{
	[ExcludeFromCodeCoverage]
	public static class DependencyInjection
	{
		public static void AddScopedCrudApiGateway(this IServiceCollection services, Type implementationGateway)
		{
			services.AddScoped(typeof(ICrudApiGateway<>), implementationGateway);
		}

		public static IServiceCollection AddCrudWebApi(this IServiceCollection services)
		{
			services.AddWebApi();

			services.TryAddScoped(typeof(ICrudApiController<>), typeof(CrudApiController<>));
			services.TryAddScoped(typeof(ICrudApiInteractor<>), typeof(CrudApiInteractor<>));
			services.TryAddScoped(typeof(ICrudApiRepository<>), typeof(CrudApiRepository<>));

			return services;
		}
	}
}