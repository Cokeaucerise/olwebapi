using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Crud.Repository
{
	public interface ICrudApiRepository<T> : IApiRepository<T>,
	IReadApiRepository<T>,
	ICreateApiRepository<T>,
	IUpdateApiRepository<T>,
	IDeleteApiRepository<T>
	where T : IApiRecord
	{ }

	public interface IReadApiRepository<T> : IApiRepository<T> where T : IApiRecord
	{
		Task<T> Get(string id);
	}

	public interface ICreateApiRepository<T> : IApiRepository<T> where T : IApiRecord
	{
		IAsyncEnumerable<T> Create(IEnumerable<T> record);
	}

	public interface IUpdateApiRepository<T> : IApiRepository<T> where T : IApiRecord
	{
		IAsyncEnumerable<T> Update(Updates<T> updates);
	}

	public interface IDeleteApiRepository<T> : IApiRepository<T> where T : IApiRecord
	{
		Task<bool> Delete(IEnumerable<string> ids);
	}
}