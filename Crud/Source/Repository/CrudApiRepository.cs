using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway;

namespace OLWebApi.Crud.Repository
{
	public class CrudApiRepository<T> : ApiRepository<T>, ICrudApiRepository<T>
	where T : IApiRecord
	{
		protected new ICrudApiGateway<T> Gateway => base.Gateway as ICrudApiGateway<T>;

		public CrudApiRepository(ICrudApiGateway<T> gateway) : base(gateway)
		{ }

		public Task<T> Get(string id)
		{
			return Gateway.Get(id);
		}

		public virtual IAsyncEnumerable<T> Create(IEnumerable<T> records)
		{
			return Gateway.Create(records) ?? AsyncEnumerable.Empty<T>();
		}

		public virtual IAsyncEnumerable<T> Update(Updates<T> updates)
		{
			return Gateway.Update(updates) ?? AsyncEnumerable.Empty<T>();
		}

		public virtual Task<bool> Delete(IEnumerable<string> ids)
		{
			return Gateway.Delete(ids);
		}
	}
}
