using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Crud.Gateway
{
	public interface ICrudApiGateway<T> : IApiGateway<T>,
		IReadApiGateway<T>,
		ICreateApiGateway<T>,
		IUpdateApiGateway<T>,
		IDeleteApiGateway<T>
	where T : IApiRecord
	{ }

	public interface IReadApiGateway<T> where T : IApiRecord
	{
		Task<T> Get(string id);
	}
	public interface ICreateApiGateway<T>
	where T : IApiRecord
	{
		IAsyncEnumerable<T> Create(IEnumerable<T> record);
	}

	public interface IUpdateApiGateway<T>
	where T : IApiRecord
	{
		IAsyncEnumerable<T> Update(Updates<T> updates);
	}

	public interface IDeleteApiGateway<T>
	where T : IApiRecord
	{
		Task<bool> Delete(IEnumerable<string> ids);
	}
}