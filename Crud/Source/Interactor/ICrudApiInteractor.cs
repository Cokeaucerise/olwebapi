using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Crud.Interactor
{
	public interface ICrudApiInteractor<T> : IApiInteractor<T>,
		IReadApiInteractor<T>,
		ICreateApiInteractor<T>,
		IUpdateApiInteractor<T>,
		IDeleteApiInteractor<T>
	where T : IApiRecord
	{ }

	public interface IReadApiInteractor<T> where T : IApiRecord
	{
		Task<Get<T>> Get(string id);
	}

	public interface ICreateApiInteractor<T> where T : IApiRecord
	{
		Task<Many<T>> Create(IEnumerable<T> records);
	}

	public interface IUpdateApiInteractor<T> where T : IApiRecord
	{
		Task<Many<T>> Update(Updates<T> updates);
	}

	public interface IDeleteApiInteractor<T> where T : IApiRecord
	{
		Task<bool> Delete(IEnumerable<string> id);
	}
}