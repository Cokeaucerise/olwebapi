using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Repository;

namespace OLWebApi.Crud.Interactor
{
	public class CrudApiInteractor<T> : ApiInteractor<T>, ICrudApiInteractor<T>
	where T : ApiRecord
	{
		protected new ICrudApiRepository<T> Repository => base.Repository as ICrudApiRepository<T>;

		public CrudApiInteractor(ICrudApiRepository<T> repository) : base(repository)
		{ }

		public async Task<Get<T>> Get(string id)
		{
			var data = await Repository.Get(id);
			return new() { Data = data };
		}

		public virtual async Task<Many<T>> Create(IEnumerable<T> records)
		{
			return new(await (Repository.Create(records) ?? AsyncEnumerable.Empty<T>()).ToListAsync());
		}

		public virtual async Task<Many<T>> Update(Updates<T> updates)
		{
			return new(await (Repository.Update(updates) ?? AsyncEnumerable.Empty<T>()).ToListAsync());
		}

		public virtual Task<bool> Delete(IEnumerable<string> ids)
		{
			return Repository.Delete(ids);
		}
	}
}