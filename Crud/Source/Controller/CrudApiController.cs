using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Interactor;

namespace OLWebApi.Crud.Controller
{
	public class CrudApiController<T> : ApiController<T>, ICrudApiController<T> where T : ApiRecord
	{
		protected new ICrudApiInteractor<T> Interactor => base.Interactor as ICrudApiInteractor<T> ?? throw new InvalidOperationException(typeof(ICrudApiInteractor<T>).Name);

		public CrudApiController(ICrudApiInteractor<T> interactor) : base(interactor) { }


		[HttpGet("{id}")]
		public virtual async Task<ActionResult<Get<T>>> Get([FromRoute, Required] string id)
		{
			if (id is null) return BadRequest(new ArgumentNullException(nameof(id)));
			if (!ModelState.IsValid) return BadRequest(ModelState);

			Get<T> result = await Interactor.Get(id);
			return Ok(result ?? new Get<T>());
		}

		[HttpPost]
		public virtual async Task<ActionResult<Many<T>>> Create([FromBody] Create<T> body)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			return !(body?.Data?.Any() ?? false) ?
				BadRequest(new ArgumentNullException(nameof(body))) :
				Ok(await Interactor.Create(body?.Data));
		}

		[HttpPut]
		public virtual async Task<ActionResult<Many<T>>> Update([FromBody] IEnumerable<T> updates)
		{
			if (!(updates?.Any() ?? false)) return BadRequest(new ArgumentNullException(nameof(updates)));
			if (updates.Any(u => u == null)) return BadRequest(new ArgumentNullException(nameof(updates)));

			var patches = new Updates<T>(updates?.ToDictionary(u => u.Id, u => new Update<T>(u)));

			return await Update(patches);
		}

		[HttpPatch]
		public virtual async Task<ActionResult<Many<T>>> Update([FromBody] Updates<T> updates)
		{
			if (!(updates?.Any() ?? false)) return BadRequest(new ArgumentNullException(nameof(updates)));
			if (updates.Values.Any(u => u == null)) return BadRequest(new ArgumentNullException(nameof(updates)));

			return Ok(await Interactor.Update(updates) ?? new());
		}

		[HttpDelete]
		public virtual async Task<ActionResult<bool>> Delete([FromQuery] IEnumerable<string> ids)
		{
			if (!(ids?.Any() ?? false)) return BadRequest(new ArgumentNullException(nameof(ids)));
			if (ids.Any(id => id == null)) return BadRequest(new ArgumentNullException(nameof(ids)));

			return Ok(await Interactor.Delete(ids));
		}

	}
}
