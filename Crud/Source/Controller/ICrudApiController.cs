using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Crud.Controller
{
	public interface ICrudApiController<T> : IApiController<T>,
		IReadApiController<T>,
		ICreateApiController<T>,
		IUpdateApiController<T>,
		IDeleteApiController<T>
	where T : IApiRecord
	{ }

	public interface IReadApiController<T> where T : IApiRecord
	{
		Task<ActionResult<Get<T>>> Get(string id);
	}

	public interface ICreateApiController<T> where T : IApiRecord
	{
		Task<ActionResult<Many<T>>> Create(Create<T> body);
	}

	public interface IUpdateApiController<T> where T : IApiRecord
	{
		Task<ActionResult<Many<T>>> Update(Updates<T> updates);
		Task<ActionResult<Many<T>>> Update(IEnumerable<T> updates);
	}

	public interface IDeleteApiController<T> where T : IApiRecord
	{
		Task<ActionResult<bool>> Delete(IEnumerable<string> ids);
	}
}
