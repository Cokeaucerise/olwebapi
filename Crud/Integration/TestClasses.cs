using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway;

namespace OLWebApi.Crud.Integration
{
	public record TestRecord : ApiRecord
	{
		public TestRecord() { }
		public TestRecord(string id) : base(id) { }
		public ManyRelations<TestRecordChild> Childs { get; set; }
		public int TestValidationValue { get; set; }
	}

	public record TestRecordChild : ApiRecord
	{
		public TestRecordChild() { }
		public TestRecordChild(string id) : base(id) { }
	}

	public class TestApiGateway<T> : ICrudApiGateway<T>
	where T : IApiRecord
	{
		public Task<T> Get(string id) => throw new NotImplementedException();
		public IAsyncEnumerable<T> Create(IEnumerable<T> record) => throw new NotImplementedException();
		public IAsyncEnumerable<T> Update(Updates<T> updates) => throw new NotImplementedException();
		public Task<bool> Delete(IEnumerable<string> ids) => throw new NotImplementedException();
	}
}