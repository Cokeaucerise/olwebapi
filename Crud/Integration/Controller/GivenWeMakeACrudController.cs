using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Integration;
using Xunit;

namespace OLWebApi.Crud.Integration
{
	public class GivenWeMakeACrudController : GivenWeUseACrudApi
	{
		public GivenWeMakeACrudController(ApiFactory factory) : base(factory) { }

		protected Create<TestRecord> BuildBodyRequest() => new()
		{
			Data = new TestRecord[] {
				new("1") { Childs = new ManyRelations<TestRecordChild>(new[] { "2" }) }
			}
		};

		[Fact]
		public async Task WhenWeMakeAGetRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var client = Factory.CreateClient();

			// When
			var result = await client.GetAsync($"{nameof(TestRecord)}/1");

			// Then
			Gateway.Verify(g => g.Get("1"), Times.Once());
		}

		[Fact]
		public async Task WhenWeMakeACreateRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var body = new Create<TestRecord>() { Data = new TestRecord[] { new() { Id = "1" } } };
			var create = CreateStringContent(body);
			var client = Factory.CreateClient();

			// When
			var result = await client.PostAsync($"{nameof(TestRecord)}", create);

			// Then
			Gateway.Verify(g => g.Create(It.IsAny<IEnumerable<TestRecord>>()), Times.Once());
		}

		[Fact]
		public async Task WhenWeMakeAUpdateRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var client = Factory.CreateClient();
			var updates = CreateStringContent(new Updates<TestRecord> { { "0", new() } });

			// When
			var result = await client.PatchAsync($"{nameof(TestRecord)}", updates);

			// Then
			Gateway.Verify(g => g.Update(It.IsAny<Updates<TestRecord>>()), Times.Once());
		}

		[Fact]
		public async Task WhenWeMakeADeleteRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var client = Factory.CreateClient();

			// When
			var result = await client.DeleteAsync($"{nameof(TestRecord)}?ids=1");

			// Then
			Gateway.Verify(g => g.Delete(It.Is<IEnumerable<string>>(s => s.Contains("1"))), Times.Once());
		}
	}
}