using System.Net.Http;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.JsonConverters;
using OLWebApi.Crud.Gateway;
using OLWebApi.Integration;
using Xunit;

namespace OLWebApi.Crud.Integration
{
	public class GivenWeUseACrudApi : IClassFixture<ApiFactory>
	{
		protected WebApplicationFactory<Startup> Factory;
		protected readonly Mock<IApiInteractor<TestRecord>> Interactor = new();
		protected readonly Mock<ICrudApiGateway<TestRecord>> Gateway = new();
		protected readonly Mock<ICrudApiGateway<TestRecordChild>> GatewayChild = new();

		protected static JsonSerializerOptions JsonOptions
		{
			get
			{
				JsonSerializerOptions options = new() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
				options.Converters.Add(new UpdateJsonConverterfactory());
				return options;
			}
		}

		protected static StringContent CreateStringContent(object body)
			=> new(JsonSerializer.Serialize(body, JsonOptions), Encoding.UTF8, "application/json");

		public GivenWeUseACrudApi(ApiFactory factory)
		{
			Gateway.Setup(g => g.Get(It.IsAny<string>())).ReturnsAsync(new TestRecord());
			Factory = factory.WithWebHostBuilder(builder =>
			{
				builder.ConfigureServices(services =>
				{
					services.AddTransient(p => Gateway.Object);         //Overide open type service
					services.AddTransient(p => GatewayChild.Object);    //Overide open type service
				});
			});
		}
	}
}