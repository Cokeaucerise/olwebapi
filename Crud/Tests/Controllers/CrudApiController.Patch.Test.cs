using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using OLWebApi.Core.Entities.Action;
using Xunit;

namespace OLWebApi.Crud.Tests.Controllers
{
	public class CrudApiControllerPatchTest : CrudApiControllerTest
	{
		protected Updates<TestRecord> TestUpdates => new()
		{
			{ TestModel.Id, new(new Dictionary<string, object>() { { nameof(TestRecord.Value), 1 } }) }
		};

		[Fact]
		public async Task Update_NullEntry_BadRequestArgumentException()
		{
			//Given
			Updates<TestRecord> updates = new() { { "1", null } };

			//When
			ActionResult<Many<TestRecord>> response = await CrudApiController.Update(updates);

			//Then
			Assert.NotNull(response);

			BadRequestObjectResult error = response.Result as BadRequestObjectResult;
			Assert.NotNull(error);
			Assert.Equal((int)HttpStatusCode.BadRequest, error.StatusCode);

			ArgumentNullException exception = error.Value as ArgumentNullException;
			Assert.NotNull(exception);
			Assert.Equal("updates", exception.ParamName);
		}

		[Fact]
		public async Task Update_ModelDoesntExist_EmptyResults()
		{
			//Given
			CrudApiInteractor
				.Setup(s => s.Update(It.IsAny<Updates<TestRecord>>()))
				.Returns(Task.FromResult(null as Many<TestRecord>));

			//When
			ActionResult<Many<TestRecord>> response = await CrudApiController.Update(TestUpdates);

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.Empty((ok.Value as Many<TestRecord>).Data);
		}

		[Fact]
		public async Task Update_ValidModel_ReturnUpdatedModel()
		{
			//Given
			CrudApiInteractor
				.Setup(s => s.Update(It.IsAny<Updates<TestRecord>>()))
				.Returns(Task.FromResult(FakeMany));

			//When
			ActionResult<Many<TestRecord>> response = await CrudApiController.Update(TestUpdates);

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Many<TestRecord> many = ok.Value as Many<TestRecord>;
			Assert.NotNull(many);
			Assert.Equal(TestModel.Id, many.Data.First().Id);
		}
	}
}