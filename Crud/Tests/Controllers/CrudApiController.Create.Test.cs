using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OLWebApi.Core.Entities.Action;
using Xunit;

namespace OLWebApi.Crud.Tests.Controllers
{
	public class CrudApiControllerCreateTest : CrudApiControllerTest
	{
		[Fact]
		public async Task Create_NullEntry_BadRequestArgumentException()
		{
			//Given

			//When
			ActionResult<Many<TestRecord>> response = await CrudApiController.Create(null);

			//Then
			Assert.NotNull(response);

			BadRequestObjectResult error = response.Result as BadRequestObjectResult;
			Assert.NotNull(error);
			Assert.Equal((int)HttpStatusCode.BadRequest, error.StatusCode);

			ArgumentNullException exception = error.Value as ArgumentNullException;
			Assert.NotNull(exception);
			Assert.Equal("body", exception.ParamName);
		}

		[Fact]
		public async Task Create_EntryNotNull_ReturnCreateResult()
		{
			//Given
			CrudApiInteractor
				.Setup(s => s.Create(new[] { TestModel }))
				.Returns(Task.FromResult(FakeMany));
			var body = new Create<TestRecord> { Data = new[] { TestModel } };

			//When
			ActionResult<Many<TestRecord>> response = await CrudApiController.Create(body);

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Many<TestRecord> get = ok.Value as Many<TestRecord>;
			Assert.NotNull(get);
			Assert.Equal(FakeMany.Data, get.Data);
		}
	}
}