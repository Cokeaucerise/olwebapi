using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace OLWebApi.Crud.Tests.Controllers
{
	public class CrudApiControllerDeleteTest : CrudApiControllerTest
	{
		[Fact]
		public async Task Delete_NullId_BadRequestArgumentException()
		{
			//Given

			//When
			ActionResult<bool> response = await CrudApiController.Delete(null);

			//Then
			Assert.NotNull(response);

			BadRequestObjectResult error = response.Result as BadRequestObjectResult;
			Assert.NotNull(error);
			Assert.Equal((int)HttpStatusCode.BadRequest, error.StatusCode);

			ArgumentNullException exception = error.Value as ArgumentNullException;
			Assert.NotNull(exception);
			Assert.Equal("ids", exception.ParamName);
		}

		[Fact]
		public async Task Delete_ModelDoesntExist_ReturnFalse()
		{
			//Given
			CrudApiInteractor
				.Setup(s => s.Delete(It.IsAny<IEnumerable<string>>()))
				.ReturnsAsync(false);

			//When
			ActionResult<bool> response = await CrudApiController.Delete(new[] { TestModel.Id });

			//Then
			Assert.NotNull(response);

			OkObjectResult error = response.Result as OkObjectResult;
			bool? result = error.Value as bool?;
			Assert.False(result);
		}

		[Fact]
		public async Task Delete_ValidModel_ReturnDeletedModel()
		{
			//Given
			CrudApiInteractor.Setup(s => s.Delete(It.IsAny<IEnumerable<string>>()))
				.ReturnsAsync(true);

			//When
			ActionResult<bool> response = await CrudApiController.Delete(new[] { TestModel.Id });

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			bool deleted = (bool)ok.Value;
			Assert.True(deleted);
		}
	}
}