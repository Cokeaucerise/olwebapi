using System;
using Moq;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Crud.Controller;
using OLWebApi.Crud.Interactor;

namespace OLWebApi.Crud.Tests.Controllers
{
	public abstract class CrudApiControllerTest
	{
		public TestRecord TestModel;
		public Single<TestRecord> FakeSingle => new(TestModel);
		public Get<TestRecord> FakeGet => new(TestModel);
		public Many<TestRecord> FakeMany => new(new[] { TestModel });

		public readonly Mock<ICrudApiInteractor<TestRecord>> CrudApiInteractor;

		public ICrudApiController<TestRecord> CrudApiController => new CrudApiController<TestRecord>(CrudApiInteractor.Object);

		public CrudApiControllerTest()
		{
			TestModel = new();
			TestModel = TestModel with { Id = Guid.NewGuid().ToString() };
			CrudApiInteractor = new Mock<ICrudApiInteractor<TestRecord>>();
		}
	}
}