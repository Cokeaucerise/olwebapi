using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using OLWebApi.Core.Entities.Action;
using Xunit;

namespace OLWebApi.Crud.Tests.Controllers
{
	public class CrudApiControllerReadTest : CrudApiControllerTest
	{
		[Fact]
		public async Task Get_InteractorReturnNull_EmptyGet()
		{
			//Given
			CrudApiInteractor.Setup(s => s.Get(TestModel.Id))
				.Returns(Task.FromResult(default(Get<TestRecord>)));

			//When
			ActionResult<Get<TestRecord>> response = await CrudApiController.Get(TestModel.Id);

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Get<TestRecord> get = ok.Value as Get<TestRecord>;
			Assert.NotNull(get);
		}

		[Fact]
		public async Task Get_InteractorReturnDataNull_EmptyGetData()
		{
			//Given
			CrudApiInteractor.Setup(s => s.Get(TestModel.Id))
				.Returns(Task.FromResult(new Get<TestRecord>()));

			//When
			ActionResult<Get<TestRecord>> response = await CrudApiController.Get(TestModel.Id);

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Get<TestRecord> get = ok.Value as Get<TestRecord>;
			Assert.NotNull(get);
			Assert.Equal(default, get.Data);
		}

		[Fact]
		public async Task Get_InteractorReturnValidModel_GetWithDataAndEmptyInclude()
		{
			//Given
			CrudApiInteractor.Setup(s => s.Get(It.IsAny<string>()))
				.Returns(Task.FromResult(FakeGet));

			//When
			ActionResult<Get<TestRecord>> response = await CrudApiController.Get(TestModel.Id);

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Get<TestRecord> get = ok.Value as Get<TestRecord>;
			Assert.NotNull(get);
			Assert.Equal(TestModel.Id, get.Data.Id);
		}

		[Fact]
		public async Task Get_IdNull_BadRequestArgumentNullException()
		{
			//Given
			CrudApiInteractor.Setup(s => s.Get(It.IsAny<string>()))
				.Returns(Task.FromResult(FakeGet));

			//When
			ActionResult<Get<TestRecord>> response = await CrudApiController.Get(null);

			//Then
			Assert.NotNull(response);

			BadRequestObjectResult error = response.Result as BadRequestObjectResult;
			Assert.NotNull(error);
			Assert.Equal((int)HttpStatusCode.BadRequest, error.StatusCode);

			ArgumentNullException exception = error.Value as ArgumentNullException;
			Assert.NotNull(exception);
			Assert.Equal("id", exception.ParamName);
		}
	}
}