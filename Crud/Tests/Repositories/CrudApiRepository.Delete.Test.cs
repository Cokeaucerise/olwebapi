using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Provider;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Repository;
using Xunit;

namespace OLWebApi.Crud.Tests.Repository
{
	public class CrudApiRepositoryTest : RepositoryTest
	{
		[Fact]
		public async Task Delete_GatewayReturnFalse_ReturnFalse()
		{
			//Given
			string[] ids = new[] { Guid.NewGuid().ToString() };
			Gateway
				.Setup(c => c.Delete(It.IsAny<IEnumerable<string>>()))
				.ReturnsAsync(false);

			//When
			bool delete = await Repository.Delete(ids);

			//Then
			Assert.False(delete);
		}

		[Fact]
		public async Task Delete_DtoNotFalse_CallDeleteOfGateway()
		{
			//Given
			string[] ids = new[] { Guid.NewGuid().ToString() };
			TestRecord record = new(ids[0]);

			Gateway
				.Setup(c => c.Delete(It.IsAny<IEnumerable<string>>()))
				.ReturnsAsync(true);

			//When
			bool deleted = await Repository.Delete(ids);

			//Then
			Gateway.Verify(c => c.Delete(ids), Times.Once());
		}
	}
}
