using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Provider;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Repository;
using Xunit;

namespace OLWebApi.Crud.Tests.Repository
{
	public class CrudApiRepositoryUpdateTest : RepositoryTest
	{
		[Fact]
		public async Task Update_GatewayReturnNull_ReturnEmpty()
		{
			//Given
			Updates<TestRecord> updates = new() { { "1", new() } };
			Gateway
				.Setup(c => c.Update(It.IsAny<Updates<TestRecord>>()))
				.Returns(null as IAsyncEnumerable<TestRecord>);

			//When
			IEnumerable<TestRecord> result = await Repository.Update(updates).ToListAsync();

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task Update_DtoNotNull_CallUpdateOfGateway()
		{
			//Given
			Updates<TestRecord> updates = new() { { "1", new() } };
			IEnumerable<TestRecord> records = new TestRecord[] { new() };

			Gateway
				.Setup(c => c.Update(It.IsAny<Updates<TestRecord>>()))
				.Returns(records.ToAsyncEnumerable());

			//When
			IEnumerable<TestRecord> result = await Repository.Update(updates).ToListAsync();

			//Then
			Gateway.Verify(c => c.Update(updates), Times.Once());
		}
	}
}
