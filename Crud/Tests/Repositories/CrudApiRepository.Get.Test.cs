using System;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Provider;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Repository;
using Xunit;

namespace OLWebApi.Crud.Tests.Repository
{
	public class CrudApiRepositoryGetTest : RepositoryTest
	{
		[Fact]
		public async Task Get_RecordDontExist_ReturnNull()
		{
			//Given
			TestRecord record = new(Guid.NewGuid().ToString());
			Gateway
				.Setup(c => c.Get(It.IsAny<string>()))
				.ReturnsAsync(null as TestRecord);

			//When
			TestRecord result = await Repository.Get(record.Id);

			//Then
			Gateway.Verify(c => c.Get(It.Is<string>(input => input.Contains(record.Id))), Times.Once());
		}

		[Fact]
		public async Task Get_RecordExist_ReturnRecordFromGateway()
		{
			//Given
			TestRecord record = new() { Id = Guid.NewGuid().ToString(), CreatedAt = DateTimeOffset.UtcNow, UpdatedAt = DateTimeOffset.UtcNow };
			Gateway
				.Setup(c => c.Get(It.IsAny<string>()))
				.ReturnsAsync(record);

			//When
			TestRecord result = await Repository.Get(record.Id);

			//Then
			Assert.Equal(record, result);
			Gateway.Verify(c => c.Get(It.Is<string>(input => input.Contains(record.Id))), Times.Once());
		}
	}
}
