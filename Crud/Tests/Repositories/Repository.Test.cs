using System;
using Moq;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Repository;

namespace OLWebApi.Crud.Tests.Repository
{
	public class RepositoryTest
	{
		public TestRecord TestModel;
		public static TestRecord FakeGet => new();

		public readonly Mock<ICrudApiGateway<TestRecord>> Gateway = new();
		public CrudApiRepository<TestRecord> Repository => new(Gateway.Object);

		public RepositoryTest()
		{
			TestModel = new(Guid.NewGuid().ToString());
		}
	}
}
