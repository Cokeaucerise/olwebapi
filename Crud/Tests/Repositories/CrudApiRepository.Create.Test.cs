using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Provider;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Repository;
using Xunit;

namespace OLWebApi.Crud.Tests.Repository
{
	public class CrudApiRepositoryCreateTest : RepositoryTest
	{
		[Fact]
		public async Task Create_GatewayReturnNull_ReturnEmpty()
		{
			//Given
			TestRecord[] records = new TestRecord[] { new() };
			Gateway
				.Setup(c => c.Create(It.IsAny<IEnumerable<TestRecord>>()))
				.Returns(null as IAsyncEnumerable<TestRecord>);

			//When
			var result = await Repository.Create(records).ToListAsync();

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task Create_DtoNotNull_CallCreateOfGateway()
		{
			//Given
			IEnumerable<TestRecord> records = new TestRecord[] { new() };

			Gateway
				.Setup(c => c.Create(It.IsAny<IEnumerable<TestRecord>>()))
				.Returns(records.ToAsyncEnumerable());

			//When
			IEnumerable<TestRecord> result = await Repository.Create(records).ToListAsync();

			//Then
			Gateway.Verify(c => c.Create(records), Times.Once());
		}
	}
}
