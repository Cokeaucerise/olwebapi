using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Core.Provider;
using OLWebApi.Crud.Gateway;
using OLWebApi.Mongo.Gateway.Builders;
using OLWebApi.Mongo.Gateway.Filterers;
using OLWebApi.Query.Gateway;

namespace OLWebApi.Mongo.Gateway
{
	[ExcludeFromCodeCoverage]
	public static class DependencyInjection
	{
		public static IServiceCollection AddMongoGateway(this IServiceCollection services)
		{
			services.AddScoped(typeof(ICrudApiGateway<>), typeof(MongoGateway<>));
			services.AddScoped(typeof(IQueryApiGateway<>), typeof(MongoGateway<>));

			services.AddMongoPropertyFilters();

			return services;
		}

		private static IServiceCollection AddMongoPropertyFilters(this IServiceCollection services)
		{
			services.AddTransient<IMongoPropertyFilterer, MongoEqualFilterer>();
			services.AddTransient<IMongoPropertyFilterer, MongoNotEqualFilterer>();
			services.AddTransient<IMongoPropertyFilterer, MongoLessFilterer>();
			services.AddTransient<IMongoPropertyFilterer, MongoLessOrEqualFilterer>();
			services.AddTransient<IMongoPropertyFilterer, MongoGreaterFilterer>();
			services.AddTransient<IMongoPropertyFilterer, MongoGreaterOrEqualFilterer>();
			services.AddTransient<IMongoPropertyFilterer, MongoInFilterer>();
			services.AddTransient<IMongoPropertyFilterer, MongoNotInFilterer>();

			services.AddScoped(typeof(IMongoFilterBuilder<>), typeof(MongoFilterBuilder<>));
			services.AddScoped(typeof(IMongoSortBuilder<>), typeof(MongoSortBuilder<>));
			services.AddScoped(typeof(IMongoGroupBuilder<>), typeof(MongoGroupBuilder<>));

			services.AddTransient<IPropertyValueProvider, CaseInsensitivePropertyValueProvider>();

			return services;
		}
	}
}