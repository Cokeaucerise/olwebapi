using MongoDB.Driver;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Mongo.Gateway.Extensions
{
	public static class IAggregateFluentExtensions
	{
		public static IAggregateFluent<T> Sort<T>(this IAggregateFluent<T> aggregate, SortDefinition<T> sortDefinition)
		{
			if (sortDefinition.ToString() != "{}")
				aggregate = aggregate.Sort(sortDefinition);

			return aggregate;
		}

		public static IAggregateFluent<T> Page<T>(this IAggregateFluent<T> aggregator, QueryParameter parameter)
		{
			if (parameter?.Paging == null)
				return aggregator;

			return aggregator
				.Skip(parameter.Paging.Skip ?? default)
				.Limit(parameter.Paging.Take ?? int.MaxValue);
		}
	}
}