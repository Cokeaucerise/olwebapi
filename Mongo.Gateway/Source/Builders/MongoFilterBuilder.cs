using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Mongo.Gateway.Filterers;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Mongo.Gateway.Builders
{
	public interface IMongoFilterBuilder<T> where T : ApiRecord
	{
		FilterDefinition<T> Build(QueryParameter parameter);
	}

	public class MongoFilterBuilder<T> : IMongoFilterBuilder<T> where T : ApiRecord
	{
		protected readonly FilterDefinitionBuilder<T> FilterBuilder = Builders<T>.Filter;

		protected IEnumerable<IMongoPropertyFilterer> Filterers { get; }

		public MongoFilterBuilder(IEnumerable<IMongoPropertyFilterer> filterers)
		{
			Filterers = filterers.Reverse();
		}

		public virtual FilterDefinition<T> Build(QueryParameter parameter)
		{
			return IdsFilter(parameter?.Ids) & BuildFilters(parameter.Filters);
		}

		protected FilterDefinition<T> IdsFilter(IEnumerable<string> ids)
		{
			return ids.Any() ? Builders<T>.Filter.In(nameof(IApiRecord.Id), ids) : Builders<T>.Filter.Empty;
		}

		protected FilterDefinition<T> BuildFilters(PropertyFilters filters)
		{
			if (!(filters?.Any() ?? false))
				return FilterBuilder.Empty;

			return filters
				.Select(filter => GetFilterDefinition(filter))
				.Aggregate((a, b) => a & b);
		}

		protected FilterDefinition<T> GetFilterDefinition(PropertyFilter filter)
		{
			return Filterers
				.FirstOrDefault(c => c.CanFilter(filter))?
				.Filter<T>(filter);
		}
	}
}