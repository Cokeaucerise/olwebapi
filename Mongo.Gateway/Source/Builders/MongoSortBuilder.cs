using System;
using System.Linq;
using MongoDB.Driver;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Mongo.Gateway.Builders
{
	public interface IMongoSortBuilder<T> where T : class
	{
		SortDefinition<T> Build(QueryParameter parameter);
	}

	public class MongoSortBuilder<T> : IMongoSortBuilder<T> where T : class
	{
		protected SortDefinitionBuilder<T> SortBuilder = Builders<T>.Sort;

		public SortDefinition<T> Build(QueryParameter parameter)
		{
			return SortBuilder.Combine(parameter?.OrderBys?.Select(BuildSortDefinition));
		}

		private SortDefinition<T> BuildSortDefinition(OrderBy orderBy)
		{
			var prop = orderBy.Target.Equals(nameof(IApiRecord.Id), StringComparison.OrdinalIgnoreCase) ? "_id" : orderBy.Target;
			return orderBy.Ascending ? SortBuilder.Ascending(prop) : SortBuilder.Descending(prop);
		}
	}
}