using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Mongo.Gateway.Builders
{
	public interface IMongoGroupBuilder<T> where T : class
	{
		ProjectionDefinition<T> Build(ReportParameter parameter);
	}

	public class MongoGroupBuilder<T> : IMongoGroupBuilder<T> where T : class
	{
		public ProjectionDefinitionBuilder<T> ProjectionBuilder = Builders<T>.Projection;

		public ProjectionDefinition<T> Build(ReportParameter parameter)
		{
			return BuildGroupBy(parameter?.GroupBies);
		}

		private static BsonDocument BuildGroupBy(GroupBies groupBies)
		{
			return BuildGroupKey(groupBies).Merge(BuildGroupValues(groupBies.Groups));
		}

		private static BsonDocument BuildGroupKey(GroupBies groupBies)
		{
			return new BsonDocument { { "_id", BuildGroupKeyFields(groupBies.Keys) } };
		}

		private static BsonDocument BuildGroupKeyFields(HashSet<string> keys)
		{
			return new BsonDocument(keys.Select(k => new BsonElement(k, $"${k}")));
		}

		private static BsonDocument BuildGroupValues(IEnumerable<GroupBy> groups)
		{
			return new BsonDocument(groups.Select(v => BuildGroupValue(v)));
		}

		private static BsonElement BuildGroupValue(GroupBy group)
		{
			return new BsonElement(group.Name, BuildGroupAccumulator(group));
		}

		private static BsonDocument BuildGroupAccumulator(GroupBy group)
		{
			if (group.Accumulator is CountAccumulator)
				return new BsonDocument { { $"$sum", 1 } };

			return new BsonDocument { { $"${group.Accumulator.Operator}", $"${group.Field}" } };
		}
	}
}