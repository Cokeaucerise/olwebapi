using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway;
using OLWebApi.Mongo.Gateway.Builders;
using OLWebApi.Mongo.Gateway.Extensions;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Gateway;

namespace OLWebApi.Mongo.Gateway
{
	public class MongoGateway<T> : ICrudApiGateway<T>, IQueryApiGateway<T> where T : ApiRecord
	{
		protected static IMongoCollection<T> Collection { get; private set; }
		protected IMongoFilterBuilder<T> FilterBuilder { get; }
		protected IMongoSortBuilder<T> SortBuilder { get; }
		protected IMongoSortBuilder<ReportEntry> ReportSortBuilder { get; }
		protected IMongoGroupBuilder<T> GroupBuilder { get; }

		protected static readonly UpdateDefinitionBuilder<T> UpdateBuilder = Builders<T>.Update;

		public MongoGateway(
			IMongoDatabase db,
			IMongoFilterBuilder<T> filterBuilder,
			IMongoSortBuilder<T> sortBuilder,
			IMongoSortBuilder<ReportEntry> reportSortBuilder,
			IMongoGroupBuilder<T> groupBuilder)
		{
			Collection = db.GetCollection<T>(typeof(T).Name);
			FilterBuilder = filterBuilder;
			SortBuilder = sortBuilder;
			ReportSortBuilder = reportSortBuilder;
			GroupBuilder = groupBuilder;
		}

		public async Task<T> Get(string id)
		{
			var getQuery = new QueryParameter
			{
				Ids = new[] { id },
				Paging = new Paging(0, 1)
			};

			return (await Query(getQuery).ToArrayAsync()).FirstOrDefault();
		}

		public async IAsyncEnumerable<T> Query(QueryParameter parameter)
		{
			var query = Collection.Aggregate()
				.Match(FilterBuilder.Build(parameter));

			if (parameter.OrderBys.Any())
				query = query.Sort(SortBuilder.Build(parameter));

			query = query.Page(parameter);

			foreach (var item in await query.ToListAsync())
				yield return item;
		}

		public async Task<Report> Report(ReportParameter parameter)
		{
			var query = Collection.Aggregate()
				.Match(FilterBuilder.Build(parameter))
				.Group(GroupBuilder.Build(parameter))
				.Project(GroupToReportEntryProjection(parameter));

			if (parameter.OrderBys.Any())
				query = query.Sort(ReportSortBuilder.Build(AddGroupKeysPrefix(parameter)));

			return new Report(await query.Page(parameter).ToListAsync());
		}

		public async IAsyncEnumerable<T> Create(IEnumerable<T> records)
		{
			var toUpsert = await ApplyBaseRecordValues(records);

			foreach (var record in toUpsert)
				await Collection.ReplaceOneAsync(
					filter: FilterBuilder.Build(new QueryParameter { Ids = new[] { record.Id } }),
					options: new ReplaceOptions { IsUpsert = true },
					replacement: record
				);

			await foreach (var item in Query(new() { Ids = toUpsert.Select(r => r.Id) }))
				yield return item;
		}

		private async Task<IEnumerable<T>> ApplyBaseRecordValues(IEnumerable<T> records)
		{
			var now = DateTimeOffset.UtcNow;
			var baseRecords = records.Select(
				async r => r with
				{
					Id = r.Id ?? Guid.NewGuid().ToString(),
					CreatedAt = (await Get(r.Id))?.CreatedAt ?? now,
					UpdatedAt = now
				}
			).ToArray();

			await Task.WhenAll(baseRecords);
			return baseRecords.Select(r => r.Result);
		}

		public async IAsyncEnumerable<T> Update(Updates<T> updates)
		{
			var updateDefinitions = updates.Select(u => new KeyValuePair<string, UpdateDefinition<T>>(u.Key, BuildUpdate(u.Value)));

			foreach ((string id, Update<T> update) in updates)
			{
				var idFilter = FilterBuilder.Build(new QueryParameter { Ids = new[] { id } });
				await Collection.FindOneAndUpdateAsync(idFilter, BuildUpdate(update));
			}

			await foreach (var item in Query(new() { Ids = updates.Keys }))
				yield return item;
		}

		private static UpdateDefinition<T> BuildUpdate(Update<T> updateValues)
		{
			UpdateDefinition<T> update = UpdateBuilder.Set(nameof(IApiRecord.UpdatedAt), DateTimeOffset.UtcNow);
			foreach (var (prop, value) in updateValues)
				update = update.Set(prop.Name, value);

			return update;
		}

		public async Task<bool> Delete(IEnumerable<string> ids)
		{
			var idsFilter = FilterBuilder.Build(new QueryParameter { Ids = ids });
			var result = await Collection.DeleteManyAsync(idsFilter);

			return result.IsAcknowledged;
		}

		private static ProjectionDefinition<BsonDocument, ReportEntry> GroupToReportEntryProjection(ReportParameter parameter)
		{
			var values = new BsonDocument(parameter.GroupBies.Groups.ToDictionary(g => g.Name, g => $"${g.Name}"));

			return new BsonDocument {
				{"_id", false },
				{ nameof(ReportEntry.Keys), "$_id" },
				{ nameof(ReportEntry.Values), values },
			};
		}

		private static ReportParameter AddGroupKeysPrefix(ReportParameter parameter)
		{
			return parameter with
			{
				OrderBys = new OrderBys(
					parameter.OrderBys.Select(
						ob => ob with { Target = $"{nameof(ReportEntry.Keys)}.{ob.Target}" }
					)
				)
			};
		}

	}
}
