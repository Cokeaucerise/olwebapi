using System.Collections;
using System.Linq;
using MongoDB.Driver;
using OLWebApi.Core.Entities.Filters;

namespace OLWebApi.Mongo.Gateway.Filterers
{
	public abstract class MongoPropertyFilterer<TFilterOperator> : IMongoPropertyFilterer
	where TFilterOperator : IFilterOperator
	{
		protected static FilterDefinitionBuilder<T> GetFilterBuilder<T>() => Builders<T>.Filter;

		public bool CanFilter(PropertyFilter filter) => filter.Operator.GetType() == typeof(TFilterOperator);
		public abstract FilterDefinition<T> Filter<T>(PropertyFilter propertyFilter);

	}

	public class MongoEqualFilterer : MongoPropertyFilterer<FilterEqualOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter) => GetFilterBuilder<T>().Eq(filter.Target, filter.Value);
	}

	public class MongoNotEqualFilterer : MongoPropertyFilterer<FilterNotEqualOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter)
		{
			var filterBuilder = GetFilterBuilder<T>();
			return filterBuilder.Not(filterBuilder.Eq(filter.Target, filter.Value));
		}
	}

	public class MongoLessFilterer : MongoPropertyFilterer<FilterLessOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter)
		{
			return GetFilterBuilder<T>().Lt(filter.Target, filter.Value);
		}
	}

	public class MongoLessOrEqualFilterer : MongoPropertyFilterer<FilterLessOrEqualOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter)
		{
			return GetFilterBuilder<T>().Lte(filter.Target, filter.Value);
		}
	}

	public class MongoGreaterFilterer : MongoPropertyFilterer<FilterGreaterOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter)
		{
			return GetFilterBuilder<T>().Gt(filter.Target, filter.Value);
		}
	}

	public class MongoGreaterOrEqualFilterer : MongoPropertyFilterer<FilterGreaterOrEqualOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter)
		{
			return GetFilterBuilder<T>().Gte(filter.Target, filter.Value);
		}
	}

	public class MongoInFilterer : MongoPropertyFilterer<FilterInOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter)
		{
			return GetFilterBuilder<T>().In(filter.Target, (filter.Value as IEnumerable).OfType<object>());
		}
	}

	public class MongoNotInFilterer : MongoPropertyFilterer<FilterNotInOperator>
	{
		public override FilterDefinition<T> Filter<T>(PropertyFilter filter)
		{
			var filterBuilder = GetFilterBuilder<T>();
			return filterBuilder.Not(filterBuilder.In(filter.Target, (filter.Value as IEnumerable).OfType<object>()));
		}
	}
}