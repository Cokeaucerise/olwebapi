using MongoDB.Driver;
using OLWebApi.Core.Entities.Filters;

namespace OLWebApi.Mongo.Gateway.Filterers
{
	public interface IMongoPropertyFilterer
	{
		bool CanFilter(PropertyFilter filter);
		FilterDefinition<T> Filter<T>(PropertyFilter propertyFilter);
	}
}