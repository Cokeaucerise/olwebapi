using System.Net.Http;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using OLWebApi.Core.JsonConverters;
using Xunit;

namespace OLWebApi.Mongo.Gateway.Integration.Tests
{
	[Collection("Mongo")]
	public class GivenWeUseACrudApi : IClassFixture<ApiFactory>, IClassFixture<DockerMongoFixture>
	{
		protected WebApplicationFactory<Startup> Factory;
		protected DockerMongoFixture DockerMongo;
		protected static JsonSerializerOptions JsonOptions
		{
			get
			{
				JsonSerializerOptions options = new() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
				options.Converters.Add(new UpdateJsonConverterfactory());
				return options;
			}
		}

		protected static StringContent CreateStringContent(object data) => new(JsonSerializer.Serialize(data, JsonOptions), Encoding.UTF8, "application/json");

		public GivenWeUseACrudApi(ApiFactory factory, DockerMongoFixture dockerMongo)
		{
			Factory = factory;
			DockerMongo = dockerMongo;
		}
	}
}