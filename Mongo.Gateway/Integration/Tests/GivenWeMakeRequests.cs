using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Mongo.Gateway.Integration.Tests
{
	[Collection("Mongo")]
	public class GivenWeMakeRequests : GivenWeUseACrudApi
	{
		private IMongoDatabase _database;
		public IMongoDatabase Database => _database ??=
			new MongoClient(MongoClientSettings.FromConnectionString(Connections.Value)).GetDatabase(Connections.Key);

		protected KeyValuePair<string, string> Connections => Factory.Services
			.GetService<IConfiguration>()
			.GetSection("ConnectionStrings")
			.Get<Dictionary<string, string>>()
			.First();

		protected IMongoCollection<TestRecord> RecordCollection => Database.GetCollection<TestRecord>(nameof(TestRecord));
		protected IMongoCollection<TestRecordChild> ChildCollection => Database.GetCollection<TestRecordChild>(nameof(TestRecordChild));

		protected FilterDefinition<T> All<T>() => Builders<T>.Filter.Empty;
		protected HttpClient Client => Factory.CreateClient();

		public GivenWeMakeRequests(ApiFactory factory, DockerMongoFixture dockerMongo) : base(factory, dockerMongo)
		{
			Database.DropCollection(RecordCollection.CollectionNamespace.CollectionName);
			Database.DropCollection(ChildCollection.CollectionNamespace.CollectionName);

			Factory = Factory.WithWebHostBuilder(builder => builder.ConfigureServices(services => services.AddSingleton(Database)));
		}

		[Fact]
		public async Task WhenWeMakeAGetRequest_TheRequestsGoTroughTheGateway()
		{
			// Given
			TestRecord record = new(Guid.NewGuid().ToString()) { Childs = new ManyRelations<TestRecordChild>(new[] { "1", "2", "3" }) };
			RecordCollection.InsertOne(record);
			ChildCollection.InsertMany(new TestRecordChild[] { new("1") { Value = 1 }, new("2") { Value = 2 }, new("3") { Value = 3 } });

			// When
			var request = $"{nameof(TestRecord)}/{record.Id}";
			request += "?includes=Childs!Value<>2!Value<3!Value>=1";
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Get<TestRecord>>(content, JsonOptions);

			Assert.Equal(record.Id, response.Data.Id);
		}

		[Fact]
		public async Task WhenWeMakeAQueryRequest_TheRequestsGoTroughTheGateway()
		{
			// Given
			TestRecord record = new("2") { Childs = new ManyRelations<TestRecordChild>(new[] { "1", "2", "3" }) };
			RecordCollection.InsertMany(new TestRecord[] { new("1"), record, new("3"), new("4") });
			ChildCollection.InsertMany(new TestRecordChild[] { new("1") { Value = 1 }, new("2") { Value = 2 }, new("3") { Value = 3 } });

			// When
			var request = $"{nameof(TestRecord)}/query?ids=1&ids=2&ids=3&ids=4";
			request += "&includes=Childs!Value<>2!Value<3!Value>=1";
			request += $"&{nameof(OrderBys)}=-Id";
			request += "&paging.take=2";
			request += "&filters=Id<>3";
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Query<TestRecord>>(content, JsonOptions);

			Assert.Equal(2, response.Data.Count());
			Assert.Equal("4", response.Data.First().Id);
			Assert.Equal("2", response.Data.Last().Id);
			Assert.Single(response.Included);
		}

		[Fact]
		public async Task WhenWeMakeAReportRequest_TheRequiredParamaterAreValidated()
		{
			// Given
			var request = $"{nameof(TestRecord)}/report";

			// When
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var errors = JsonSerializer.Deserialize<Dictionary<string, object>>(content, JsonOptions);

			Assert.Equal(2, errors.Count);
			Assert.NotNull(errors[nameof(GroupBies.Keys)]);
			Assert.NotNull(errors[nameof(GroupBies.Groups)]);
		}

		[Fact]
		public async Task WhenWeMakeAGetReportRequest_TheRequestsGoTroughTheGateway()
		{
			// Given

			RecordCollection.InsertMany(new TestRecord[] {
				new("1") { StringValue = "1" , NestedObject = new() { Value = 1 } },
				new("2") { StringValue = "1" , NestedObject = new() { Value = 2 } },
				new("3") { StringValue = "2" , NestedObject = new() { Value = 3 } },
				new("4") { StringValue = "2" , NestedObject = new() { Value = 4 } },
				new("5") { StringValue = "2" , NestedObject = new() { Value = 0 } }
			});

			// When
			var request = $"{nameof(TestRecord)}/report?";
			request += $"&{nameof(GroupBies)}.Keys={nameof(TestRecord.StringValue)}";
			request += $"&{nameof(GroupBies)}.groups=DoubleSum:sum:{nameof(TestRecord.NestedObject)}.{nameof(TestRecordChild.Value)}";
			request += $"&{nameof(GroupBies)}.groups=Count:count:{nameof(TestRecord.Id)}";
			request += $"&{nameof(OrderBys)}=-{nameof(TestRecord.StringValue)}";
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Report>(content, JsonOptions);

			Assert.Equal(2, response.Count);

			var group1 = response.Last();
			Assert.Equal(2, group1.Values.Count);
			Assert.Equal(2, int.Parse(group1.Values["Count"].ToString()));
			Assert.Equal(1d + 2d, double.Parse(group1.Values["DoubleSum"].ToString()));

			var group2 = response.First();
			Assert.Equal(2, group2.Values.Count);
			Assert.Equal(3 + 4, double.Parse(group2.Values["DoubleSum"].ToString()));
			Assert.Equal(3, double.Parse(group2.Values["Count"].ToString()));
		}

		[Fact]
		public async Task WhenWeMakeACreateRequest_TheRequestsGoTroughTheGatewayAndreturnNewRecords()
		{
			// Given
			var body = new Create<TestRecord> { Data = new TestRecord[] { new() }, };

			// When
			var result = await Client.PostAsync($"{nameof(TestRecord)}", CreateStringContent(body));

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Many<TestRecord>>(content, JsonOptions);

			Assert.NotNull(response.Data.First().Id);
			Assert.True(response.Data.First().CreatedAt <= DateTimeOffset.UtcNow);
			Assert.Equal(response.Data.First().CreatedAt, response.Data.First().UpdatedAt);

			Assert.Single(RecordCollection.Find(All<TestRecord>()).ToEnumerable());
		}

		[Fact]
		public async Task WhenWeMakeAnUpdateRequest_TheRequestsGoTroughTheGateway()
		{
			// Given
			RecordCollection.InsertOne(new("1"));
			ChildCollection.InsertOne(new("2") { Value = 2 });

			var arr = new object[] { 1, 2, 3, 4, 5 };
			Updates<TestRecord> update = new()
			{
				{
					"1",
					new(new Dictionary<string, object>
					{
						{ nameof(TestRecord.IntValue), 1 },
						{ nameof(TestRecord.DoubleValue), 2.2 },
						{ nameof(TestRecord.StringValue), "333" },
						{ nameof(TestRecord.ArrayValue), arr },
					})
				}
			};

			// When
			var result = await Client.PatchAsync($"{nameof(TestRecord)}", CreateStringContent(update));

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Many<TestRecord>>(content, JsonOptions);

			TestRecord updated = response.Data.First();
			Assert.Equal("1", updated.Id);
			Assert.Equal(1, updated.IntValue);
			Assert.Equal(2.2, updated.DoubleValue);
			Assert.Equal("333", updated.StringValue);
			Assert.Equal(arr.Select(i => $"{i}"), updated.ArrayValue.Select(i => $"{i}"));
			Assert.True(updated.CreatedAt != updated.UpdatedAt);

			Assert.Single(RecordCollection.Find(All<TestRecord>()).ToEnumerable());
			Assert.Single(ChildCollection.Find(All<TestRecordChild>()).ToEnumerable());
		}

		[Fact]
		public async Task WhenWeMakeAnDeleteRequest_TheRequestsGoTroughTheGateway()
		{
			//Given
			RecordCollection.InsertOne(new("1") { Childs = new(new[] { "2" }) });

			//When
			await Client.DeleteAsync($"{nameof(TestRecord)}?ids=1");

			//Then
			Assert.Empty(RecordCollection.Find(All<TestRecord>()).ToEnumerable());
		}

		[Fact]
		public async Task WhenWeMakeAQueryRequestWithParameter_TheRecordsAreFilteredOrderedAndPaged()
		{
			// Given
			RecordCollection.InsertMany(
				new TestRecord[] {
					new("0") { IntValue = 2 },
					new("1") { IntValue = 1 },
					new("2") { IntValue = 2 },
					new("3") { IntValue = 1 },
					new("4") { IntValue = 2 },
					new("5"), new("6"), new("7"), new("8"), new("9")
				}
			);

			// When
			var result = await Client.GetAsync(
				$"{nameof(TestRecord)}/query" +
				"?filters=Id<>2&filters=Id<5" +
				$"&{nameof(OrderBys)}=IntValue&{nameof(OrderBys)}=-id" +
				"&paging.skip=2&paging.take=2"
			);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Query<TestRecord>>(content, JsonOptions);

			Assert.Equal(2, response.Data.Count());
			Assert.Equal("4", response.Data.First().Id);
			Assert.Equal("0", response.Data.Last().Id);
			Assert.Empty(response.Included);
		}
	}
}
