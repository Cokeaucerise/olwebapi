using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Docker.DotNet;
using Docker.DotNet.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Xunit;

namespace OLWebApi.Mongo.Gateway.Integration
{

	[CollectionDefinition("Mongo")]
	public class DockerMongoFixture : IDisposable
	{
		protected CreateContainerResponse Container { get; set; }
		protected DockerClient Client = new DockerClientConfiguration().CreateClient();
		protected IConfigurationRoot Configuration = new ConfigurationBuilder()
			.AddEnvironmentVariables()
			.AddJsonFile("appsettings.json")
			.Build();
		protected MongoClientSettings MongoSettings => MongoClientSettings.FromConnectionString(Configuration.GetConnectionString("Mongo"));

		public DockerMongoFixture()
		{
			try
			{
				Start(MongoSettings).Wait();
			}
			catch (Exception) { }
		}

		public async Task Start(MongoClientSettings settings)
		{
			Container ??= await Client.Containers.CreateContainerAsync(new CreateContainerParameters
			{
				Image = "mongo",
				Name = "Mongo",
				ExposedPorts = new Dictionary<string, EmptyStruct>() { { $"{settings.Server.Port}", default } },
				HostConfig = CreateHostConfig(settings.Server.Port)
			});

			await Client.Containers.StartContainerAsync(Container.ID, new ContainerStartParameters());
		}

		private static HostConfig CreateHostConfig(int port)
		{
			var portBinding = new PortBinding
			{
				HostIP = "0.0.0.0",
				HostPort = $"{port}"
			};

			return new HostConfig
			{
				PortBindings = new Dictionary<string, IList<PortBinding>>
				{
					{ $"{port}/tcp", new[] { portBinding } }
				}
			};
		}

		public void Dispose()
		{
			if (Container != null)
			{
				Client.Containers.StopContainerAsync(Container.ID, new ContainerStopParameters()).Wait();
				Client.Containers.RemoveContainerAsync(Container.ID, new ContainerRemoveParameters()).Wait();
			}

			GC.SuppressFinalize(this);
		}
	}
}