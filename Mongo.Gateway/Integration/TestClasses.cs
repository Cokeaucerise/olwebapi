using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Mongo.Gateway.Integration
{
	public record TestRecord : ApiRecord
	{
		public TestRecord() { }
		public TestRecord(string id) : base(id) { }
		public ManyRelations<TestRecordChild> Childs { get; set; }
		public int IntValue { get; set; }
		public double DoubleValue { get; set; }
		public string StringValue { get; set; }
		public TestRecordChild NestedObject { get; set; }
		public int[] ArrayValue { get; set; }
	}

	public record TestRecordChild : ApiRecord
	{
		public TestRecordChild() { }
		public TestRecordChild(string id) : base(id) { }
		public int Value { get; set; }
		public SingleRelation<TestParentRecord> Parent { get; set; }
	}

	public record TestParentRecord : ApiRecord
	{
		public TestParentRecord() { }
		public TestParentRecord(string id) : base(id) { }
		public SingleRelation<TestRecord> Child { get; set; }
		public ManyRelations<TestRecordChild> Childs { get; set; }
	}
}