# OLWebApi

## CI/CD

[![pipeline status](https://gitlab.com/Cokeaucerise/olwebapi/badges/master/pipeline.svg)](https://gitlab.com/Cokeaucerise/olwebapi/-/commits/master)

[![Tests line coverage](https://cokeaucerise.gitlab.io/olwebapi/Coverage/badge_linecoverage.svg)](https://cokeaucerise.gitlab.io/olwebapi/Coverage/index.html)
[![Tests branch coverage](https://cokeaucerise.gitlab.io/olwebapi/Coverage/badge_branchcoverage.svg)](https://cokeaucerise.gitlab.io/olwebapi/Coverage/index.html)

## Requirements

- [DotNet Core 5.0](https://www.microsoft.com/net/core)
- [dotnet-reportgenerator-globaltool](https://github.com/danielpalme/ReportGenerator)

## Structure

### Core

To start using the OLWebApi.Core it must be added to the `IServiceCollection` and the `UseWebApi()` must be called inside the Configure method of the `Startup` class.
```csharp
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddWebApi();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseWebApi();
    }
```

This project only provide the abstract models and general structure of the expected implementations. It doesn't have a functional stack.

#### Core - Structure

The OLWebApi is composed of 4 core layers

- `Controllers`
- `Interactors`
- `Repositories`
- `Gateways`

```shell
                                            ┌───────────────────┐
                                            │ EXTERNAL  REQUEST │
                                            └───────────────────┘
                                                │            ▲
                            Routing             │            │        Answer
                                                ▼            │
                    ┌───────────────────────┬───────────────────┬──────────────────────┐
                    │ Model validation      │    CONTROLLERS    │      Action Response │
                    └───────────────────────┴───────────────────┴──────────────────────┘
                                                │            ▲
                            Request             │            │        JSON API Model
                                                ▼            │
                    ┌───────────────────────┬───────────────────┬──────────────────────┐
                    │ Buisiness logic       │    INTERACTORS    │            Inclusion │
                    └───────────────────────┴───────────────────┴──────────────────────┘
                                                │            ▲
                            Query data          │            │        Cached record
                                                ▼            │
                    ┌───────────────────────┬───────────────────┬──────────────────────┐
                    │ Cache lookup          │    REPOSITORIES   │       record Caching │
                    └───────────────────────┴───────────────────┴──────────────────────┘
                                                │            ▲
                            Query data          │            │        record models
                                                ▼            │
                    ┌───────────────────────┬───────────────────┬──────────────────────┐
                    │ Connection            │     GATEWAYS      │     Model convertion │
                    └───────────────────────┴───────────────────┴──────────────────────┘
                                                │            ▲
                            Service Driver      │            │        Raw Data
                                                ▼            │
                                            ┌───────────────────┐
                                            │ EXTERNAL  SOURCES │
                                            └───────────────────┘
```

#### Core.Entities

This namespace contains all the basic entities used for the OLWebApi. Those are breakdown into 4 categories:
1. Action : Server input and output models
2. Includes : Model definition for the inclusion feature
3. Model : Models for the base record type and the posible relations
4. Query : Parameters for the queries

#### Core - Registering Controllers And Models

Any records that inherits from `ApiRecord` or implements `IApiRecord` can be added as a valid entry point for a controller.
This can be done with the use of the `IServiceCollection` methods:

```csharp
services
    .ForApiRecord<ExempleApiRecord>()
    .AddController<ExempleApiController<ExempleApiRecord>>();
```

Many controllers can be registered for a record in a single method chain.

```csharp
services
    .ForApiRecord<ExempleApiRecord>()
    .AddController<ExempleApiController<ExempleApiRecord>>()
    .AddController<AnotherApiController<ExempleApiRecord>>();
```

Another way of registering a controller is with the `RegisterForWebApiAttribute` attribute.
It can be used on a record by setting the type parameter with the open generic type of the desired controller to bind with the record:
```csharp
[RegisterForWebApi(typeof(ExempleApiController<>))]
public record ExempleApiRecord : ApiRecord {}
```

Or it can be used on a controller by setting the type parameter with the type of the desired record to bind with the controller:
```csharp
[RegisterForWebApi(typeof(ExempleApiRecord))]
public class ExempleApiController<T> : ApiController<T>
```

### CRUD

This project provide a functional stack that implement the Core specifications.
To start using the CRUD OLWebApi the process is the same as for the Core project, but the method for the `IServiceCollection` is `AddCrudWebApi()`.

```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddCrudWebApi();
}

public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
    app.UseWebApi();
}
```

The other important part is that the gateway must bt added to the `ServiceCollection` since it's the responsability of the user to implement it ([sample mongodb gateway implementation for CRUD](https://gitlab.com/Cokeaucerise/olwebapi/-/tree/master/Mongo.Gateway)).
The gateways can be added for every desired models, but it is recommended to regsiter the gateway as a open type generic via the `AddScoped()` or `AddScopedCrudApiGateway()` method.

#### CRUD - Controllers

The CRUD OLWebApi provide a controller that implement a `Create`, `Read` (get, query), `Update`, and `Delete` route.

#### Create

This action take in a collection of objects of the controller type and call the create method of the implemented Gateway for them.

#### Read

#### Read - Get

The `Get` action allow to read a single record with a specific `Id`.

#### Read - Query

The `Query` action allow to fetch many records with the specifications of many parameters.

- `ids`: A list of ids from witch to load the records. If empty, all records are selected.
- `paging`: The starting record index and the number of records to read.
- `filters`: A collection of `PropertyFilter` to apply to the queried records.
- `orderBys`: A colelction of `OrderBy` determining the order of the output records

Both action allow to include an abitrary number of relations into the request output with the `includes` parameter.
Ex:

Given those records type:
```csharp
public record Parent: ApiRecord
{
    public Relations<Child> Childs { get; set; }
}

public record Child: ApiRecord
{
    public Relations<Child> Friends { get; set; }
    public Relations<Toy> Toys { get; set; }
}

public record Toy: ApiRecord
{
    public string Name { get; set; }
    public double Price { get; set; }
}
```

It would be possible to get all the `Childs` of a `Parent` with:

    Parent/Get/{id}?includes=Childs.

If we want to know what are the `Toys` the `Friends` of the `Childs` of a `Parent` we can request:

    Parent/Get/{id}?includes=Childs.Friends.Toys.

If we want to know what are the `Toys` the `Friends` of the `Childs` of a `Parent` that cost less than 20$ we can request:

    Parent/Get/{id}?includes=Childs.Friends.Toys!price<20.

If we want to know who are the `Friends` of 'Timmy' we can request:

    Parent/Get/{id}?includes=Childs!name="Timmy".Friends.

*more details about the query parameter at the end*

#### Read - Report

The `Report` action allow to get the grouped values for a collection with the specification of the keys and the groups accumulator.

    Parent/Report?GroupBies.Keys=Lastname&GroupBies.Groups={output_name}:{accumulator}:{field}
    Parent/Report?GroupBies.Keys=Lastname&GroupBies.Groups=Count:count:Id

#### Update

This action take in a collection of update parts. A single update parts is a dictionary of string keys representing the property name and the new value. If a value is not specified, it will not be changed.

Ex:

Given this record
```csharp
public record ExempleApiRecord: ApiRecord
{
    public string Id = "my-old-id";
    public decimal Number = 100;
    public string Name = "My record";
}
```

When we update using this `Update` value
```json
{
    "Id": "my-new-string-id",
    "Number": 50
}
```

The updated record will look like this
```csharp
public record ExempleApiRecord: ApiRecord
{
    public string Id = "my-new-string-id";
    public decimal Number = 50;
    public string Name = "My record";
}
```

#### Delete

This action take in an array of ids for the records to delete.


### Parser

To help with the construction of request, a parser can be added that allow the construction of request with a natural language form.
To start using the Parser, the method for the `IServiceCollection` is `AddWebApiParser()`.

```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.AddCrudWebApi().AddWebApiParser();
}
```

#### Parser - Models

- `paging`: The paging can be added as a query parameter for a URL request with the following syntax: `paging.skip={skip}, paging.take={take}`
- `filters`: The filters can be added as a query parameter for a URL request with the following syntax: `filters={property_name}{filter_operator}{value}`
- `orderBy`: The orderby can be added as a query parameter for a URL request with the following syntax: `OrderBys=[-]{property_name}`
- `includes`: The includes can be added as a query parameter for a URL request with the following syntax: `includes={property_name}[!{filter}](.{inner_property_name}[!{filter}])`
