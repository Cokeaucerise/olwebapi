using System;
using Microsoft.Extensions.Configuration;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Benchmark
{
	public static class DataGenerator
	{
		public static IConfiguration Configuration { get; set; }

		private static readonly Random r = new();

		public static T NewApiRecord<T>(string id) where T : class, IApiRecord, new()
		{
			if (typeof(T) == typeof(Questionnaire))
				return NewQuestionnaire<T>(id);

			if (typeof(T) == typeof(Question))
				return NewQuestion<T>(id);

			if (typeof(T) == typeof(Answer))
				return NewAnswer<T>(id);

			return null;
		}

		private static T NewQuestionnaire<T>(string id) where T : class, IApiRecord, new()
		{
			return new Questionnaire(id)
			{
				Name = $"{nameof(Questionnaire)}-{id}",
				Number = r.Next(100),
				Group = r.Next(10),
				Group2 = r.Next(10),
				Questions = new(new[] { $"{id}-1", $"{id}-2", $"{id}-3" }),
				StartingQuestion = new($"{id}-1")
			} as T;
		}

		private static T NewQuestion<T>(string id) where T : class, IApiRecord, new()
		{
			string[] parts = id.Split("-");
			(int QuestionnaireId, int QuestionId) = (int.Parse(parts[0]), int.Parse(parts[1]));
			return new Question(id)
			{
				Name = $"{nameof(Question)}-{id}",
				DisplayQuestion = $"Question for {nameof(Questionnaire)}-{id}?",
				Number = r.Next(100),
				Answer = new($"{id}-1"),
				NextQuestion = new($"{QuestionnaireId}-{QuestionId + 1}"),
			} as T;
		}

		private static T NewAnswer<T>(string id) where T : class, IApiRecord, new()
		{
			string[] parts = id.Split("-");
			(int QuestionnaireId, int QuestionId, int answerid) = (int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]));
			return new Answer(id)
			{
				Name = $"{nameof(Question)}-{id}",
				DisplayAnswer = $"Answer for {nameof(Question)}-{QuestionnaireId}-{QuestionId}.",
				Value = r.NextDouble() * 100,
				AnswerType = string.Empty,
			} as T;
		}
	}
}