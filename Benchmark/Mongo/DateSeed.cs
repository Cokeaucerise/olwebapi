using System.Linq;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace OLWebApi.Benchmark.Mongo
{
	public class DataSeed
	{
		public static IMongoClient GetMongoClient()
		{
			var connection = DataGenerator.Configuration.GetConnectionString("mongo") ?? "mongodb://localhost:27017";
			var clientSettings = MongoClientSettings.FromConnectionString(connection);
			System.Console.WriteLine(clientSettings.ApplicationName);
			return new MongoClient(clientSettings);
		}

		public static void Seed(IMongoClient client)
		{
			client.DropDatabase("benchmark");
			InsertData(client.GetDatabase("benchmark"), 10);
		}

		private static void InsertData(IMongoDatabase db, double scale = 1)
		{
			var questionnaires = Enumerable.Range(1, (int)(1000 * scale)).Select(i => DataGenerator.NewApiRecord<Questionnaire>(i.ToString()));
			var questions = questionnaires.SelectMany(q => q.Questions.Ids).Select(id => DataGenerator.NewApiRecord<Question>(id));
			var answers = questions.Select(q => q.Answer.Id).Select(id => DataGenerator.NewApiRecord<Answer>(id));

			db.GetCollection<Questionnaire>(nameof(Questionnaire)).InsertMany(questionnaires);
			db.GetCollection<Question>(nameof(Question)).InsertMany(questions);
			db.GetCollection<Answer>(nameof(Answer)).InsertMany(answers);
		}
	}
}