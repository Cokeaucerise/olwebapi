﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Order;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Cache.Gateway.Cache;
using OLWebApi.Crud.Server.Extensions;
using OLWebApi.Mongo.Gateway;
using OLWebApi.Query.Controller;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Parser.Extensions;
using OLWebApi.Query.Server.Extensions;

namespace OLWebApi.Benchmark.Mongo
{
	[RPlotExporter]
	[IterationCount(5)]
	[MemoryDiagnoser, ThreadingDiagnoser]
	// [Config(typeof(DebugInProcessConfig))]
	[Orderer(SummaryOrderPolicy.Declared, MethodOrderPolicy.Declared)]
	public class BenchmarkQueryMongoJob : BenchmarkQueryJob
	{
		[GlobalSetup]
		public void Setup()
		{
			Services = new ServiceCollection()
				.AddCrudWebApi()
				.AddQueryWebApi()
				.AddQueryParser()
				.AddMongoGateway()
				.AddSingleton(p => DataSeed.GetMongoClient().GetDatabase("benchmark"));

			Client = Services.BuildServiceProvider().GetService<IQueryApiController<Questionnaire>>();
		}
	}


	[RPlotExporter]
	[IterationCount(5)]
	[MemoryDiagnoser, ThreadingDiagnoser]
	// [Config(typeof(DebugInProcessConfig))]
	[Orderer(SummaryOrderPolicy.Declared, MethodOrderPolicy.Declared)]
	public class BenchmarkReportMongoJob : BenchmarkReportJob
	{
		[GlobalSetup]
		public void Setup()
		{
			Services = new ServiceCollection()
				.AddCrudWebApi()
				.AddQueryWebApi()
				.AddQueryParser()
				.AddMongoGateway()
				.AddSingleton(p => DataSeed.GetMongoClient().GetDatabase("benchmark"));

			Client = Services.BuildServiceProvider().GetService<IQueryApiController<Questionnaire>>();
		}
	}

	[RPlotExporter]
	[IterationCount(5)]
	[MemoryDiagnoser, ThreadingDiagnoser]
	// [Config(typeof(DebugInProcessConfig))]
	[Orderer(SummaryOrderPolicy.Declared, MethodOrderPolicy.Declared)]
	public class BenchmarkCacheQueryMongoJob : BenchmarkQueryJob
	{
		[GlobalSetup]
		public void Setup()
		{
			Services = new ServiceCollection()
				.AddCrudWebApi()
				.AddQueryWebApi()
				.AddQueryParser()
				.AddMongoGateway()
				.AddCacheGatewayDecoration()
				.DecorateQueryGatewayWithCache<MongoGateway<Questionnaire>, Questionnaire>()
				.DecorateQueryGatewayWithCache<MongoGateway<Question>, Question>()
				.DecorateQueryGatewayWithCache<MongoGateway<Answer>, Answer>()
				.AddSingleton(p => DataSeed.GetMongoClient().GetDatabase("benchmark"));

			Client = Services.BuildServiceProvider().GetService<IQueryApiController<Questionnaire>>();
		}
	}
}
