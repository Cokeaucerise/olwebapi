using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Benchmark
{
	public record BenchmarkReportParameter : ReportParameter
	{
		public string Name { get; }

		public Includes Includes { get; set; }

		public BenchmarkReportParameter(string name)
		{
			Name = name;
		}

		public override string ToString() => Name;
	}
}
