﻿using System;
using BenchmarkDotNet.Attributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Query.Controller;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Benchmark
{
	public class BenchmarkQueryJob
	{
		public IServiceCollection Services { get; set; }
		public IQueryApiController<Questionnaire> Client { get; set; }

		[ParamsSource(nameof(Parameters))]
		public BenchmarkQueryParameter Parameter { get; set; }
		public BenchmarkQueryParameter[] Parameters =>
			new BenchmarkQueryParameter[] {
				new("empty"),
				new("ob:n")         { OrderBys = new() { nameof(Questionnaire.Number) } },
				new("f:n>=50")      { Filters = new() { new PropertyFilter<FilterGreaterOrEqualOperator>(nameof(Questionnaire.Number), "50") } },
				new("in:q")         { Includes = new() { { nameof(Questionnaire.Questions) } } },
				new("in:q.a")       { Includes = new() { { nameof(Questionnaire.Questions), nameof(Question.Answer) } } },
				new("in:*q.a")      { Includes = new() { new PropertyChain { new(nameof(Questionnaire.Questions)) { Include = true }, new(nameof(Question.Answer)) } } },
				new("in:*q!n<=50.a") {
					Includes = new() {
						new PropertyChain() {
							new(nameof(Questionnaire.Questions)) {
								Include = true,
								Filters = new() { new PropertyFilter<FilterLessOrEqualOperator>(nameof(Questionnaire.Number), "50") }
							},
							new(nameof(Question.Answer))
						}
					}
				},
				new("in:*q.a!n<=50") {
					Includes = new() {
						new PropertyChain() {
							new(nameof(Questionnaire.Questions)) { Include = true },
							new(nameof(Question.Answer)) {
								Filters = new() { new PropertyFilter<FilterLessOrEqualOperator>(nameof(Answer.Value), "50") }
							}
						}
					}
				},
				new("in:*q.a!n>=25!n<=50") {
					Includes = new() {
						new PropertyChain() {
							new(nameof(Questionnaire.Questions)) { Include = true },
							new(nameof(Question.Answer)) {
								Filters = new() {
									new PropertyFilter<FilterGreaterOrEqualOperator>(nameof(Answer.Value), "25"),
									new PropertyFilter<FilterLessOrEqualOperator>(nameof(Answer.Value), "50")
								}
							}
						}
					}
				}
			};

		[Benchmark]
		public virtual ActionResult<Query<Questionnaire>> Query_1000()
		{
			return Query(1000);
		}

		protected ActionResult<Query<Questionnaire>> Query(int size)
		{
			return Client.Query(
				Array.Empty<string>(),
				Parameter.Paging with { Take = size },
				Parameter.Filters,
				Parameter.OrderBys,
				Parameter.Includes
			).Result;
		}
	}
}
