﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Order;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Crud.Server.Extensions;
using OLWebApi.Query.Controller;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Gateway;
using OLWebApi.Query.Parser.Extensions;
using OLWebApi.Query.Server.Extensions;

namespace OLWebApi.Benchmark.Memory
{
	[RPlotExporter]
	[IterationCount(5)]
	// [Config(typeof(DebugInProcessConfig))]
	[Orderer(SummaryOrderPolicy.Declared, MethodOrderPolicy.Declared)]
	public class BenchmarkQueryMemoryJob : BenchmarkQueryJob
	{
		[GlobalSetup]
		public void Setup()
		{
			Services = new ServiceCollection();
			Services.AddQueryWebApi().AddQueryParser();

			Services.AddScoped(typeof(IQueryApiController<>), typeof(QueryApiController<>));
			Services.AddScoped(typeof(IQueryApiGateway<>), typeof(BenchmarkApiMemoryGateway<>));

			Client = Services.BuildServiceProvider().GetService<IQueryApiController<Questionnaire>>();
		}
	}
}
