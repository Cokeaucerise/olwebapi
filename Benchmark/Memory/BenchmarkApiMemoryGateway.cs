using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Gateway;

namespace OLWebApi.Benchmark.Memory
{
	public class BenchmarkApiMemoryGateway<T> : IQueryApiGateway<T>
	where T : class, IApiRecord, new()
	{
		public Task<T> Get(string id) => Task.FromResult(DataGenerator.NewApiRecord<T>(id));

		public IAsyncEnumerable<T> Query(QueryParameter parameter) => GetIdsOrGenerateFromPaging(parameter).Select(id => DataGenerator.NewApiRecord<T>(id)).ToAsyncEnumerable();

		public Task<Report> Report(ReportParameter parameter)
		{
			throw new System.NotImplementedException();
		}

		private static IEnumerable<string> GetIdsOrGenerateFromPaging(QueryParameter parameter)
		{
			if (parameter.Ids.Any())
				return parameter.Ids;

			return Enumerable.Range(0, parameter?.Paging?.Take ?? 0).Select(i => $"{i}");
		}

	}
}