using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Benchmark
{
	public record Questionnaire : ApiRecord
	{
		public Questionnaire() { }
		public Questionnaire(string id) : base(id) { }

		public string Name { get; set; }
		public int Number { get; set; }
		public int Group { get; set; }
		public int Group2 { get; set; }
		public SingleRelation<Question> StartingQuestion { get; set; }
		public ManyRelations<Question> Questions { get; set; }
	}

	public record Question : ApiRecord
	{
		public Question() { }
		public Question(string id) : base(id) { }

		public string Name { get; set; }
		public string DisplayQuestion { get; set; }
		public int Number { get; set; }
		public SingleRelation<Answer> Answer { get; set; }
		public SingleRelation<Question> NextQuestion { get; set; }
	}

	public record Answer : ApiRecord
	{
		public Answer() { }
		public Answer(string id) : base(id) { }
		public string Name { get; set; }
		public string DisplayAnswer { get; set; }
		public string AnswerType { get; set; }
		public double Value { get; set; }
	}
}