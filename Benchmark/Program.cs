using System;
using BenchmarkDotNet.Running;
using Microsoft.Extensions.Configuration;
using OLWebApi.Benchmark;
using OLWebApi.Benchmark.Memory;
using OLWebApi.Benchmark.Mongo;
using OLWebApi.Benchmark.Sql;

DataGenerator.Configuration = new ConfigurationBuilder()
	.AddUserSecrets<BenchmarkQueryJob>()
	.AddCommandLine(args)
	.AddEnvironmentVariables()
	.Build();

_ = new DockerMongoFixture();
_ = new DockerSqlFixture();

OLWebApi.Benchmark.Mongo.DataSeed.Seed(OLWebApi.Benchmark.Mongo.DataSeed.GetMongoClient());
OLWebApi.Benchmark.Sql.DataSeed.Seed(OLWebApi.Benchmark.Sql.DataSeed.GetContext());

Console.WriteLine(BenchmarkRunner.Run<BenchmarkQueryMemoryJob>());

Console.WriteLine(BenchmarkRunner.Run<BenchmarkQueryMongoJob>());
Console.WriteLine(BenchmarkRunner.Run<BenchmarkReportMongoJob>());
Console.WriteLine(BenchmarkRunner.Run<BenchmarkCacheQueryMongoJob>());

Console.WriteLine(BenchmarkRunner.Run<BenchmarkQuerySqlJob>());
Console.WriteLine(BenchmarkRunner.Run<BenchmarkReportSqlJob>());
Console.WriteLine(BenchmarkRunner.Run<BenchmarkCacheQuerySqlJob>());
