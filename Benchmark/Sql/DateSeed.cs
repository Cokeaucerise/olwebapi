using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace OLWebApi.Benchmark.Sql
{
	public class DataSeed
	{
		public static BenchmarkContext GetContext()
		{
			var options = new DbContextOptionsBuilder().UseNpgsql(DockerSqlFixture.ConnectionString).Options;

			return new BenchmarkContext(options);
		}

		public static void Seed(BenchmarkContext context)
		{
			context.Database.EnsureDeleted();
			context.Database.EnsureCreated();

			InsertData(context, 10);
		}

		private static void InsertData(BenchmarkContext db, double scale = 1)
		{
			var questionnaires = Enumerable.Range(1, (int)(1000 * scale)).Select(i => DataGenerator.NewApiRecord<Questionnaire>(i.ToString()));
			var questions = questionnaires.SelectMany(q => q.Questions.Ids).Select(id => DataGenerator.NewApiRecord<Question>(id));
			var answers = questions.Select(q => q.Answer.Id).Select(id => DataGenerator.NewApiRecord<Answer>(id));

			db.Answers.AddRange(answers);
			db.Questions.AddRange(questions);
			db.Questionnaires.AddRange(questionnaires);

			db.SaveChanges();
		}
	}
}