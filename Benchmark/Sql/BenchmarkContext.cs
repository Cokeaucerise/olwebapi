using Microsoft.EntityFrameworkCore;
using OLWebApi.Sql.Gateway;

namespace OLWebApi.Benchmark.Sql
{
	public class BenchmarkContext : ApiDbContext
	{
		public DbSet<Questionnaire> Questionnaires { get; set; }
		public DbSet<Question> Questions { get; set; }
		public DbSet<Answer> Answers { get; set; }

		protected BenchmarkContext() { }
		public BenchmarkContext(DbContextOptions options) : base(options) { }
	}
}