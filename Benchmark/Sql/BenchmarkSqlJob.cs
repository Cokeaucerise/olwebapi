﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Order;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Cache.Gateway.Cache;
using OLWebApi.Crud.Server.Extensions;
using OLWebApi.Query.Controller;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Parser.Extensions;
using OLWebApi.Query.Server.Extensions;
using OLWebApi.Sql.Gateway;

namespace OLWebApi.Benchmark.Sql
{
	[RPlotExporter]
	[IterationCount(5)]
	[MemoryDiagnoser, ThreadingDiagnoser]
	[Config(typeof(DebugInProcessConfig))]
	[Orderer(SummaryOrderPolicy.Declared, MethodOrderPolicy.Declared)]
	public class BenchmarkQuerySqlJob : BenchmarkQueryJob
	{
		[GlobalSetup]
		public void Setup()
		{
			Services = new ServiceCollection()
				.AddCrudWebApi()
				.AddQueryWebApi()
				.AddQueryParser()
				.AddSqlGateway()
				.AddDbContext<BenchmarkContext>(o => o.UseNpgsql(DockerSqlFixture.ConnectionString))
				.AddTransient<DbContext>(p => p.GetService<BenchmarkContext>());

			Client = Services.BuildServiceProvider().GetService<IQueryApiController<Questionnaire>>();
		}
	}

	[RPlotExporter]
	[IterationCount(5)]
	[MemoryDiagnoser, ThreadingDiagnoser]
	[Config(typeof(DebugInProcessConfig))]
	[Orderer(SummaryOrderPolicy.Declared, MethodOrderPolicy.Declared)]
	public class BenchmarkReportSqlJob : BenchmarkReportJob
	{
		[GlobalSetup]
		public void Setup()
		{
			Services = new ServiceCollection()
				.AddCrudWebApi()
				.AddQueryWebApi()
				.AddQueryParser()
				.AddSqlGateway()
				.AddDbContext<BenchmarkContext>(o => o.UseNpgsql(DockerSqlFixture.ConnectionString))
				.AddTransient<DbContext>(p => p.GetService<BenchmarkContext>());

			Client = Services.BuildServiceProvider().GetService<IQueryApiController<Questionnaire>>();
		}
	}

	[RPlotExporter]
	[IterationCount(5)]
	[MemoryDiagnoser, ThreadingDiagnoser]
	[Config(typeof(DebugInProcessConfig))]
	[Orderer(SummaryOrderPolicy.Declared, MethodOrderPolicy.Declared)]
	public class BenchmarkCacheQuerySqlJob : BenchmarkQueryJob
	{
		[GlobalSetup]
		public void Setup()
		{
			Services = new ServiceCollection()
				.AddCrudWebApi()
				.AddQueryWebApi()
				.AddQueryParser()
				.AddSqlGateway()
				.AddCacheGatewayDecoration()
				.DecorateQueryGatewayWithCache<SqlGateway<Questionnaire>, Questionnaire>()
				.DecorateQueryGatewayWithCache<SqlGateway<Question>, Question>()
				.DecorateQueryGatewayWithCache<SqlGateway<Answer>, Answer>()
				.AddDbContext<BenchmarkContext>(o => o.UseNpgsql(DockerSqlFixture.ConnectionString))
				.AddTransient<DbContext>(p => p.GetService<BenchmarkContext>());

			Client = Services.BuildServiceProvider().GetService<IQueryApiController<Questionnaire>>();
		}
	}

}
