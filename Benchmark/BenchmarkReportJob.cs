﻿using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Query.Controller;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Benchmark
{
	public class BenchmarkReportJob
	{
		public IServiceCollection Services { get; set; }
		public IQueryApiController<Questionnaire> Client { get; set; }

		[ParamsSource(nameof(Parameters))]
		public BenchmarkReportParameter Parameter { get; set; }
		public BenchmarkReportParameter[] Parameters =>
			new BenchmarkReportParameter[] {
				new("1_group:sum") {
					GroupBies = new GroupBies {
						Keys = new HashSet<string> { nameof(Questionnaire.Group) },
						Groups = new List<GroupBy> { new GroupBy<SumAccumulator>("sum", nameof(Questionnaire.Number)) }
					}
				},
				new("2_group:sum") {
					GroupBies = new GroupBies {
						Keys = new HashSet<string> { nameof(Questionnaire.Group), nameof(Questionnaire.Group2) },
						Groups = new List<GroupBy> { new GroupBy<SumAccumulator>("sum", nameof(Questionnaire.Number)) }
					}
				},
				new("2_group:sum:count") {
					GroupBies = new GroupBies {
						Keys = new HashSet<string> { nameof(Questionnaire.Group), nameof(Questionnaire.Group2) },
						Groups = new List<GroupBy> {
							new GroupBy<SumAccumulator>("sum", nameof(Questionnaire.Number)),
							new GroupBy<CountAccumulator>("count", string.Empty)
						}
					}
				}
			};

		[Benchmark]
		public virtual ActionResult<Report> Report_1000()
		{
			return Report(1000);
		}

		protected ActionResult<Report> Report(int size)
		{
			return Client.Report(Parameter with { Paging = new(size) }).Result;
		}
	}
}
