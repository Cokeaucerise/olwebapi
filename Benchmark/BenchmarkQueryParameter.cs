using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Benchmark
{
	public record BenchmarkQueryParameter : QueryParameter
	{
		public string Name { get; }

		public Includes Includes { get; set; }

		public BenchmarkQueryParameter(string name)
		{
			Name = name;
		}

		public override string ToString() => Name;
	}
}
