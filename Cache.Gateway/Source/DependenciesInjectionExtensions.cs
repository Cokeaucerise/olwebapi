using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using OLWebApi.Caching;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Gateway.Queryable;
using OLWebApi.Query.Gateway;

namespace OLWebApi.Cache.Gateway.Cache
{
	public static class DependenciesInjectionExtensions
	{
		public static IServiceCollection AddCacheGatewayDecoration(this IServiceCollection services)
		{
			services.TryAddSingleton(typeof(IMemoryCache<>), typeof(MemoryCache<>));
			services.TryAddSingleton(typeof(IQueryableMemoryCache<>), typeof(QueryableMemoryCacheDecorator<>));

			services.AddQueryableGateway();

			return services;
		}

		public static IServiceCollection DecorateCrudGatewayWithCache<TGateway, TRecord>(this IServiceCollection services)
		where TGateway : class, ICrudApiGateway<TRecord>
		where TRecord : IApiRecord
		{
			services.TryAddScoped<ICrudApiGateway<TRecord>, TGateway>();
			return services.Decorate<ICrudApiGateway<TRecord>, CacheCrudApiGatewayDecorator<TRecord>>();
		}

		public static IServiceCollection DecorateQueryGatewayWithCache<TGateway, TRecord>(this IServiceCollection services)
		where TGateway : class, IQueryApiGateway<TRecord>
		where TRecord : IApiRecord
		{
			services.TryAddScoped<IQueryApiGateway<TRecord>, TGateway>();
			return services.Decorate<IQueryApiGateway<TRecord>, CacheQueryApiGatewayDecorator<TRecord>>();
		}
	}
}