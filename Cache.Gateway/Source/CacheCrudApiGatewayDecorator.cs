using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Caching;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway;

namespace OLWebApi.Cache.Gateway
{
	public class CacheCrudApiGatewayDecorator<T> : ICrudApiGateway<T>
	where T : IApiRecord
	{
		protected ICrudApiGateway<T> Gateway { get; }
		protected IMemoryCache<T> Cache { get; }

		public CacheCrudApiGatewayDecorator(ICrudApiGateway<T> gateway, IMemoryCache<T> cache)
		{
			Gateway = gateway;
			Cache = cache;
		}

		public Task<T> Get(string id)
		{
			return Cache.GetOrCreateAsync(id, _ => Gateway.Get(id));
		}

		public async IAsyncEnumerable<T> Create(IEnumerable<T> records)
		{
			await foreach (var record in Gateway.Create(records))
				yield return Cache.Set(record.Id, record);
		}

		public async IAsyncEnumerable<T> Update(Updates<T> updates)
		{
			await foreach (var record in Gateway.Update(updates))
				yield return Cache.Set(record.Id, record);
		}

		public async Task<bool> Delete(IEnumerable<string> ids)
		{
			var deleted = await Gateway.Delete(ids);

			if (deleted)
				foreach (var record in ids)
					Cache.Remove(record);

			return deleted;
		}
	}
}