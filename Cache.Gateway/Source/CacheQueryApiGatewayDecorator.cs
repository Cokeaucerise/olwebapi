using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Caching;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway.Queryable;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Gateway;
using OLWebApi.Queryable.Gateway.Accumulators;

namespace OLWebApi.Cache.Gateway
{
	public class CacheQueryApiGatewayDecorator<T> : QueryableGateway<T>, IQueryApiGateway<T>
	where T : IApiRecord
	{
		protected IQueryApiGateway<T> Gateway { get; }
		protected IQueryableMemoryCache<T> Cache { get; }

		public CacheQueryApiGatewayDecorator(
			IQueryApiGateway<T> gateway,
			IQueryableMemoryCache<T> cache,
			QueryableGatewayServices<T> services
		) : base(() => cache.AsQueryable(), services)
		{
			Gateway = gateway;
			Cache = cache;
		}

		public override IAsyncEnumerable<T> Query(QueryParameter parameter)
		{
			return (parameter?.Ids?.Any() ?? false) ?
				QueryFromIds(parameter) :
				QueryFromParameter(parameter);
		}

		private async IAsyncEnumerable<T> QueryFromIds(QueryParameter parameter)
		{
			var cached = Cache.Get(parameter.Ids).ToArray();
			foreach (var record in cached)
				yield return record;

			if (cached.Length >= parameter.Ids.Count())
				yield break;

			var missing = parameter.Ids.Except(cached.Select(c => c.Id)).ToArray();
			var idsParameters = new QueryParameter { Ids = missing, Paging = new(missing.Length) };
			await foreach (var record in this.CacheGatewayQuery(idsParameters))
				yield return record;
		}

		private async IAsyncEnumerable<T> QueryFromParameter(QueryParameter parameter)
		{
			var result = await base.Query(parameter).ToArrayAsync();
			foreach (var record in result)
				yield return record;

			if (result.Length >= parameter.Paging.Take)
				yield break;

			parameter = ExcludeResultFromQuery(parameter, result);
			await foreach (var record in CacheGatewayQuery(parameter))
				yield return record;
		}

		private async IAsyncEnumerable<T> CacheGatewayQuery(QueryParameter parameter)
		{
			await foreach (var record in Gateway.Query(parameter) ?? AsyncEnumerable.Empty<T>())
				yield return Cache.Set(record.Id, record);
		}

		public override Task<Report> Report(ReportParameter parameter)
		{
			return base.Report(parameter);
		}

		private static QueryParameter ExcludeResultFromQuery(QueryParameter parameter, IEnumerable<T> result)
		{
			var idsExclude = result.Select(r => new PropertyFilter<FilterNotEqualOperator>(nameof(IApiRecord.Id), r.Id));

			parameter.Filters.AddRange(idsExclude);
			parameter.Paging = parameter.Paging with { Take = parameter.Paging.Take - result.Count() };

			return parameter;
		}
	}
}