using System;
using Microsoft.Extensions.Caching.Memory;
using Moq;
using OLWebApi.Cache.Gateway;
using OLWebApi.Caching;
using OLWebApi.Core.Provider;

namespace OLWebApi.Crud.Gateway.Cache.Tests
{
	public abstract class CacheGatewayTest : IDisposable
	{
		protected IQueryableMemoryCache<TestRecord> Cache = new QueryableMemoryCacheDecorator<TestRecord>(new MemoryCache<TestRecord>(new MemoryCacheOptions()));
		protected Mock<ICrudApiGateway<TestRecord>> InnerGateway = new();
		// protected IQueryFilterBuilder<TestRecord> FilterBuilder;
		// protected IQueryOrderByBuilder<TestRecord> OrderByBuilder;
		// protected IEnumerable<IQueryablePropertyFilterer> Filterers;
		protected IPropertyValueProvider ValueProvider = new CaseInsensitivePropertyValueProvider();

		protected ICrudApiGateway<TestRecord> Gateway => new CacheCrudApiGatewayDecorator<TestRecord>(InnerGateway.Object, Cache);

		// public CacheGatewayTest()
		// {
		// 	Filterers = new IQueryablePropertyFilterer[] {
		// 		new EqualFilterer(),
		// 		new NotEqualFilterer(),
		// 		new LessThanFilterer(),
		// 		new GreaterThanFilterer(),
		// 		new LessThanOrEqualFilterer(),
		// 		new GreaterThanOrEqualFilterer()
		// 	};
		// 	FilterBuilder = new QueryFilterBuilder<TestRecord>(Filterers);

		// 	OrderByBuilder = new QueryOrderByBuilder<TestRecord>();
		// }

		public void Dispose()
		{
			Cache.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}
