// using System;
// using System.Linq;
// using System.Threading.Tasks;
// using Moq;
// using OLWebApi.Core.Entities.Query;
// using Xunit;

// namespace OLWebApi.Crud.Gateway.Cache.Tests
// {
// 	public class CacheGatewayQueryTest : CacheGatewayTest
// 	{
// 		[Fact]
// 		public async Task Query_CacheEmpty_CallGateway()
// 		{
// 			//Given
// 			QueryParameter parameter = new()
// 			{
// 				Paging = new(0, 1)
// 			};

// 			//When
// 			var result = await Gateway.Query(parameter).ToListAsync();

// 			//Then
// 			InnerGateway.Verify(g => g.Query(It.IsAny<QueryParameter>()), Times.Once());
// 		}

// 		[Fact]
// 		public async Task Query_CacheNotEmpty_DontCallGateway()
// 		{
// 			//Given
// 			Cache.Set("test", new());
// 			QueryParameter parameter = new()
// 			{
// 				Paging = new(0, 1)
// 			};

// 			//When
// 			var result = await Gateway.Query(parameter).ToListAsync();

// 			//Then
// 			InnerGateway.Verify(g => g.Query(It.IsAny<QueryParameter>()), Times.Never());
// 		}

// 		[Fact]
// 		public async Task Query_PartialCache_CallGatewayForMissingRecordsAndAddToCache()
// 		{
// 			//Given
// 			Cache.Set("test", new());
// 			InnerGateway
// 				.Setup(g => g.Query(It.IsAny<QueryParameter>()))
// 				.Returns(new[] { new TestRecord { Id = "gateway" } }.ToAsyncEnumerable());
// 			QueryParameter parameter = new() { Paging = new(0, 2) };

// 			//When
// 			var result = await Gateway.Query(parameter).ToListAsync();

// 			//Then
// 			InnerGateway.Verify(g => g.Query(It.Is<QueryParameter>(p => p.Paging.Take == 1)), Times.Once());
// 			Assert.Equal(2, Cache.AsQueryable().Count());
// 		}


// 		[Fact]
// 		public async Task Query_ParameterHavesIds_GetRecordWithIdsFromCache()
// 		{
// 			//Given
// 			Cache.Set("test", new());
// 			QueryParameter parameter = new() { Ids = new[] { "test" }, Paging = new(0, 1) };

// 			//When
// 			var result = await Gateway.Query(parameter).ToListAsync();

// 			//Then
// 			InnerGateway.Verify(g => g.Query(It.IsAny<QueryParameter>()), Times.Never());
// 		}
// 	}
// }