using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace OLWebApi.Crud.Gateway.Cache.Tests
{
	public class CacheGatewayDeleteTest : CacheGatewayTest
	{
		[Fact]
		public async Task Remove_DeleteIsSuccessAndCacheEmpty_CacheStayEmpty()
		{
			//Given
			var deletes = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
			InnerGateway.Setup(g => g.Delete(It.IsAny<IEnumerable<string>>())).ReturnsAsync(true);

			//When
			var result = await Gateway.Delete(deletes);

			//Then
			InnerGateway.Verify(g => g.Delete(It.IsAny<IEnumerable<string>>()), Times.Once());
			Assert.Empty(Cache.AsQueryable());
		}

		[Fact]
		public async Task Remove_DeleteIsSuccessAndCacheHaveRecord_CacheRemoveRecord()
		{
			//Given
			var deletes = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
			Cache.Set(deletes.First(), new TestRecord(deletes.First()));
			Cache.Set(deletes.Last(), new TestRecord(deletes.Last()));
			InnerGateway.Setup(g => g.Delete(It.IsAny<IEnumerable<string>>())).ReturnsAsync(true);

			//When
			var result = await Gateway.Delete(deletes);

			//Then
			InnerGateway.Verify(g => g.Delete(It.IsAny<IEnumerable<string>>()), Times.Once());
			Assert.Empty(Cache.AsQueryable());
		}

		[Fact]
		public async Task Remove_DeleteIsNotSuccessAndCacheHaveRecord_CacheDontRemoveRecord()
		{
			//Given
			var deletes = new[] { Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
			Cache.Set(deletes.First(), new TestRecord(deletes.First()));
			Cache.Set(deletes.Last(), new TestRecord(deletes.Last()));
			InnerGateway.Setup(g => g.Delete(It.IsAny<IEnumerable<string>>())).ReturnsAsync(false);

			//When
			var result = await Gateway.Delete(deletes);

			//Then
			InnerGateway.Verify(g => g.Delete(It.IsAny<IEnumerable<string>>()), Times.Once());
			Assert.Equal(deletes.Length, Cache.AsQueryable().Count());
		}
	}
}