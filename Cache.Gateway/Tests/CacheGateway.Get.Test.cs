using System;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace OLWebApi.Crud.Gateway.Cache.Tests
{
	public class CacheGatewayGetTest : CacheGatewayTest
	{
		[Fact]
		public async Task Get_CacheIsEmpty_CallInnerGatewayAndRecordCached()
		{
			//Given
			const string key = "test";

			//When
			var record = await Gateway.Get(key);

			//Then
			InnerGateway.Verify(g => g.Get(It.IsAny<string>()), Times.Once());
			Assert.True(Cache.TryGetValue(key, out _));
		}

		[Fact]
		public async Task Get_CacheHaveRecord_DontCallInnerGateway()
		{
			//Given
			TestRecord record = new() { Id = Guid.NewGuid().ToString() };
			Cache.Set(record.Id, record);

			//When
			var result = await Gateway.Get(record.Id);

			//Then
			Assert.Equal(record, result);
			InnerGateway.Verify(g => g.Get(It.IsAny<string>()), Times.Never());
		}
	}
}