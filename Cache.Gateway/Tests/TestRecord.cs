using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Crud.Gateway.Cache.Tests
{
	public record TestRecord : ApiRecord
	{
		public string String { get; set; }
		public double Double { get; set; }

		public TestRecord() { }
		public TestRecord(ApiRecord original) : base(original) { }
		public TestRecord(string id) : base(id) { }
	}
}
