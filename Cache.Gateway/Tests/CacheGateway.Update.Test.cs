using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Entities.Action;
using Xunit;

namespace OLWebApi.Crud.Gateway.Cache.Tests
{
	public class CacheGatewayUpdateTest : CacheGatewayTest
	{
		[Fact]
		public async Task Update_CacheEmpty_AddUpdatedToCache()
		{
			//Given
			var updated = new TestRecord[] { new(Guid.NewGuid().ToString()), new(Guid.NewGuid().ToString()) };
			InnerGateway
				.Setup(g => g.Update(It.IsAny<Updates<TestRecord>>()))
				.Returns(updated.ToAsyncEnumerable());

			//When
			var result = await Gateway.Update(default).ToListAsync();

			//Then
			InnerGateway.Verify(g => g.Update(It.IsAny<Updates<TestRecord>>()), Times.Once());
			Assert.True(updated.SequenceEqual(Cache.AsQueryable()));
		}

		[Fact]
		public async Task Update_CacheWithExistingModel_UpdateCache()
		{
			//Given
			var updated = new TestRecord[] { new(Guid.NewGuid().ToString()), new(Guid.NewGuid().ToString()) };
			InnerGateway
				.Setup(g => g.Update(It.IsAny<Updates<TestRecord>>()))
				.Returns(updated.ToAsyncEnumerable());
			Cache.Set(updated.First().Id, new TestRecord("not same 1"));
			Cache.Set(updated.Last().Id, new TestRecord("not same 2"));

			//When
			var result = await Gateway.Update(default).ToListAsync();

			//Then
			InnerGateway.Verify(g => g.Update(It.IsAny<Updates<TestRecord>>()), Times.Once());
			Assert.True(updated.SequenceEqual(Cache.AsQueryable()));
		}
	}
}