using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using Xunit;

namespace OLWebApi.Crud.Gateway.Cache.Tests
{
	public class CacheGatewayCreateTest : CacheGatewayTest
	{
		[Fact]
		public async Task Create_CacheEmpty_AddCreatedToCache()
		{
			//Given
			var createRecord = new TestRecord[] { new(Guid.NewGuid().ToString()), new(Guid.NewGuid().ToString()) };
			InnerGateway.Setup(g => g.Create(It.IsAny<IEnumerable<TestRecord>>())).Returns(createRecord.ToAsyncEnumerable());

			//When
			var result = await Gateway.Create(createRecord).ToListAsync();

			//Then
			InnerGateway.Verify(g => g.Create(It.IsAny<IEnumerable<TestRecord>>()), Times.Once());
			Assert.True(createRecord.SequenceEqual(Cache.AsQueryable()));
		}

		[Fact]
		public async Task Create_CacheWithExistingRecord_UpdateCache()
		{
			//Given
			var creates = new TestRecord[] { new(Guid.NewGuid().ToString()), new(Guid.NewGuid().ToString()) };
			InnerGateway.Setup(g => g.Create(It.IsAny<IEnumerable<TestRecord>>())).Returns(creates.ToAsyncEnumerable());
			Cache.Set(creates.First().Id, new TestRecord("not same 1"));
			Cache.Set(creates.Last().Id, new TestRecord("not same 2"));

			//When
			var result = await Gateway.Create(creates).ToListAsync();

			//Then
			InnerGateway.Verify(g => g.Create(It.IsAny<IEnumerable<TestRecord>>()), Times.Once());
			Assert.True(creates.SequenceEqual(Cache.AsQueryable()));
		}
	}
}