using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;

namespace OLWebApi.Caching
{
	public interface IQueryableMemoryCache<T> : IMemoryCache<T>
	{
		IQueryable<T> AsQueryable();
	}

	public class QueryableMemoryCacheDecorator<T> : IQueryableMemoryCache<T>
	{
		protected IMemoryCache<T> Cache { get; }
		private readonly HashSet<object> Keys = new();

		public QueryableMemoryCacheDecorator(IMemoryCache<T> cache)
		{
			Cache = cache;
		}

		public T Get(object key)
		{
			return GetWithKey(key, () => Cache.Get(key));
		}

		public IEnumerable<T> Get(params object[] keys) => this.Get(keys.AsEnumerable());
		public IEnumerable<T> Get(IEnumerable<object> keys)
		{
			foreach (var key in keys)
				if (this.Get(key) is T value)
					yield return value;
		}

		public T GetOrCreate(object key, Func<ICacheEntry, T> factory)
		{
			return GetWithKey(key, () => Cache.GetOrCreate(key, factory));
		}

		public async Task<T> GetOrCreateAsync(object key, Func<ICacheEntry, Task<T>> factory)
		{
			var item = await Cache.GetOrCreateAsync(key, factory);
			return GetWithKey(key, () => item);
		}

		public T Set(object key, T value)
		{
			return SetWithKey(key, () => Cache.Set(key, value));
		}

		public T Set(object key, T value, MemoryCacheEntryOptions options)
		{
			return SetWithKey(key, () => Cache.Set(key, value, options));
		}

		public T Set(object key, T value, IChangeToken expirationToken)
		{
			return SetWithKey(key, () => Cache.Set(key, value, expirationToken));

		}

		public T Set(object key, T value, DateTimeOffset absoluteExpiration)
		{
			return SetWithKey(key, () => Cache.Set(key, value, absoluteExpiration));
		}

		public T Set(object key, T value, TimeSpan absoluteExpirationRelativeToNow)
		{
			return SetWithKey(key, () => Cache.Set(key, value, absoluteExpirationRelativeToNow));
		}

		private T GetWithKey(object key, Func<T> getter)
		{
			if (getter() is T item)
			{
				AddKey(key);
				return item;
			}

			RemoveKey(key);
			return default;
		}

		private T SetWithKey(object key, Func<T> setter)
		{
			if (setter() is T item)
			{
				AddKey(key);
				return item;
			}

			return default;
		}

		public bool TryGetValue(object key, out T value)
		{
			if (Cache.TryGetValue(key, out value))
			{
				AddKey(key);
				return true;
			}
			else
			{
				RemoveKey(key);
				return false;
			}
		}

		public bool TryGetValue(object key, out object value)
		{
			if (Cache.TryGetValue(key, out value))
			{
				AddKey(key);
				return true;
			}
			else
			{
				RemoveKey(key);
				return false;
			}
		}

		public ICacheEntry CreateEntry(object key)
		{
			return Cache.CreateEntry(key);
		}

		public void Remove(object key)
		{
			Cache.Remove(key);
			RemoveKey(key);
		}

		private void AddKey(object key)
		{
			Keys.Add(key);
		}

		private void RemoveKey(object key)
		{
			Keys.Remove(key);
		}

		public IQueryable<T> AsQueryable()
		{
			return this.Get(Keys).AsQueryable();
		}

		public void Dispose()
		{
			Cache.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}