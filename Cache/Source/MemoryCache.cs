using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;

namespace OLWebApi.Caching
{
	public class MemoryCache<T> : MemoryCache, IMemoryCache<T>
	{
		private IMemoryCache Cache => this;

		public MemoryCache(IOptions<MemoryCacheOptions> optionsAccessor) : base(optionsAccessor) { }
		public MemoryCache(IOptions<MemoryCacheOptions> optionsAccessor, ILoggerFactory loggerFactory) : base(optionsAccessor, loggerFactory) { }

		public IEnumerable<T> Get(params object[] keys) => this.Get(keys.AsEnumerable());
		public IEnumerable<T> Get(IEnumerable<object> keys)
		{
			foreach (var key in keys)
				if (Get(key) is T value)
					yield return value;
		}

		public T GetOrCreate(object key, Func<ICacheEntry, T> factory)
		{
			return Cache.GetOrCreate(key, factory);
		}

		public Task<T> GetOrCreateAsync(object key, Func<ICacheEntry, Task<T>> factory)
		{
			return Cache.GetOrCreateAsync(key, factory);
		}

		public T Set(object key, T value)
		{
			return Cache.Set(key, value);
		}

		public T Set(object key, T value, MemoryCacheEntryOptions options)
		{
			return Cache.Set(key, value, options);
		}

		public T Set(object key, T value, IChangeToken expirationToken)
		{
			return Cache.Set(key, value, expirationToken);
		}

		public T Set(object key, T value, DateTimeOffset absoluteExpiration)
		{
			return Cache.Set(key, value, absoluteExpiration);
		}

		public T Set(object key, T value, TimeSpan absoluteExpirationRelativeToNow)
		{
			return Cache.Set(key, value, absoluteExpirationRelativeToNow);
		}

		public new ICacheEntry CreateEntry(object key)
		{
			return base.CreateEntry(key);
		}

		public bool TryGetValue(object key, out T value) => Cache.TryGetValue(key, out value);
		public new bool TryGetValue(object key, out object value) => base.TryGetValue(key, out value);

		public T Get(object key)
		{
			return Cache.Get<T>(key);
		}

		public new void Dispose()
		{
			GC.SuppressFinalize(this);
		}
	}
}