using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;

namespace OLWebApi.Caching
{
	public interface IMemoryCache<T> : IMemoryCache
	{
		T Get(object key);
		IEnumerable<T> Get(params object[] keys);
		IEnumerable<T> Get(IEnumerable<object> keys);
		T GetOrCreate(object key, Func<ICacheEntry, T> factory);
		Task<T> GetOrCreateAsync(object key, Func<ICacheEntry, Task<T>> factory);
		T Set(object key, T value);
		T Set(object key, T value, MemoryCacheEntryOptions options);
		T Set(object key, T value, IChangeToken expirationToken);
		T Set(object key, T value, DateTimeOffset absoluteExpiration);
		T Set(object key, T value, TimeSpan absoluteExpirationRelativeToNow);
		bool TryGetValue(object key, out T value);
	}
}