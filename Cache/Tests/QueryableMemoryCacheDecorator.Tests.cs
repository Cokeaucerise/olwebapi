using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
using OLWebApi.Core.Entities.Model;
using Xunit;

namespace OLWebApi.Caching.Tests
{
	public record TestRecord : ApiRecord
	{
		public TestRecord(string id) : base(id) { }
	}

	public class QueryableMemoryCachecacheTests : IDisposable
	{
		protected IMemoryCache<TestRecord> InnerCache = new MemoryCache<TestRecord>(new MemoryCacheOptions());
		protected IQueryableMemoryCache<TestRecord> Cache => new QueryableMemoryCacheDecorator<TestRecord>(InnerCache);

		[Fact]
		public void UntrackedItem_Get_AddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			InnerCache.Set(item.Id, item);
			var cache = Cache;

			//When
			_ = cache.Get(item.Id);

			//Then
			Assert.Equal(1, cache.AsQueryable().Count());
			Assert.Equal(item.Id, cache.AsQueryable().First().Id);
		}

		[Fact]
		public void GetTrackedItem_ItemDontExist_RemovedFromQueryable()
		{
			//Given
			TestRecord item = new("1");
			var cache = Cache;
			cache.Set(item.Id, item);
			InnerCache.Remove(item.Id);

			//When
			_ = cache.Get(item.Id);

			//Then
			Assert.Empty(cache.AsQueryable());
		}

		[Fact]
		public void UntrackedItems_GetMany_AddedToQueryable()
		{
			//Given
			TestRecord item1 = new("1"), item2 = new("2");
			InnerCache.Set(item1.Id, item1);
			InnerCache.Set(item2.Id, item2);
			var cache = Cache;

			//When
			_ = cache.Get(item1.Id, item2.Id).ToArray();

			//Then
			Assert.Equal(2, cache.AsQueryable().Count());
			Assert.Equal(item1.Id, cache.AsQueryable().First().Id);
			Assert.Equal(item2.Id, cache.AsQueryable().Last().Id);
		}

		[Fact]
		public void GetOrCreate_ItemExists_ReturnItem()
		{
			//Given
			TestRecord item = new("1");
			InnerCache.Set(item.Id, item);
			var cache = Cache;

			//When
			var result = cache.GetOrCreate(item.Id, _ => throw new Exception());

			//Then
			Assert.Same(item, result);
			Assert.Equal(1, cache.AsQueryable().Count());
		}

		[Fact]
		public void UntrackedItem_TryGetValue_AddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			InnerCache.Set(item.Id, item);
			var cache = Cache;

			//When
			var result = cache.TryGetValue(item.Id, out _);

			//Then
			Assert.True(result);
			Assert.Equal(1, cache.AsQueryable().Count());
			Assert.Equal(item.Id, cache.AsQueryable().First().Id);
		}

		[Fact]
		public void ItemDontExistButIsTracked_TryGetValue_RemovedFromQueryable()
		{
			//Given
			TestRecord item = new("1");
			var cache = Cache;
			cache.Set(item.Id, item);
			InnerCache.Remove(item.Id);

			//When
			var result = cache.TryGetValue(item.Id, out _);

			//Then
			Assert.False(result);
			Assert.Empty(cache.AsQueryable());
		}

		[Fact]
		public void UntrackedItem_TryGetValueObject_AddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			InnerCache.Set(item.Id, item);
			var cache = Cache;

			//When
			var result = cache.TryGetValue(item.Id, out object _);

			//Then
			Assert.True(result);
			Assert.Equal(1, cache.AsQueryable().Count());
			Assert.Equal(item.Id, cache.AsQueryable().First().Id);
		}

		[Fact]
		public void ItemDontExistButIsTracked_TryGetValueObject_RemovedFromQueryable()
		{
			//Given
			TestRecord item = new("1");
			var cache = Cache;
			cache.Set(item.Id, item);
			InnerCache.Remove(item.Id);

			//When
			var result = cache.TryGetValue(item.Id, out object _);

			//Then
			Assert.False(result);
			Assert.Empty(cache.AsQueryable());
		}

		[Fact]
		public void GetOrCreate_ItemDontExists_CreateItem()
		{
			//Given
			TestRecord item = new("1");
			var cache = Cache;

			//When
			var result = cache.GetOrCreate(item.Id, _ => item);

			//Then
			Assert.Same(item, result);
			Assert.Equal(1, cache.AsQueryable().Count());
		}

		[Fact]
		public async Task GetOrCreateAsync_ItemExists_ReturnItem()
		{
			//Given
			TestRecord item = new("1");
			InnerCache.Set(item.Id, item);
			var cache = Cache;

			//When
			var result = await cache.GetOrCreateAsync(item.Id, _ => throw new Exception());

			//Then
			Assert.Same(item, result);
			Assert.Equal(1, cache.AsQueryable().Count());
		}

		[Fact]
		public async Task GetOrCreateAsync_ItemDontExists_CreateItem()
		{
			//Given
			TestRecord item = new("1");
			var cache = Cache;

			//When
			var result = await cache.GetOrCreateAsync(item.Id, _ => Task.FromResult(item));

			//Then
			Assert.Same(item, result);
			Assert.Equal(1, cache.AsQueryable().Count());
		}

		[Fact]
		public void Set_ItemAddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			var cache = Cache;

			//When
			cache.Set(item.Id, item);

			//Then
			Assert.Same(item, cache.AsQueryable().First());
		}

		[Fact]
		public void SetWithOptions_ItemAddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			MemoryCacheEntryOptions options = new();
			var cache = Cache;

			//When
			cache.Set(item.Id, item, options);

			//Then
			Assert.Same(item, cache.AsQueryable().First());
		}

		[Fact]
		public void SetWithExpirations_ItemAddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			IChangeToken expiration = new CancellationChangeToken(default);
			var cache = Cache;

			//When
			cache.Set(item.Id, item, expiration);

			//Then
			Assert.Same(item, cache.AsQueryable().First());
		}

		[Fact]
		public void SetWithAbsoluteExpirations_ItemAddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			var expiration = DateTimeOffset.Now.AddHours(1);
			var cache = Cache;

			//When
			cache.Set(item.Id, item, expiration);

			//Then
			Assert.Same(item, cache.AsQueryable().First());
		}

		[Fact]
		public void SetWithRelativeExpirations_ItemAddedToQueryable()
		{
			//Given
			TestRecord item = new("1");
			var expiration = TimeSpan.FromHours(1);
			var cache = Cache;

			//When
			cache.Set(item.Id, item, expiration);

			//Then
			Assert.Same(item, cache.AsQueryable().First());
		}

		[Fact]
		public void Remove_ItemTracked_ItemRemovedFromQueryable()
		{
			//Given
			TestRecord item = new("1");
			var cache = Cache;
			cache.Set(item.Id, item);

			//When
			cache.Remove(item.Id);
			InnerCache.Set(item.Id, item);

			//Then
			Assert.Empty(cache.AsQueryable());
		}

		public void Dispose()
		{
			InnerCache.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}