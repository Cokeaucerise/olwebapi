using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;

namespace OLWebApi.Core.Provider
{
	public class RegisteredApiControllerFeatureProvider : IApplicationFeatureProvider<ControllerFeature>
	{
		private readonly List<Type> RegisteredApiController = new();

		internal void RegisterApiController(Type type)
		{
			RegisteredApiController.Add(type);
		}

		public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
		{
			PopulateFeatureFromRegisteredControllers(feature);
		}

		private void PopulateFeatureFromRegisteredControllers(ControllerFeature feature)
		{
			foreach (Type controller in RegisteredApiController)
				feature.Controllers.Add(controller.GetTypeInfo());
		}
	}
}