using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using OLWebApi.Core.Attributes;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Extensions;

namespace OLWebApi.Core.Provider
{
	public class RegisteredApiControllerFromRegisteredApiRecord : IApplicationFeatureProvider<ControllerFeature>
	{
		public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
		{
			var registereds = parts.LoadRegisteredTypesFromAttribute(typeof(IApiController<>), typeof(IApiRecord));
			feature.RegisterApiControllers(registereds.Select(r => (r.target, r.source)));
		}
	}

	public class RegisteredApiControllerFromRegisteredApiController : IApplicationFeatureProvider<ControllerFeature>
	{
		public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
		{
			var registereds = parts.LoadRegisteredTypesFromAttribute(typeof(IApiRecord), typeof(IApiController<>));
			feature.RegisterApiControllers(registereds);
		}
	}

	public static class ApplicationPartsExtensions
	{
		public static IEnumerable<(Type source, Type target)> LoadRegisteredTypesFromAttribute(this IEnumerable<ApplicationPart> parts, Type source, Type target)
		{
			var registeredSources = parts.Cast<AssemblyPart>()
				.SelectMany(p => p.Types.Where(t => t.IsAssignableToGeneric(source)));

			foreach (Type s in registeredSources)
			{
				var targets = s
					.GetCustomAttributes<RegisterForWebApi>()
					.Select(attr => attr.RegisteredType)
					.Where(t => t.IsAssignableToGeneric(target));

				foreach (var t in targets)
					yield return (s, t);
			}
		}

		public static void RegisterApiControllers(this ControllerFeature feature, IEnumerable<(Type record, Type controller)> controllers)
		{
			foreach (var controller in controllers)
				RegisterApiController(feature, controller.record, controller.controller);
		}

		public static void RegisterApiController(this ControllerFeature feature, Type record, Type controller)
		{
			feature.Controllers.Add(controller?.MakeGenericType(record).GetTypeInfo());
		}
	}
}