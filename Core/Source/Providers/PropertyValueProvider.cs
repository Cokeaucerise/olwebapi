using System;
using System.Reflection;

namespace OLWebApi.Core.Provider
{
	public interface IPropertyValueProvider
	{
		PropertyInfo GetPropertyInfo<T>(string name) => GetPropertyInfo(typeof(T), name);
		PropertyInfo GetPropertyInfo(Type type, string name);
		object GetPropertyValue(string name, object record);
	}

	public abstract class PropertyValueProvider : IPropertyValueProvider
	{
		public abstract PropertyInfo GetPropertyInfo(Type type, string name);

		public virtual object GetPropertyValue(string name, object record)
		{
			if (string.IsNullOrEmpty(name) || record == default) return null;

			object value = record;
			PropertyInfo prop;
			foreach (var part in name.Split('.'))
			{
				if (string.IsNullOrEmpty(part) || value == default) return null;
				prop = GetPropertyInfo(value.GetType(), part);
				value = prop?.GetValue(value);
			}

			return value;
		}
	}

	public class CaseSensitivePropertyValueProvider : PropertyValueProvider
	{
		public override PropertyInfo GetPropertyInfo(Type type, string name)
		{
			if (string.IsNullOrEmpty(name)) return null;

			return type?.GetProperty(name);
		}
	}

	public class CaseInsensitivePropertyValueProvider : PropertyValueProvider
	{
		private const BindingFlags IGNORE_CASE = BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance;

		public override PropertyInfo GetPropertyInfo(Type type, string name)
		{
			if (string.IsNullOrEmpty(name)) return null;

			return type?.GetProperty(name, IGNORE_CASE);
		}

	}
}