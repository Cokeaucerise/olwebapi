using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace OLWebApi.Core.Extensions
{
	[ExcludeFromCodeCoverage]
	public static class StringExtensions
	{
		public static IEnumerable<string> ToLower(this IEnumerable<string> array) => array.Select(s => s?.ToLower());
		public static string JoinWith(this IEnumerable<string> values, char separator) => values.JoinWith(separator.ToString());
		public static string JoinWith(this IEnumerable<string> values, string separator) => string.Join(separator, values);
		public static IEnumerable<string> NotNullOrDefault(this IEnumerable<string> str) => str.Where(s => !string.IsNullOrEmpty(s));
	}
}