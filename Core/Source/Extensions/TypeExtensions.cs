using System;
using System.Linq;

namespace OLWebApi.Core.Extensions
{
	public static class TypeExtensions
	{
		public static Type IsAssignableFromGeneric<T>(this Type source) => source.IsAssignableFromGeneric(typeof(T));
		public static Type IsAssignableFromGeneric(this Type source, Type target)
		{
			return GenericEqual(source, target) ? source : source.BaseType?.IsAssignableFromGeneric(target);
		}

		public static bool IsAssignableTo<T>(this Type source) => source.IsAssignableToGeneric(typeof(T));
		public static bool IsAssignableToGeneric(this Type source, Type target)
		{
			return source.IsAssignableTo(target) || GenericEqual(source, target) || (source.BaseType?.IsAssignableToGeneric(target) ?? false);
		}

		private static bool GenericEqual(Type source, Type target)
		{
			return source.Equals(target) || GenericInterfaceEqual(source, target) || OpenGenericEqual(source, target);
		}

		private static bool OpenGenericEqual(Type source, Type target)
		{
			return source.IsGenericType &&
				target.IsGenericType &&
				!target.HaveGenericArguments() &&
				source.GetGenericTypeDefinition().Equals(target.GetGenericTypeDefinition());
		}

		private static bool GenericInterfaceEqual(Type source, Type target)
		{
			if (!target.IsInterface)
				return false;

			if (target.IsGenericType && target.HaveGenericArguments() && source.GetInterfaces().Any(i => i.Equals(target)))
				return true;

			if ((!target.IsGenericType || !target.HaveGenericArguments()) && source.GetInterface(target.Name) != null)
				return true;

			return false;
		}

		public static bool HaveGenericArguments(this Type t) => t.IsGenericType && t.GenericTypeArguments.Length > 0;
	}
}