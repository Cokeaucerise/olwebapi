using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.JsonConverters;
using OLWebApi.Core.Provider;

namespace OLWebApi.Core.Server
{
	[ExcludeFromCodeCoverage]
	public static class WebApiManager
	{
		private static RegisteredApiControllerFeatureProvider ApiControllerFeatureProvider = new();

		public static IMvcBuilder AddWebApi(this IServiceCollection services)
		{
			services.AddOptions();
			ApiControllerFeatureProvider = new();

			services.AddPropertyLinkValueLoader();
			services.AddFilterOperators();
			services.AddAccumulator();

			var filterOperators = services.BuildServiceProvider().GetServices<IFilterOperator>();
			var groupByAccumulators = services.BuildServiceProvider().GetServices<IPropertyAccumulator>();

			return services
				.AddControllers()
				.AddJsonOptions(options =>
				{
					options.JsonSerializerOptions.Converters.Add(new UpdateJsonConverterfactory());
					options.JsonSerializerOptions.Converters.Add(new FilterOperatorJsonConverter(filterOperators));
					options.JsonSerializerOptions.Converters.Add(new GroupByAccumulatorJsonConverter(groupByAccumulators));
				})
				.ConfigureApplicationPartManager(m => m.FeatureProviders.Add(ApiControllerFeatureProvider));
		}

		public static IApplicationBuilder UseWebApi(this IApplicationBuilder app)
		{
			app.UseRouting();
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});

			return app;
		}

		public static IMvcBuilder AddWebApiControllerFromRecord(this IMvcBuilder builder)
		{
			return builder
				.ConfigureApplicationPartManager(m =>
					m.FeatureProviders.Add(new RegisteredApiControllerFromRegisteredApiRecord())
				);
		}

		public static IMvcBuilder AddWebApiControllerFromController(this IMvcBuilder builder)
		{
			return builder
				.ConfigureApplicationPartManager(m =>
					m.FeatureProviders.Add(new RegisteredApiControllerFromRegisteredApiController())
				);
		}

		public static IMvcBuilder AddWebApiControllerFromRegistred(this IMvcBuilder builder)
		{
			return builder
				.ConfigureApplicationPartManager(m =>
				{
					m.FeatureProviders.Add(new RegisteredApiControllerFromRegisteredApiRecord());
					m.FeatureProviders.Add(new RegisteredApiControllerFromRegisteredApiController());
				});
		}

		public static ApiRecordOptionBuilder<T> ForApiRecord<T>(this IServiceCollection services) where T : class, IApiRecord
		{
			return new ApiRecordOptionBuilder<T>(ApiControllerFeatureProvider);
		}

		public static IServiceCollection AddPropertyLinkValueLoader(this IServiceCollection services)
		{
			services.AddTransient<IPropertyValueProvider, CaseInsensitivePropertyValueProvider>();

			return services;
		}

		public static IServiceCollection AddFilterOperators(this IServiceCollection services)
		{
			return services
				.AddFilterOperator<FilterEqualOperator>()
				.AddFilterOperator<FilterNotEqualOperator>()
				.AddFilterOperator<FilterGreaterOperator>()
				.AddFilterOperator<FilterGreaterOrEqualOperator>()
				.AddFilterOperator<FilterLessOperator>()
				.AddFilterOperator<FilterLessOrEqualOperator>()
				.AddFilterOperator<FilterInOperator>()
				.AddFilterOperator<FilterNotInOperator>();
		}

		public static IServiceCollection AddFilterOperator<T>(this IServiceCollection services) where T : class, IFilterOperator, new()
		{
			return services.AddSingleton<IFilterOperator>(new T());
		}

		public static IServiceCollection AddAccumulator(this IServiceCollection services)
		{
			services.AddSingleton<IPropertyAccumulator>(new SumAccumulator());
			services.AddSingleton<IPropertyAccumulator>(new CountAccumulator());

			return services;
		}
	}
}
