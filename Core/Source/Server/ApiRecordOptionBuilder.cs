using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Provider;

namespace OLWebApi.Core.Server
{
	public class ApiRecordOptionBuilder<T> where T : class, IApiRecord
	{
		private readonly RegisteredApiControllerFeatureProvider ControllerFeature;
		public ApiRecordOptionBuilder(RegisteredApiControllerFeatureProvider controllerFeature)
		{
			ControllerFeature = controllerFeature;
		}

		public ApiRecordOptionBuilder<T> AddController<TApiController>() where TApiController : class, IApiController<T>
		{
			ControllerFeature.RegisterApiController(typeof(TApiController));
			return this;
		}
	}
}