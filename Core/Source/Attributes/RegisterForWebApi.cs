using System;

namespace OLWebApi.Core.Attributes
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
	public class RegisterForWebApi : Attribute
	{
		public Type RegisteredType { get; private set; }

		public RegisterForWebApi(Type registeredType)
		{
			this.RegisteredType = registeredType;
		}
	}
}