using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using OLWebApi.Core.Extensions;

namespace OLWebApi.Core.Attributes
{
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class ApiControllerNameAttribute : Attribute, IControllerModelConvention
	{
		private readonly string prefix;
		private readonly Type ApiControllerType = typeof(IApiController<>);

		public ApiControllerNameAttribute() { }
		public ApiControllerNameAttribute(string prefix)
		{
			this.prefix = prefix;
		}

		public void Apply(ControllerModel controller)
		{
			var controllerType = controller.ControllerType.AsType();

			if (!controllerType.IsAssignableToGeneric(ApiControllerType)) return;
			if (!(controllerType.GetInterface(ApiControllerType.Name)?.HaveGenericArguments() ?? false)) return;
			if (controllerType.GetCustomAttributes<RouteAttribute>().Count() > 1) return;

			string baseControllerName = controllerType.GetGenericArguments()?.FirstOrDefault()?.Name ?? string.Empty;

			if (string.IsNullOrEmpty(prefix))
				controller.ControllerName = baseControllerName;
			else
				controller.ControllerName = string.Join("/", prefix, baseControllerName);
		}
	}
}
