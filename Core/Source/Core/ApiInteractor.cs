using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Provider;

namespace OLWebApi.Core
{
	public interface IApiInteractor<T>
	where T : IApiRecord
	{ }

	public abstract class ApiInteractor<T> : IApiInteractor<T>
	where T : IApiRecord
	{
		protected readonly IApiRepository<T> Repository;

		public ApiInteractor(IApiRepository<T> repo)
		{
			Repository = repo;
		}
	}
}
