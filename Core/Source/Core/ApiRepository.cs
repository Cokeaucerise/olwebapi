using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core
{
	public interface IApiRepository<T> where T : IApiRecord
	{

	}

	public abstract class ApiRepository<T> : IApiRepository<T>
	where T : IApiRecord
	{
		protected IApiGateway<T> Gateway { get; }

		public ApiRepository(IApiGateway<T> gateway)
		{
			Gateway = gateway;
		}
	}
}
