using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core
{
	public interface IApiGateway<T>
	where T : IApiRecord
	{ }
}
