using Microsoft.AspNetCore.Mvc;
using OLWebApi.Core.Attributes;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core
{
	public interface IApiController<T>
	where T : IApiRecord
	{ }

	[Route("[controller]"), ApiControllerName]
	public class ApiController<T> : Controller, IApiController<T>
	where T : IApiRecord
	{
		protected readonly IApiInteractor<T> Interactor;

		public ApiController(IApiInteractor<T> interactor)
		{
			Interactor = interactor;
		}
	}
}
