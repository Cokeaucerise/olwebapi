using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using OLWebApi.Core.Entities.Action;

namespace OLWebApi.Core.Validation
{
	public class UpdateValidationAttribute : ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			if (value is null) return true;

			return IsValid(value, new ValidationContext(value)) == ValidationResult.Success;
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (!ValidateType(value.GetType()))
				return new ValidationResult($"Object type is not {typeof(Update<>).Name}");

			return ValidateProperties(value, validationContext);
		}

		private static ValidationResult ValidateProperties(object value, ValidationContext validationContext)
		{
			foreach (var (k, v) in value as Update)
			{
				var result = ValidateProperty(k, v, validationContext);
				if (result != ValidationResult.Success)
					return result;
			}

			return ValidationResult.Success;
		}

		private static ValidationResult ValidateProperty(PropertyInfo prop, object v, ValidationContext validationContext)
		{
			var valAttrs = prop.GetCustomAttributes<ValidationAttribute>();
			foreach (var attr in valAttrs)
			{
				var result = attr.GetValidationResult(v, validationContext);
				if (result != ValidationResult.Success)
					return result;
			}

			return ValidationResult.Success;
		}

		private static bool ValidateType(Type t)
		{
			return t.IsAssignableTo(typeof(Update));
		}
	}
}