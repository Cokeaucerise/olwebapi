namespace OLWebApi.Core.Entities.Filters
{
	public class PropertyInFilter : PropertyFilter<FilterInOperator>
	{
		public PropertyInFilter(string target, params string[] values)
		: base(target, string.Join(",", values))
		{ }
	}

	public class PropertyNotInFilter : PropertyFilter<FilterNotInOperator>
	{
		public PropertyNotInFilter(string target, params string[] values)
		: base(target, string.Join(",", values))
		{ }
	}

	public class PropertyFilter<TFilterOperator> : PropertyFilter
	where TFilterOperator : class, IFilterOperator, new()
	{
		public PropertyFilter(string target, string value) : base(target, value, new TFilterOperator()) { }
	}

	public class PropertyFilter
	{
		public string Target { get; set; }
		public string Value { get; set; }
		public IFilterOperator Operator { get; set; }

		public PropertyFilter() { }
		public PropertyFilter(string target, string value, IFilterOperator @operator)
		{
			Target = target;
			Value = value;
			Operator = @operator;
		}
	}

	public interface IFilterOperator
	{
		string Operator { get; }
	}

	public class FilterEqualOperator : IFilterOperator
	{
		public virtual string Operator => "=";
	}

	public class FilterNotEqualOperator : FilterEqualOperator
	{
		public override string Operator => "<>";
	}

	public class FilterLessOperator : IFilterOperator
	{
		public string Operator => "<";
	}

	public class FilterLessOrEqualOperator : IFilterOperator
	{
		public string Operator => "<=";
	}

	public class FilterGreaterOperator : IFilterOperator
	{
		public string Operator => ">";
	}

	public class FilterGreaterOrEqualOperator : IFilterOperator
	{
		public string Operator => ">=";
	}

	public class FilterInOperator : IFilterOperator
	{
		public virtual string Operator => "><";
	}

	public class FilterNotInOperator : FilterInOperator
	{
		public override string Operator => "><><";
	}
}