using System.Diagnostics.CodeAnalysis;

namespace OLWebApi.Core.Entities.Accumulators
{
	public interface IPropertyAccumulator
	{
		string Operator { get; }
	}

	[ExcludeFromCodeCoverage]
	public class SumAccumulator : IPropertyAccumulator
	{
		public string Operator => "sum";
	}

	[ExcludeFromCodeCoverage]
	public class CountAccumulator : IPropertyAccumulator
	{
		public string Operator => "count";
	}

	[ExcludeFromCodeCoverage]
	public class MaxAccumulator : IPropertyAccumulator
	{
		public string Operator => "max";
	}

	[ExcludeFromCodeCoverage]
	public class MinAccumulator : IPropertyAccumulator
	{
		public string Operator => "min";
	}
}
