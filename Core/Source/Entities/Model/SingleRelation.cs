using System;

namespace OLWebApi.Core.Entities.Model
{
	public interface ISingleRelation : IRelation
	{
		string Id { get; set; }
	}

	public sealed class SingleRelation<T> : Relation<T>, ISingleRelation where T : IApiRecord
	{
		public string Id { get; set; }

		public SingleRelation() { }

		public SingleRelation(string id)
		{
			Id = id ?? throw new ArgumentNullException(nameof(id));
		}

		public SingleRelation(T record)
		{
			Id = record?.Id ?? throw new ArgumentNullException(nameof(record));
		}
	}
}
