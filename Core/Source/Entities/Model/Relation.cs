using System;
using System.Text.Json.Serialization;
using OLWebApi.Core.JsonConverters;

namespace OLWebApi.Core.Entities.Model
{
	public class Relation<T> : IRelation
	where T : IApiRecord
	{
		[JsonConverter(typeof(TypeWriterConverter))]
		public Type Type => typeof(T);
	}
}