using System;
using OLWebApi.Core.JsonConverters;

namespace OLWebApi.Core.Entities.Model
{
	[JsonInterfaceConverter(typeof(ApiRecordReaderConverter))]
	public interface IApiRecord
	{
		string Id { get; init; }
		string Type { get; }
		DateTimeOffset CreatedAt { get; init; }
		DateTimeOffset UpdatedAt { get; init; }
	}

	public record ApiRecord : IApiRecord
	{
		public string Id { get; init; }
		public string Type => GetType().FullName;
		public DateTimeOffset CreatedAt { get; init; }
		public DateTimeOffset UpdatedAt { get; init; }

		public ApiRecord() { }
		public ApiRecord(string id)
		{
			Id = id;
		}
	}

}
