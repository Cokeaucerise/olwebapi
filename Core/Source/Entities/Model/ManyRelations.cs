using System;
using System.Collections.Generic;
using System.Linq;

namespace OLWebApi.Core.Entities.Model
{
	public interface IManyRelations : IRelation
	{
		IEnumerable<string> Ids { get; set; }
	}

	public sealed class ManyRelations<T> : Relation<T>, IManyRelations where T : IApiRecord
	{
		public IEnumerable<string> Ids { get; set; }

		public ManyRelations() { }

		public ManyRelations(IEnumerable<string> ids)
		{
			Ids = ids ?? throw new ArgumentNullException(nameof(ids));
		}

		public ManyRelations(IEnumerable<T> records)
		{
			Ids = records?.Select(r => r.Id) ?? throw new ArgumentNullException(nameof(records));
		}

	}
}
