using System;

namespace OLWebApi.Core.Entities.Model
{
	public interface IRelation
	{
		Type Type { get; }
	}
}
