using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.Entities.Action
{
	[ExcludeFromCodeCoverage]
	public class Create<T> where T : IApiRecord
	{
		public IEnumerable<T> Data { get; set; }
	}
}