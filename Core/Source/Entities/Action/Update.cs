using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OLWebApi.Core.Entities.Action
{
	public class Updates<T> : Dictionary<string, Update<T>>
	{
		public Updates() { }

		public Updates(IDictionary<string, Update<T>> dictionary) : base(dictionary) { }
	}

	public abstract class Update : Dictionary<PropertyInfo, object> { }

	public class Update<T> : Update
	{
		public Update() { }
		public Update(T data) : this(data == null ? new Dictionary<string, object>() : GetPropertiesValue(data))
		{ }

		public Update(Dictionary<string, object> values)
		{
			if (values == null) return;

			foreach (var (name, value) in values)
				if (typeof(T).GetProperty(name) is PropertyInfo prop && IsWriteProperty(prop))
					Add(prop, value);
		}

		private static Dictionary<string, object> GetPropertiesValue(T data)
		{
			return typeof(T).GetProperties()
				.Where(IsWriteProperty)
				.ToDictionary(p => p.Name, p => p.GetValue(data));
		}

		private static bool IsWriteProperty(PropertyInfo prop)
		{
			return prop.CanWrite && !prop.GetIndexParameters().Any();
		}
	}

}