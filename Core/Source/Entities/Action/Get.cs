using System.Collections.Generic;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.Entities.Action
{
	public class Get<T> : Single<T> where T : IApiRecord
	{
		public Get()
		{
			Data = default;
		}

		public Get(T data)
		{
			Data = data ?? default;
		}
	}
}