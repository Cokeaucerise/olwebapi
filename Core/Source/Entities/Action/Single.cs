using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.Entities.Action
{
	public class Single<T> where T : IApiRecord
	{
		public Single()
		{
			Data = default;
		}

		public Single(T data)
		{
			Data = data ?? default;
		}

		public T Data { get; set; }
	}
}