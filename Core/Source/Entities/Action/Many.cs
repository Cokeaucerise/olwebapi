using System.Collections.Generic;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.Entities.Action
{
	public class Many<T>
	where T : IApiRecord
	{
		public Many()
		{
			Data = new List<T>();
		}

		public Many(IEnumerable<T> data)
		{
			Data = data ?? new List<T>();
		}

		public IEnumerable<T> Data { get; set; }
	}
}