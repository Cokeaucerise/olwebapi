using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OLWebApi.Core.JsonConverters
{
	public class TypeWriterConverter : JsonConverter<Type>
	{
		public override Type Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			return default;
		}

		public override void Write(Utf8JsonWriter writer, Type value, JsonSerializerOptions options)
		{
			JsonSerializer.Serialize(writer, value?.FullName, options);
		}
	}
}