using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.JsonConverters
{
	public class ApiRecordReaderConverter : ApiJsonConverter<IApiRecord>
	{
		private readonly Type ApiRecordType = typeof(IApiRecord);
		private static Dictionary<string, Type> ApiRecordsTypes { get; set; }

		public ApiRecordReaderConverter()
		{
			ApiRecordsTypes ??= AppDomain.CurrentDomain.GetAssemblies()
				.SelectMany(s => s.GetTypes())
				.Where(p => !p.IsInterface)
				.Where(p => ApiRecordType.IsAssignableFrom(p))
				.ToDictionary(p => p.FullName, p => p);
		}

		public override IApiRecord Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			try
			{
				var json = JsonDocument.ParseValue(ref reader)?.RootElement;
				var typeName = ReadStringProperty(json, nameof(IApiRecord.Type), options);

				var recordType = ApiRecordsTypes.GetValueOrDefault(typeName);
				return JsonSerializer.Deserialize(json.ToString(), recordType, options) as IApiRecord;
			}
			catch (Exception) { }

			return default;
		}

		public override void Write(Utf8JsonWriter writer, IApiRecord value, JsonSerializerOptions options)
		{
			JsonSerializer.Serialize(writer, value, value.GetType(), options);
		}
	}
}