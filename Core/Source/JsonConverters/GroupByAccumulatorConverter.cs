using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using OLWebApi.Core.Entities.Accumulators;

namespace OLWebApi.Core.JsonConverters
{
	public class GroupByAccumulatorJsonConverter : ApiJsonConverter<IPropertyAccumulator>
	{
		private readonly Dictionary<string, IPropertyAccumulator> groupByAccumulatorsDictionary;

		public GroupByAccumulatorJsonConverter(IEnumerable<IPropertyAccumulator> groupByAccumulators)
		{
			groupByAccumulatorsDictionary = groupByAccumulators
				.GroupBy(o => o.Operator)
				.ToDictionary(g => g.Key, g => g.FirstOrDefault());
		}

		public override IPropertyAccumulator Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			try
			{
				var json = JsonDocument.ParseValue(ref reader)?.RootElement;
				var accumulator = ReadStringProperty(json, nameof(IPropertyAccumulator.Operator), options);

				return groupByAccumulatorsDictionary.GetValueOrDefault(accumulator);
			}
			catch (Exception) { }

			return default;
		}

		public override void Write(Utf8JsonWriter writer, IPropertyAccumulator value, JsonSerializerOptions options)
		{
			if (value is null)
				JsonSerializer.Serialize(writer, null as IPropertyAccumulator, options);
			else
				JsonSerializer.Serialize(writer, value, value.GetType(), options);
		}
	}
}