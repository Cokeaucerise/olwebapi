using System;
using System.Text.Json.Serialization;

namespace OLWebApi.Core.JsonConverters
{
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple = false)]
	public class JsonInterfaceConverterAttribute : JsonConverterAttribute
	{
		public JsonInterfaceConverterAttribute(Type converterType) : base(converterType) { }
	}
}