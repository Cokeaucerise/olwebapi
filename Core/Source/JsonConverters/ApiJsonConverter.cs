using System.Text.Json;
using System.Text.Json.Serialization;

namespace OLWebApi.Core.JsonConverters
{
	public abstract class ApiJsonConverter<T> : JsonConverter<T>
	{
		protected static string ReadStringProperty(JsonElement? json, string propertyName, JsonSerializerOptions options)
		{
			var typePropertyName = options?.PropertyNamingPolicy?.ConvertName(propertyName) ?? propertyName;

			JsonElement property = default;
			if (!(json?.TryGetProperty(typePropertyName, out property) ?? false)) return default;

			return property.GetString();
		}
	}
}