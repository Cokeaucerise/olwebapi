using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.JsonConverters
{
	public class UpdateConverter<T> : JsonConverter<Update<T>> where T : IApiRecord
	{
		private static readonly PropertyInfo[] Properties = typeof(T).GetProperties();
		private static PropertyInfo GetProperty(string key, JsonSerializerOptions options)
			=> Properties?.FirstOrDefault(p => PropertyNameCompare(p.Name, key, options));

		private static bool PropertyNameCompare(string property, string key, JsonSerializerOptions options)
		{
			property = options.PropertyNamingPolicy.ConvertName(property);
			return property.Equals(key, options.PropertyNameCaseInsensitive ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal);
		}

		public override Update<T> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			Update<T> update = new();
			var values = JsonSerializer.Deserialize<Dictionary<string, JsonElement>>(ref reader, options);

			foreach (var (key, value) in values)
				if (value is JsonElement json && GetProperty(key, options) is PropertyInfo prop)
					update[prop] = JsonSerializer.Deserialize(json.GetRawText(), prop.PropertyType, options);

			return update;
		}

		public override void Write(Utf8JsonWriter writer, Update<T> value, JsonSerializerOptions options)
		{
			var json = value.ToDictionary(k => k.Key.Name, v => v.Value);
			JsonSerializer.Serialize(writer, json, options);
		}
	}

	public class UpdateJsonConverterfactory : JsonConverterFactory
	{
		public override bool CanConvert(Type typeToConvert)
		{
			if (!typeToConvert.IsGenericType)
				return false;

			if (typeToConvert.GetGenericTypeDefinition() != typeof(Update<>))
				return false;

			return typeToConvert.GetGenericArguments()[0].IsAssignableTo(typeof(IApiRecord));
		}

		public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
		{
			var converterType = typeof(UpdateConverter<>).MakeGenericType(typeToConvert.GetGenericArguments()[0]);
			return Activator.CreateInstance(converterType) as JsonConverter;
		}
	}
}