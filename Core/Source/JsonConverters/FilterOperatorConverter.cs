using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using OLWebApi.Core.Entities.Filters;

namespace OLWebApi.Core.JsonConverters
{
	public class FilterOperatorJsonConverter : ApiJsonConverter<IFilterOperator>
	{
		private readonly Dictionary<string, IFilterOperator> filterOperatorsDictionary;

		public FilterOperatorJsonConverter(IEnumerable<IFilterOperator> filterOperators)
		{
			filterOperatorsDictionary = filterOperators
				.GroupBy(o => o.Operator)
				.ToDictionary(o => o.Key, g => g.FirstOrDefault());
		}

		public override IFilterOperator Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			try
			{
				var json = JsonDocument.ParseValue(ref reader)?.RootElement;
				var @operator = ReadStringProperty(json, nameof(IFilterOperator.Operator), options);

				return filterOperatorsDictionary.GetValueOrDefault(@operator);
			}
			catch (Exception) { }

			return default;
		}

		public override void Write(Utf8JsonWriter writer, IFilterOperator value, JsonSerializerOptions options)
		{
			if (value is null)
				JsonSerializer.Serialize(writer, null as IFilterOperator, options);
			else
				JsonSerializer.Serialize(writer, value, value.GetType(), options);
		}
	}
}