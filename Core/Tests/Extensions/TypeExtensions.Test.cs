using System;
using OLWebApi.Core.Extensions;
using Xunit;

namespace OLWebApi.Core.Tests.Extensions
{

	public class TypeExtensionsTest
	{
		[Fact]
		public void GetInheritedType_TargetIsSame_ReturnSourceTypeInstance()
		{
			//Given
			Type source = typeof(object);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.Equal(result, source);
			Assert.True(result == source);
			Assert.Equal(result.GetHashCode(), source.GetHashCode());
		}

		[Fact]
		public void GetInheritedType_SourceBasicClassTargetObject_ReturnInnerObjectType()
		{
			//Given
			Type source = typeof(BasicClass);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.Equal(typeof(object), result);
			Assert.Equal(result, source.BaseType);
		}

		[Fact]
		public void GetInheritedType_SourceEmptyGenericClassTargetObject_ReturnInnerObjectType()
		{
			//Given
			Type source = typeof(GenericClass<>);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.Equal(typeof(object), result);
			Assert.Equal(result, source.BaseType);
		}

		[Fact]
		public void GetInheritedType_SourceGenericClassTargetObject_ReturnInnerObjectType()
		{
			//Given
			Type source = typeof(GenericClass<object>);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.Equal(typeof(object), result);
			Assert.Equal(result, source.BaseType);
		}

		[Fact]
		public void GetInheritedType_SourceEmptyGenericClassTargetEmptyGenericClass_ReturnEmptyGenericClass()
		{
			//Given
			Type source = typeof(GenericClass<>);
			Type target = typeof(GenericClass<>);

			//When
			Type result = source.IsAssignableFromGeneric(target);

			//Then
			Assert.Equal(typeof(GenericClass<>), result);
		}

		[Fact]
		public void GetInheritedType_SourceGenericClassTargetEmptyGenericClass_ReturnGenericClass()
		{
			//Given
			Type source = typeof(GenericClass<object>);
			Type target = typeof(GenericClass<>);

			//When
			Type result = source.IsAssignableFromGeneric(target);

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(GenericClass<object>), result);
		}

		[Fact]
		public void GetInheritedType_SourceGenericClassTargetGenericClass_ReturnGenericClass()
		{
			//Given
			Type source = typeof(GenericClassObject);

			//When
			Type result = source.IsAssignableFromGeneric<GenericClass<object>>();

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(GenericClass<object>), result);
		}

		[Fact]
		public void GetInheritedType_SourceEmptyGenericClassTargetGenericClass_ReturnNull()
		{
			//Given
			Type source = typeof(GenericClass<>);

			//When
			Type result = source.IsAssignableFromGeneric<GenericClass<object>>();

			//Then
			Assert.Null(result);
		}

		[Fact]
		public void GetInheritedType_SourceInterfaceTargetObject_ReturnNull()
		{
			//Given
			Type source = typeof(IBasicInterface);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.Null(result);
		}

		[Fact]
		public void GetInheritedType_SourceInterfaceClassTargetObject_ReturnObjectType()
		{
			//Given
			Type source = typeof(InterfaceClass);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(object), result);
		}

		[Fact]
		public void GetInheritedType_SourceInterfaceClassTargetInterface_ReturnInterfaceClass()
		{
			//Given
			Type source = typeof(InterfaceClass);

			//When
			Type result = source.IsAssignableFromGeneric<IBasicInterface>();

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(InterfaceClass), result);
		}

		[Fact]
		public void GetInheritedType_SourceEmptyGenericInterfaceTargetObject_ReturnNull()
		{
			//Given
			Type source = typeof(IGenericInterface<>);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.Null(result);
		}

		[Fact]
		public void GetInheritedType_SourceGenericInterfaceTargetObject_ReturnNull()
		{
			//Given
			Type source = typeof(IGenericInterface<object>);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.Null(result);
		}

		[Fact]
		public void GetInheritedType_SourceEmptyGenericInterfaceClassTargetObject_ReturnObjectType()
		{
			//Given
			Type source = typeof(GenericInterfaceClass<>);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(object), result);
		}

		[Fact]
		public void GetInheritedType_SourceGenericInterfaceClassTargetObject_ReturnObjectType()
		{
			//Given
			Type source = typeof(GenericInterfaceClass<object>);

			//When
			Type result = source.IsAssignableFromGeneric<object>();

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(object), result);
		}

		[Fact]
		public void GetInheritedType_SourceEmptyGenericInterfaceClassTargetEmptyGenericInterface_ReturnInterfaceClass()
		{
			//Given
			Type source = typeof(GenericInterfaceClass<>);
			Type target = typeof(IGenericInterface<>);

			//When
			Type result = source.IsAssignableFromGeneric(target);

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(GenericInterfaceClass<>), result);
		}

		[Fact]
		public void GetInheritedType_SourceGenericInterfaceClassTargetEmptyGenericInterface_ReturnInterfaceClass()
		{
			//Given
			Type source = typeof(GenericInterfaceClass<object>);
			Type target = typeof(IGenericInterface<>);

			//When
			Type result = source.IsAssignableFromGeneric(target);

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(GenericInterfaceClass<object>), result);
		}

		[Fact]
		public void GetInheritedType_SourceGenericInterfaceClassTargetGenericInterface_ReturnInterfaceClass()
		{
			//Given
			Type source = typeof(GenericInterfaceClass<object>);

			//When
			Type result = source.IsAssignableFromGeneric<IGenericInterface<object>>();

			//Then
			Assert.NotNull(result);
			Assert.Equal(typeof(GenericInterfaceClass<object>), result);
		}

		[Fact]
		public void GetInheritedType_SourceEmptyGenericInterfaceClassTargetGenericInterface_ReturnNull()
		{
			//Given
			Type source = typeof(GenericInterfaceClass<>);

			//When
			Type result = source.IsAssignableFromGeneric<IGenericInterface<object>>();

			//Then
			Assert.Null(result);
		}

	}

	public class BasicClass { }
	public class GenericClass<T> { }
	public class GenericClassObject : GenericClass<object> { }

	public interface IBasicInterface { }
	public interface IGenericInterface<T> { }

	public class InterfaceClass : IBasicInterface { }
	public class GenericInterfaceClass<T> : IGenericInterface<T> { }
}
