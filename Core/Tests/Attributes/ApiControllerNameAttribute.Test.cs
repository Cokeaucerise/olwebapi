using System;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using OLWebApi.Core.Attributes;
using Xunit;

namespace OLWebApi.Core.Tests.Attributes
{

	[Route("[Controller]/MyControllerRoute")]
	public class TestCustomController : ApiController<TestRecord>
	{
		public TestCustomController(IApiInteractor<TestRecord> interactor) : base(interactor) { }
	}

	public class ApiControllerNameAttributeTest
	{
		[Fact]
		public void Constructor_InvalidBaseType_ControllerNameNull()
		{
			//Given
			ControllerModel cm = new(typeof(Controller).GetTypeInfo(), Array.Empty<object>());
			ApiControllerNameAttribute attr = new();

			//When
			attr.Apply(cm);

			//Then
			Assert.Null(cm.ControllerName);
		}

		[Fact]
		public void Constructor_ValidBaseControllerNoType_ControllerNameNull()
		{
			//Given
			ControllerModel cm = new(typeof(IApiController<>).GetTypeInfo(), Array.Empty<object>());
			ApiControllerNameAttribute attr = new();

			//When
			attr.Apply(cm);

			//Then
			Assert.Null(cm.ControllerName);
		}

		[Fact]
		public void Constructor_ValidBaseControllerWithType_ControllerNameMatchClassName()
		{
			//Given
			ControllerModel cm = new(typeof(ApiController<TestRecord>).GetTypeInfo(), Array.Empty<object>());
			ApiControllerNameAttribute attr = new();

			//When
			attr.Apply(cm);

			//Then
			Assert.NotNull(cm.ControllerName);
			Assert.Equal(nameof(TestRecord), cm.ControllerName);
		}

		[Fact]
		public void Constructor_InterfaceOnlyControllerWithType_ControllerNameIsNotChanged()
		{
			//Given
			ControllerModel cm = new(typeof(IApiController<TestRecord>).GetTypeInfo(), Array.Empty<object>());
			ApiControllerNameAttribute attr = new();

			//When
			attr.Apply(cm);

			//Then
			Assert.Null(cm.ControllerName);
		}

		[Fact]
		public void Constructor_CustomControllerWithCustomRouteAttribute_ControllerNameIsNotChanged()
		{
			//Given
			ControllerModel cm = new(typeof(TestCustomController).GetTypeInfo(), Array.Empty<object>());
			ApiControllerNameAttribute attr = new();

			//When
			attr.Apply(cm);

			//Then
			Assert.Null(cm.ControllerName);
		}

		[Fact]
		public void Constructor_PrefixValue_ControllerNameContainPrefixAtStart()
		{
			//Given
			ControllerModel cm = new(typeof(ApiController<TestRecord>).GetTypeInfo(), Array.Empty<object>());
			ApiControllerNameAttribute attr = new("prefix");

			//When
			attr.Apply(cm);

			//Then
			Assert.NotNull(cm.ControllerName);
			Assert.Equal("prefix/" + nameof(TestRecord), cm.ControllerName);
		}
	}
}
