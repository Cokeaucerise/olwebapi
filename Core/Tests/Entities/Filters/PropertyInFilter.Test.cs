using OLWebApi.Core.Entities.Filters;
using Xunit;

namespace OLWebApi.Core.Tests.Entities.Filters
{
	public class PropertyInFilterTest
	{
		[Fact]
		public void Constructor_ParamsValues_ValuesAreJoinedWithComma()
		{
			var values = new string[] { "1", "2", "3" };

			// When
			var filter = new PropertyInFilter("test", values);

			// Then
			Assert.Equal("1,2,3", filter.Value);
		}
	}
}