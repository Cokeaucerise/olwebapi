using OLWebApi.Core.Entities.Filters;
using Xunit;

namespace OLWebApi.Core.Tests.Entities.Filters
{
	public class PropertyNotInFilterTest
	{
		[Fact]
		public void Constructor_ParamsValues_ValuesAreJoinedWithComma()
		{
			var values = new string[] { "1", "2", "3" };

			// When
			var filter = new PropertyNotInFilter("test", values);

			// Then
			Assert.Equal("1,2,3", filter.Value);
		}
	}
}