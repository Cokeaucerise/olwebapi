using OLWebApi.Core.Entities.Action;
using Xunit;

namespace OLWebApi.Core.Tests.Model.Action
{
	public class SingleTest
	{
		[Fact]
		public void Constructor_NoParameter_DataPropertyReturnDefault()
		{
			var single = new Single<TestRecord>();

			Assert.Equal(default, single.Data);
		}

		[Fact]
		public void Constructor_DataNull_DataPropertyReturnDefault()
		{
			var single = new Single<TestRecord>(null);

			Assert.Equal(default, single.Data);
		}

		[Fact]
		public void Constructor_DataNotNull_DataPropertyReturnData()
		{
			var data = new TestRecord();

			var single = new Single<TestRecord>(data);

			Assert.Same(data, single.Data);
		}
	}
}