using System;
using System.Collections.Generic;
using System.Linq;
using OLWebApi.Core.Entities.Action;
using Xunit;

namespace OLWebApi.Core.Tests.Model.Action
{
	public class UpdateTest
	{
		[Fact]
		public void Constructor_NoParameter_UpdateIsEmpty()
		{
			var update = new Update<TestRecord>();

			Assert.Empty(update);
		}

		[Fact]
		public void Constructor_NullData_UpdateIsEmpty()
		{
			var data = null as TestRecord;

			var update = new Update<TestRecord>(data);

			Assert.Empty(update);
		}

		[Fact]
		public void Constructor_NullValues_UpdateIsEmpty()
		{
			var values = null as Dictionary<string, object>;

			var update = new Update<TestRecord>(values);

			Assert.Empty(update);
		}

		[Fact]
		public void Constructor_DataNotNull_UpdateContainAllWriteProperty()
		{
			var data = new TestRecord
			{
				Id = "1",
				Value = "value",
				UpdatedAt = DateTimeOffset.Now,
				CreatedAt = DateTimeOffset.Now
			};

			var update = new Update<TestRecord>(data);

			var keys = update.Keys.Select(k => k.Name).ToArray();
			Assert.Equal(4, keys.Length);
			Assert.Contains(nameof(TestRecord.Id), keys);
			Assert.Contains(nameof(TestRecord.Value), keys);
			Assert.Contains(nameof(TestRecord.UpdatedAt), keys);
			Assert.Contains(nameof(TestRecord.CreatedAt), keys);
		}

		[Fact]
		public void Constructor_PartialValues_UpdateContainOnlyValues()
		{
			var values = new Dictionary<string, object> {
				{ nameof(TestRecord.Value), "asd" }
			};

			var update = new Update<TestRecord>(values);

			var keys = update.Keys.Select(k => k.Name).ToArray();
			Assert.Single(keys);
			Assert.Contains(nameof(TestRecord.Value), keys);
		}

		[Fact]
		public void Constructor_ValuesWithReadOnlyProperty_UpdateContainOnlyWriteValues()
		{
			var values = new Dictionary<string, object> {
				{ nameof(TestRecord.Value), nameof(TestRecord.Value) },
				{ nameof(TestRecord.Type), nameof(TestRecord.Type) }
			};

			var update = new Update<TestRecord>(values);

			Assert.Single(update.Keys);
			Assert.Equal(nameof(TestRecord.Value), update.Keys.First().Name);
			Assert.Equal(nameof(TestRecord.Value), update.Values.First());
		}
	}
}