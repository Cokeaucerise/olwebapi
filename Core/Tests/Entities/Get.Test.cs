using System.Collections.Generic;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using Xunit;

namespace OLWebApi.Core.Tests.Model
{
	public class GetTest
	{
		public GetTest() { }

		[Fact]
		public void Constructor_EmptyCtr_PropsDefault()
		{
			//Given

			//When
			Get<TestRecord> g = new();

			//Then
			Assert.Equal(default, g.Data);
		}

		[Fact]
		public void Constructor_DataNull_DataDefault()
		{
			//Given

			//When
			Get<TestRecord> g = new(null);

			//Then
			Assert.Equal(default, g.Data);
		}

		[Fact]
		public void Constructor_WithData_DataDefault()
		{
			//Given
			TestRecord record = new();

			//When
			Get<TestRecord> g = new(record);

			//Then
			Assert.NotNull(g.Data);
			Assert.Equal(record, g.Data);
		}
	}
}