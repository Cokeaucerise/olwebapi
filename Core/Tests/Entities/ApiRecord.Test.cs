using System;
using System.Globalization;
using OLWebApi.Core.Entities.Model;
using Xunit;

namespace OLWebApi.Core.Tests.Model
{

	public class ApiRecordTest
	{
		public ApiRecordTest() { }

		[Fact]
		public void Abstract_Type_ReturnFullName()
		{
			//Given

			//When
			var name = new TestRecord().Type;

			//Then
			Assert.Equal(typeof(TestRecord).FullName, name);
		}

		[Fact]
		public void Contructor_NoParam_NullProps()
		{
			//Given

			//When
			TestRecord record = new();

			//Then
			Assert.Null(record.Id);
			Assert.Equal(default, record.CreatedAt);
			Assert.Equal(default, record.UpdatedAt);
			Assert.Equal(typeof(TestRecord).FullName, record.Type);
		}

		[Fact]
		public void Contructor_IdParam_IdNotNull()
		{
			//Given
			string guid = Guid.NewGuid().ToString();

			//When
			TestRecord record = new(guid);

			//Then
			Assert.NotNull(record.Id);
			Assert.Equal(guid, record.Id);
			Assert.Equal(default, record.CreatedAt);
			Assert.Equal(default, record.UpdatedAt);
			Assert.Equal(typeof(TestRecord).FullName, record.Type);
		}

		[Fact]
		public void Equals_XIdNullYIdNullSameTypeDefaultCreateDefaultUpdate_ReturnTrue()
		{
			//Given
			TestRecord x = new();
			TestRecord y = new();

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.True(eq);
		}

		[Fact]
		public void Equals_XIdNotNullYIdNullSameTypeDefaultCreateDefaultUpdate_ReturnFalse()
		{
			//Given
			TestRecord x = new(Guid.NewGuid().ToString());
			TestRecord y = new();

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.False(eq);
		}

		[Fact]
		public void Equals_XIdNullYIdNotNullSameTypeDefaultCreateDefaultUpdate_ReturnFalse()
		{
			//Given
			TestRecord x = new();
			TestRecord y = new(Guid.NewGuid().ToString());

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.False(eq);
		}

		[Fact]
		public void Equals_DifferentIdSameTypeDefaultCreateDefaultUpdate_ReturnFalse()
		{
			//Given
			TestRecord x = new(Guid.NewGuid().ToString());
			TestRecord y = new(Guid.NewGuid().ToString());

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.False(eq);
		}

		[Fact]
		public void Equals_SameIdDifferentTypeDefaultCreateDefaultUpdate_ReturnFalse()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			ApiRecord x = new TestRecord(id);
			ApiRecord y = new TestEmptyRecord(id);

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.False(eq);
		}

		[Fact]
		public void Equals_SameIdSameTypeDefaultCreateDefaultUpdate_ReturnTrue()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			TestRecord x = new(id);
			TestRecord y = new(id);

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.True(eq);
		}

		[Fact]
		public void Equals_SameIdSameTypeRandomCreateDefaultUpdate_ReturnFalse()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			TestRecord x = new TestRecord(id) with { CreatedAt = DateTimeOffset.UtcNow };
			TestRecord y = new TestRecord(id) with { CreatedAt = DateTimeOffset.UtcNow };

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.False(eq);
		}

		[Fact]
		public void Equals_SameIdSameTypeDefaultCreateRandomUpdate_ReturnFalse()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			TestRecord x = new TestRecord(id) with { CreatedAt = DateTimeOffset.UtcNow };
			TestRecord y = new TestRecord(id) with { CreatedAt = DateTimeOffset.UtcNow };

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.False(eq);
		}

		[Fact]
		public void Equals_SameIdSameTypeRandomCreateRandomUpdate_ReturnFalse()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			TestRecord x = new TestRecord(id) with { CreatedAt = DateTimeOffset.UtcNow, UpdatedAt = DateTimeOffset.UtcNow };
			TestRecord y = new TestRecord(id) with { CreatedAt = DateTimeOffset.UtcNow, UpdatedAt = DateTimeOffset.UtcNow };

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.False(eq);
		}

		[Fact]
		public void Equals_SameIdSameTypeSameCreateDefaultUpdate_ReturnTrue()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			DateTimeOffset created = DateTimeOffset.UtcNow;
			TestRecord x = new TestRecord(id) with { CreatedAt = created };
			TestRecord y = new TestRecord(id) with { CreatedAt = created };

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.True(eq);
		}

		[Fact]
		public void Equals_SameIdSameTypeDefaultCreateSameUpdate_ReturnTrue()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			DateTimeOffset updated = DateTimeOffset.UtcNow;
			TestRecord x = new TestRecord(id) with { CreatedAt = updated };
			TestRecord y = new TestRecord(id) with { CreatedAt = updated };

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.True(eq);
		}

		[Fact]
		public void Equals_SameIdSameTypeSameCreateSameUpdate_ReturnTrue()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			DateTimeOffset created = DateTimeOffset.UtcNow;
			DateTimeOffset updated = DateTimeOffset.UtcNow;

			TestRecord x = new TestRecord(id) with { CreatedAt = created, UpdatedAt = updated };
			TestRecord y = new TestRecord(id) with { CreatedAt = created, UpdatedAt = updated };

			//When
			bool eq = x.Equals(y);

			//Then
			Assert.True(eq);
		}

		[Fact]
		public void GetHashCode_DefaultRecord_ReturnNotDefaultLong()
		{
			//Given
			TestRecord record = new();

			//When
			int hash = record.GetHashCode();

			//Then
			Assert.NotEqual(default, hash);
		}

		[Fact]
		public void GetHashCode_RandomId_ReturnDifferentThanDefault()
		{
			//Given
			TestRecord baseline = new();
			TestRecord record = new(Guid.NewGuid().ToString());

			//When
			int hash = record.GetHashCode();

			//Then
			Assert.NotEqual(baseline.GetHashCode(), hash);
		}

		[Fact]
		public void GetHashCode_RandomIdRandomCreated_ReturnDifferentThanIdOnly()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			TestRecord baseline = new(id);
			TestRecord record = new TestRecord(id) with { CreatedAt = DateTimeOffset.UtcNow };

			//When
			int hash = record.GetHashCode();

			//Then
			Assert.NotEqual(baseline.GetHashCode(), hash);
		}

		[Fact]
		public void GetHashCode_RandomIdRandomCreatedRandomUpdated_ReturnDifferentThanIdAndCreated()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			DateTimeOffset created = DateTimeOffset.UtcNow;

			TestRecord baseline = new TestRecord(id) with { CreatedAt = created };

			TestRecord record = new TestRecord(id) with { CreatedAt = created, UpdatedAt = DateTimeOffset.UtcNow };

			//When
			int hash = record.GetHashCode();

			//Then
			Assert.NotEqual(baseline.GetHashCode(), hash);
		}

		[Fact]
		public void GetHashCode_RandomIdRandomCreatedRandomUpdated_ReturnSameAsClone()
		{
			//Given
			string id = Guid.NewGuid().ToString();
			DateTimeOffset created = DateTimeOffset.UtcNow;
			DateTimeOffset updated = DateTimeOffset.UtcNow;

			TestRecord a = new(id)
			{
				CreatedAt = created,
				UpdatedAt = updated
			};
			TestRecord b = new(id)
			{
				CreatedAt = created,
				UpdatedAt = updated
			};

			//When
			int hash = b.GetHashCode();

			//Then
			Assert.Equal(a.GetHashCode(), hash);
		}
	}
}