using System;
using OLWebApi.Core.Provider;
using Xunit;

namespace OLWebApi.Core.Tests.Providers
{
	public class CaseSensitivePropertyValueProviderTest
	{
		protected IPropertyValueProvider Provider = new CaseSensitivePropertyValueProvider();

		[Fact]
		public void GetPropertyInfo_NullType_ReturnNull()
		{
			Type type = null;

			var info = Provider.GetPropertyInfo(type, "name");

			Assert.Null(info);
		}


		[Fact]
		public void GetPropertyInfo_NullName_ReturnNull()
		{
			Type type = typeof(TestRecord);

			var info = Provider.GetPropertyInfo(type, null);

			Assert.Null(info);
		}

		[Fact]
		public void GetPropertyInfo_ValidName_ReturnInfo()
		{
			Type type = typeof(TestRecord);
			string name = nameof(TestRecord.Value);

			var info = Provider.GetPropertyInfo(type, name);

			Assert.Equal(type.GetProperty(name), info);
		}

		[Fact]
		public void GetPropertyInfo_NameBadCasing_ReturnNull()
		{
			Type type = typeof(TestRecord);
			string name = nameof(TestRecord.Value);

			var info = Provider.GetPropertyInfo(type, name.ToLower());

			Assert.Null(info);
		}

		[Fact]
		public void GetPropertyValue_NullName_ReturnNull()
		{
			var record = new TestRecord();

			var value = Provider.GetPropertyValue(null, record);

			Assert.Null(value);
		}

		[Fact]
		public void GetPropertyValue_NullRecord_ReturnNull()
		{
			var name = nameof(TestRecord.Value);

			var value = Provider.GetPropertyValue(name, null);

			Assert.Null(value);
		}

		[Fact]
		public void GetPropertyValue_ValidName_ReturnPropertyValue()
		{
			var name = nameof(TestRecord.Value);
			var record = new TestRecord { Value = "test" };

			var value = Provider.GetPropertyValue(name, record);

			Assert.Equal(record.Value, value);
		}

		[Fact]
		public void GetPropertyValue_NameBadCasing_ReturnNull()
		{
			var name = nameof(TestRecord.Value);
			var record = new TestRecord { Value = "test" };

			var value = Provider.GetPropertyValue(name.ToLower(), record);

			Assert.Null(value);
		}
	}
}