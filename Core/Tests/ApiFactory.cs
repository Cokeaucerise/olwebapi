using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OLWebApi.Core.Server;

namespace OLWebApi.Core.Tests
{
	public class ApiFactory : WebApplicationFactory<Startup>
	{
		protected override IHostBuilder CreateHostBuilder()
		{
			return Host
				.CreateDefaultBuilder()
				.ConfigureWebHost(builder =>
				{
					builder.UseStartup<Startup>()
						.UseContentRoot("qwe");
				});
		}
	}
	public class Startup
	{
		private const string APPSETTINGS_FILE_NAME = "appsettings";
		public IConfiguration Configuration { get; }

		public Startup(IWebHostEnvironment env)
		{
			Configuration = LoadConfiguration();
		}

		private static IConfiguration LoadConfiguration()
		{
			return new ConfigurationBuilder()
				.AddJsonFile($"{APPSETTINGS_FILE_NAME}.json", optional: true, reloadOnChange: false)
				.AddEnvironmentVariables()
				.Build();
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//Singletons
			services.AddSingleton(Configuration);

			//Dependency injection
			services
				.AddWebApi();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			app.UseWebApi();
		}
	}
}