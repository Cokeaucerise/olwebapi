using System;
using System.Text.Json.Serialization;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.Tests
{
	public record TestRecord : ApiRecord
	{
		public string Value { get; set; }

		public TestRecord() { }

		public TestRecord(string id) : base(id) { }
	}

	public record TestEmptyRecord : ApiRecord
	{
		public TestEmptyRecord() { }

		public TestEmptyRecord(string id) : base(id) { }
	}

	public record TestParentRecord : ApiRecord
	{
		public TestParentRecord() { }

		public TestParentRecord(string id) : base(id) { }

		public SingleRelation<TestRecord> Child { get; set; }
		public ManyRelations<TestChildRecord> Childs { get; set; }
	}

	public record TestChildRecord : ApiRecord
	{
		public TestChildRecord() { }

		public TestChildRecord(string id) : base(id) { }

		public SingleRelation<TestParentRecord> Parent { get; set; }
	}

	public record TestSingleRelationRecord : ApiRecord
	{
		public TestSingleRelationRecord() { }

		public TestSingleRelationRecord(string id) : base(id) { }

		public SingleRelation<TestRecord> Single { get; set; }
	}

	public record TestManyRelationsRecord : ApiRecord
	{
		public TestManyRelationsRecord() { }

		public TestManyRelationsRecord(string id) : base(id) { }

		public ManyRelations<TestRecord> Many { get; set; }
	}

	public record TestMultipleSingleRelationSameTypeRecord : ApiRecord
	{
		public TestMultipleSingleRelationSameTypeRecord() { }

		public TestMultipleSingleRelationSameTypeRecord(string id) : base(id) { }

		public SingleRelation<TestRecord> Single0 { get; set; }
		public SingleRelation<TestRecord> Single1 { get; set; }
		public SingleRelation<TestRecord> Single2 { get; set; }
	}

	public record TestMultipleSingleRelationDifferentTypesRecord : ApiRecord
	{
		public TestMultipleSingleRelationDifferentTypesRecord() { }

		public TestMultipleSingleRelationDifferentTypesRecord(string id) : base(id) { }

		public SingleRelation<TestRecord> TestModel { get; set; }
		public SingleRelation<TestEmptyRecord> TestModelEmpty { get; set; }
	}

	public record TestMultipleManyRelationSameTypeRecord : ApiRecord
	{
		public TestMultipleManyRelationSameTypeRecord() { }

		public TestMultipleManyRelationSameTypeRecord(string id) : base(id) { }

		public ManyRelations<TestRecord> Many0 { get; set; }
		public ManyRelations<TestRecord> Many1 { get; set; }
		public ManyRelations<TestRecord> Many2 { get; set; }
	}

	public record TestMultipleManyRelationDifferentTypeRecord : ApiRecord
	{

		public ManyRelations<TestRecord> TestModel { get; set; }
		public ManyRelations<TestEmptyRecord> TestModelEmpty { get; set; }
	}

	public record TestCircularParentRecord : ApiRecord
	{
		public TestCircularParentRecord() { }

		public TestCircularParentRecord(string id) : base(id) { }

		public SingleRelation<TestCircularChildRecord> Child { get; set; }
	}

	public record TestCircularChildRecord : ApiRecord
	{
		public TestCircularChildRecord() { }

		public TestCircularChildRecord(string id) : base(id) { }

		public SingleRelation<TestCircularParentRecord> Parent { get; set; }
	}

	public record TestCircularParentManyRecord : ApiRecord
	{
		public TestCircularParentManyRecord() { }

		public TestCircularParentManyRecord(string id) : base(id) { }

		public ManyRelations<TestCircularChildManyRecord> Child { get; set; }
	}

	public record TestCircularChildManyRecord : ApiRecord
	{
		public TestCircularChildManyRecord() { }

		public TestCircularChildManyRecord(string id) : base(id) { }

		public ManyRelations<TestCircularParentManyRecord> Parent { get; set; }
	}

	public record TestSpreadingManyRelationsRecord : ApiRecord
	{
		public TestSpreadingManyRelationsRecord() { }

		public TestSpreadingManyRelationsRecord(string id) : base(id) { }

		public ManyRelations<TestSpreadingManyRelationsChild1Record> Child1 { get; set; }
		public ManyRelations<TestSpreadingManyRelationsChild1Record> Child2 { get; set; }
		public ManyRelations<TestSpreadingManyRelationsChild1Record> Child3 { get; set; }
	}

	public record TestSpreadingManyRelationsChild1Record : ApiRecord
	{
		public TestSpreadingManyRelationsChild1Record() { }

		public TestSpreadingManyRelationsChild1Record(string id) : base(id) { }

		public ManyRelations<TestSpreadingManyRelationsChild2Record> ChildChild1 { get; set; }
		public ManyRelations<TestSpreadingManyRelationsChild2Record> ChildChild2 { get; set; }
		public ManyRelations<TestSpreadingManyRelationsChild2Record> ChildChild3 { get; set; }
	}
	public record TestSpreadingManyRelationsChild2Record : ApiRecord
	{
		public TestSpreadingManyRelationsChild2Record() { }

		public TestSpreadingManyRelationsChild2Record(string id) : base(id) { }

		public ManyRelations<TestRecord> ChildChildChild1 { get; set; }
		public ManyRelations<TestRecord> ChildChildChild2 { get; set; }
		public ManyRelations<TestRecord> ChildChildChild3 { get; set; }

	}

	public record TestDataMemberAttributeRecord : ApiRecord
	{
		public TestDataMemberAttributeRecord() { }

		public TestDataMemberAttributeRecord(string id) : base(id) { }

		[JsonPropertyName("CustomName")]
		public SingleRelation<TestRecord> TestModel { get; set; }
	}

}