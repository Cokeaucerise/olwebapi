using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Validation;
using Xunit;

namespace OLWebApi.Core.Tests.Validation
{
	public record UpdateRecord : ApiRecord
	{
		[Range(0, 5)]
		public int IntVal { get; set; }

		[MaxLength(10)]
		public string StringVal { get; set; }

		[MinLength(1)]
		public int[] IntArray { get; set; }
	}

	public class UpdateValidationTest
	{
		protected UpdateValidationAttribute Validation = new();

		[Fact]
		public void ObjectNull_ReturnTrue()
		{
			//Given

			//When
			var result = Validation.IsValid(null);

			//Then
			Assert.True(result);
		}

		[Fact]
		public void UpdateNull_ReturnTrue()
		{
			//Given
			Update<UpdateRecord> update = null;

			//When
			var result = Validation.IsValid(update);

			//Then
			Assert.True(result);
		}

		[Fact]
		public void ObjectNotTypeUpdate_ReturnFalse()
		{
			//Given
			string update = "Not an update";

			//When
			var result = Validation.IsValid(update);

			//Then
			Assert.False(result);
		}

		[Fact]
		public void EmptyUpdate_ReturnTrue()
		{
			//Given
			Update<UpdateRecord> update = new(new Dictionary<string, object>());

			//When
			var result = Validation.IsValid(update);

			//Then
			Assert.True(result);
		}

		[Fact]
		public void UpdateWithInvalidValues_ReturnFalse()
		{
			//Given
			Update<UpdateRecord> update = new(new Dictionary<string, object>
			{
				{ nameof(UpdateRecord.IntVal), 6 },
				{ nameof(UpdateRecord.StringVal), "above ten characters" },
				{ nameof(UpdateRecord.IntArray), System.Array.Empty<int>() },
			});

			//When
			var result = Validation.IsValid(update);

			//Then
			Assert.False(result);
		}

		[Fact]
		public void UpdateWithValidValues_ReturnTrue()
		{
			//Given
			Update<UpdateRecord> update = new(new Dictionary<string, object>
			{
				{ nameof(UpdateRecord.IntVal), 1 },
				{ nameof(UpdateRecord.StringVal), "valid" },
				{ nameof(UpdateRecord.IntArray), new[] { 1, 2, 3 } },
			});

			//When
			var result = Validation.IsValid(update);

			//Then
			Assert.True(result);
		}
	}
}