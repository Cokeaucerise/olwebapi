using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using OLWebApi.Core.Server;
using Xunit;

namespace OLWebApi.Core.Integration
{

	public class GivenWeUseAWebApi : IClassFixture<ApiFactory>
	{
		protected readonly WebApplicationFactory<Startup> Factory;
		protected readonly Mock<IApiInteractor<TestApiRecord>> Interactor = new();

		protected static Task<HttpResponseMessage> ClientRequest(HttpClient client, string route, string verb) => verb switch
		{
			"GET" => client.GetAsync(route),
			"POST" or "UPDATE" => client.PostAsync(route, TestStringContent),
			"DELETE" => client.DeleteAsync(route),
			_ => throw new NotImplementedException(),
		};

		protected static StringContent TestStringContent
			=> new(JsonSerializer.Serialize(new TestApiRecord(Guid.NewGuid().ToString())), Encoding.UTF8, "application/json");

		public GivenWeUseAWebApi(ApiFactory factory)
		{
			Factory = factory;
		}

		[Theory]
		[InlineData("TestApiRecord/1", "GET")]
		[InlineData("TestApiRecord", "POST")]
		[InlineData("TestApiRecord/1", "UPDATE")]
		[InlineData("TestApiRecord/1", "DELETE")]
		public async Task WhenWeRegsiterControllersFromApiController_TheControllerEndpointsAreAccessible(string route, string verb)
		{
			// Given
			var client = Factory.WithWebHostBuilder(builder =>
			{
				builder.ConfigureServices(services =>
				{
					services.AddMvc().AddWebApiControllerFromController();
					services.AddTransient(p => Interactor.Object);
				});
			})
			.CreateClient();

			// When
			var result = await ClientRequest(client, route, verb);

			// Then
			result.EnsureSuccessStatusCode();
			Assert.NotNull(await result.Content.ReadAsStringAsync());
		}

		[Theory]
		[InlineData("TestApiRecord/1", "GET")]
		[InlineData("TestApiRecord", "POST")]
		[InlineData("TestApiRecord/1", "UPDATE")]
		[InlineData("TestApiRecord/1", "DELETE")]
		public async Task WhenWeRegsiterControllersFromApiRecord_TheControllerEndpointsAreAccessible(string route, string verb)
		{
			//Given
			var client = Factory.WithWebHostBuilder(builder =>
			{
				builder.ConfigureServices(services =>
				{
					services.AddMvc().AddWebApiControllerFromRecord();
					services.AddTransient(p => Interactor.Object);
				});
			})
			.CreateClient();

			// When
			var result = await ClientRequest(client, route, verb);

			// Then
			result.EnsureSuccessStatusCode();
			Assert.NotNull(await result.Content.ReadAsStringAsync());
		}

		[Theory]
		[InlineData("TestApiRecordMix/1", "GET")]
		[InlineData("TestApiRecordMix", "POST")]
		[InlineData("TestApiRecordMix/1", "UPDATE")]
		[InlineData("TestApiRecordMix/1", "DELETE")]
		public async Task WhenWeRegsiterControllersFromApiRecordAndApiController_TheControllerEndpointsAreAccessible(string route, string verb)
		{
			//Given
			var client = Factory.WithWebHostBuilder(builder =>
			{
				builder.ConfigureServices(services =>
				{
					services.AddMvc().AddWebApiControllerFromRegistred();
					services.AddTransient(p => new Mock<IApiInteractor<TestApiRecordMix>>().Object);
				});
			})
			.CreateClient();

			// When
			var result = await ClientRequest(client, route, verb);

			// Then
			result.EnsureSuccessStatusCode();
			Assert.NotNull(await result.Content.ReadAsStringAsync());
		}

		[Theory]
		[InlineData("TestApiRecord/1", "GET")]
		[InlineData("TestApiRecord", "POST")]
		[InlineData("TestApiRecord/1", "UPDATE")]
		[InlineData("TestApiRecord/1", "DELETE")]
		public async Task WhenWeRegsiterManualyMultiplePartialControllers_TheControllerEndpointsAreAccessible(string route, string verb)
		{
			//Given
			var client = Factory.WithWebHostBuilder(builder =>
			{
				builder.ConfigureServices(services =>
				{
					services.ForApiRecord<TestApiRecord>()
						.AddController<GetApiController<TestApiRecord>>()
						.AddController<CreateApiController<TestApiRecord>>()
						.AddController<UpdateApiController<TestApiRecord>>()
						.AddController<DeleteApiController<TestApiRecord>>();

					services.AddTransient(p => Interactor.Object);
				});
			})
			.CreateClient();

			// When
			var result = await ClientRequest(client, route, verb);

			// Then
			result.EnsureSuccessStatusCode();
			Assert.NotNull(await result.Content.ReadAsStringAsync());
		}
	}
}