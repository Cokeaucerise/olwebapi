using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using OLWebApi.Core.Attributes;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Core.Integration
{
	[RegisterForWebApi(typeof(GetApiController<>))]
	[RegisterForWebApi(typeof(CreateApiController<>))]
	[RegisterForWebApi(typeof(UpdateApiController<>))]
	[RegisterForWebApi(typeof(DeleteApiController<>))]
	public record TestApiRecord : ApiRecord
	{
		public TestApiRecord() { }
		public TestApiRecord(string id) : base(id) { }
	}

	[RegisterForWebApi(typeof(GetApiController<>))]
	[RegisterForWebApi(typeof(CreateApiController<>))]
	public record TestApiRecordMix : ApiRecord
	{
		public TestApiRecordMix() { }
		public TestApiRecordMix(string id) : base(id) { }
	}

	[RegisterForWebApi(typeof(TestApiRecord))]
	public class GetApiController<T> : ApiController<T>
	where T : IApiRecord
	{
		public GetApiController(IApiInteractor<T> interactor) : base(interactor) { }

		[HttpGet("{id}")]
		public virtual ActionResult<Single<T>> Get([FromRoute] string id) => Ok();
	}

	// [RegisterForWebApi(typeof(TestApiRecord))]
	// public class GetManyApiController<T> : ApiController<T>
	// where T : IApiRecord
	// {
	// 	public GetManyApiController(IApiInteractor<T> interactor) : base(interactor) { }

	// 	[HttpGet("")]
	// 	public virtual ActionResult<Query<T>> GetMany([FromQuery] IEnumerable<string> ids, [FromQuery] Includes include) => Ok();
	// }

	[RegisterForWebApi(typeof(TestApiRecord))]
	public class CreateApiController<T> : ApiController<T>
	where T : IApiRecord
	{
		public CreateApiController(IApiInteractor<T> interactor) : base(interactor) { }

		[HttpPost("")]
		public virtual ActionResult<Single<T>> Create([FromBody] T record) => Ok();
	}

	[RegisterForWebApi(typeof(TestApiRecord))]
	[RegisterForWebApi(typeof(TestApiRecordMix))]
	public class UpdateApiController<T> : ApiController<T>
	where T : IApiRecord
	{
		public UpdateApiController(IApiInteractor<T> interactor) : base(interactor) { }

		[HttpPost("{id}")]
		public virtual ActionResult<Single<T>> Update([FromRoute] string id, [FromBody] T record) => Ok();
	}

	[RegisterForWebApi(typeof(TestApiRecord))]
	[RegisterForWebApi(typeof(TestApiRecordMix))]
	public class DeleteApiController<T> : ApiController<T>
	where T : IApiRecord
	{
		public DeleteApiController(IApiInteractor<T> interactor) : base(interactor) { }

		[HttpDelete("{id}")]
		public virtual ActionResult<Single<T>> Delete([FromRoute] string id) => Ok();
	}

	public class TestApiGateway<T> : IApiGateway<T>
	where T : IApiRecord
	{
	}
}