using System.Net.Http;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using OLWebApi.Core.JsonConverters;
using Xunit;

namespace OLWebApi.Sql.Gateway.Integration.Tests
{
	[Collection("Sql")]
	public class GivenWeUseACrudApi : IClassFixture<ApiFactory>, IClassFixture<DockerSqlFixture>
	{
		protected WebApplicationFactory<Startup> Factory;
		protected DockerSqlFixture Docker;
		protected static JsonSerializerOptions JsonOptions
		{
			get
			{
				JsonSerializerOptions options = new() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
				options.Converters.Add(new UpdateJsonConverterfactory());
				return options;
			}
		}

		protected static StringContent CreateStringContent(object data) => new(JsonSerializer.Serialize(data, JsonOptions), Encoding.UTF8, "application/json");

		public GivenWeUseACrudApi(ApiFactory factory, DockerSqlFixture docker)
		{
			Factory = factory;
			Docker = docker;
		}
	}
}