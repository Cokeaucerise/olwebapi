using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Sql.Gateway.Integration.Tests
{
	[Collection("Sql")]
	public class GivenWeMakeRequests : GivenWeUseACrudApi, IDisposable
	{
		private TestDbContext _dbContext;
		public TestDbContext DbContext => _dbContext ??= new TestDbContext(Connections.Value);

		protected KeyValuePair<string, string> Connections => Factory.Services
			.GetService<IConfiguration>()
			.GetSection("ConnectionStrings")
			.Get<Dictionary<string, string>>()
			.First();

		protected HttpClient Client => Factory.CreateClient();

		public GivenWeMakeRequests(ApiFactory factory, DockerSqlFixture docker) : base(factory, docker)
		{
			DbContext.Database.EnsureDeleted();
			DbContext.Database.EnsureCreated();
			DbContext.SaveChanges();

			Factory = Factory.WithWebHostBuilder(
				builder => builder.ConfigureServices(
					services => services.AddConnections().AddSingleton<DbContext>(DbContext)
				)
			);
		}

		[Fact]
		public async Task WhenWeMakeAGetRequest_TheRequestsGoTroughTheGateway()
		{
			// Given
			TestRecord record = new(Guid.NewGuid().ToString()) { Childs = new ManyRelations<TestRecordChild>(new[] { "1", "2", "3" }) };
			DbContext.TestRecords.Add(record);
			DbContext.TestRecordChildren.AddRange(new TestRecordChild[] { new("1") { Value = 1 }, new("2") { Value = 2 }, new("3") { Value = 3 } });
			DbContext.SaveChanges();

			// When
			var request = $"{nameof(TestRecord)}/{record.Id}";
			request += "?includes=Childs!Value<>2!Value<3!Value>=1";
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Get<TestRecord>>(content, JsonOptions);

			Assert.Equal(record.Id, response.Data.Id);
		}

		[Fact]
		public async Task WhenWeMakeAQueryRequest_TheRequestsGoTroughTheGateway()
		{
			// Given
			TestRecord record = new("2") { Childs = new ManyRelations<TestRecordChild>(new[] { "1", "2", "3" }) };
			DbContext.TestRecords.AddRange(new TestRecord[] { new("1"), record, new("3"), new("4") });
			DbContext.TestRecordChildren.AddRange(new TestRecordChild[] { new("1") { Value = 1 }, new("2") { Value = 2 }, new("3") { Value = 3 } });
			DbContext.SaveChanges();

			// When
			var request = $"{nameof(TestRecord)}/query?ids=1&ids=2&ids=3&ids=4";
			request += "&includes=Childs!Value<>2!Value<3!Value>=1";
			request += $"&{nameof(OrderBys)}=-Id";
			request += "&paging.take=2";
			request += "&filters=Id<>3";
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Query<TestRecord>>(content, JsonOptions);

			Assert.Equal(2, response.Data.Count());
			Assert.Equal("4", response.Data.First().Id);
			Assert.Equal("2", response.Data.Last().Id);
			Assert.Single(response.Included);
		}

		[Fact]
		public async Task WhenWeMakeAReportRequest_TheRequiredParamaterAreValidated()
		{
			// Given
			var request = $"{nameof(TestRecord)}/report";

			// When
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var errors = JsonSerializer.Deserialize<Dictionary<string, object>>(content, JsonOptions);

			Assert.Equal(2, errors.Count);
			Assert.NotNull(errors[nameof(GroupBies.Keys)]);
			Assert.NotNull(errors[nameof(GroupBies.Groups)]);
		}

		[Fact]
		public async Task WhenWeMakeAGetReportRequest_TheRequestsGoTroughTheGateway()
		{
			// Given
			DbContext.TestRecords.AddRange(new TestRecord[] {
				new("1") { StringValue = "1" , NestedObject = new("1-1") { Value = 1 } },
				new("2") { StringValue = "1" , NestedObject = new("2-1") { Value = 2 } },
				new("3") { StringValue = "2" , NestedObject = new("3-1") { Value = 3 } },
				new("4") { StringValue = "2" , NestedObject = new("4-1") { Value = 4 } },
				new("5") { StringValue = "2" , NestedObject = new("5-1") { Value = 0 } }
			});
			DbContext.SaveChanges();


			// When
			var request = $"{nameof(TestRecord)}/report?";
			request += $"&{nameof(GroupBies)}.Keys={nameof(TestRecord.StringValue)}";
			request += $"&{nameof(GroupBies)}.groups=DoubleSum:sum:{nameof(TestRecord.NestedObject)}.{nameof(TestRecordChild.Value)}";
			request += $"&{nameof(GroupBies)}.groups=Count:count:{nameof(TestRecord.Id)}";
			request += $"&{nameof(OrderBys)}=-{nameof(TestRecord.StringValue)}";
			var result = await Client.GetAsync(request);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Report>(content, JsonOptions);

			Assert.Equal(2, response.Count);

			var group1 = response.Last();
			Assert.Equal(2, group1.Values.Count);
			Assert.Equal(2, int.Parse(group1.Values["Count"].ToString()));
			Assert.Equal(1d + 2d, double.Parse(group1.Values["DoubleSum"].ToString()));

			var group2 = response.First();
			Assert.Equal(2, group2.Values.Count);
			Assert.Equal(3 + 4, double.Parse(group2.Values["DoubleSum"].ToString()));
			Assert.Equal(3, double.Parse(group2.Values["Count"].ToString()));
		}

		[Fact]
		public async Task WhenWeMakeACreateRequest_TheRequestsGoTroughTheGatewayAndreturnNewRecords()
		{
			// Given
			var body = new Create<TestRecord> { Data = new TestRecord[] { new() }, };

			// When
			var result = await Client.PostAsync($"{nameof(TestRecord)}", CreateStringContent(body));

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Many<TestRecord>>(content, JsonOptions);
			var data = response.Data.ToList();

			Assert.NotNull(data.First().Id);
			Assert.True(data.First().CreatedAt <= DateTimeOffset.UtcNow);
			Assert.Equal(data.First().CreatedAt, response.Data.First().UpdatedAt);

			Assert.Single(DbContext.TestRecords.ToEnumerable());
		}

		[Fact]
		public async Task WhenWeMakeAnUpdateRequest_TheRequestsGoTroughTheGateway()
		{
			// Given
			DbContext.TestRecords.Add(new("1"));
			DbContext.TestRecordChildren.Add(new("2") { Value = 2 });
			DbContext.SaveChanges();

			var arr = new object[] { 1, 2, 3, 4, 5 };
			Updates<TestRecord> update = new()
			{
				{
					"1",
					new(new Dictionary<string, object>
					{
						{ nameof(TestRecord.IntValue), 1 },
						{ nameof(TestRecord.DoubleValue), 2.2 },
						{ nameof(TestRecord.StringValue), "333" },
						{ nameof(TestRecord.ArrayValue), arr },
					})
				}
			};

			// When
			var result = await Client.PatchAsync($"{nameof(TestRecord)}", CreateStringContent(update));

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Many<TestRecord>>(content, JsonOptions);

			TestRecord updated = response.Data.First();
			Assert.Equal("1", updated.Id);
			Assert.Equal(1, updated.IntValue);
			Assert.Equal(2.2, updated.DoubleValue);
			Assert.Equal("333", updated.StringValue);
			Assert.Equal(arr.Select(i => $"{i}"), updated.ArrayValue.Select(i => $"{i}"));
			Assert.True(updated.CreatedAt != updated.UpdatedAt);

			Assert.Single(DbContext.TestRecords.ToEnumerable());
			Assert.Single(DbContext.TestRecordChildren.ToEnumerable());
		}

		[Fact]
		public async Task WhenWeMakeAnDeleteRequest_TheRequestsGoTroughTheGateway()
		{
			//Given
			DbContext.TestRecords.Add(new("1") { Childs = new(new[] { "2" }) });
			DbContext.SaveChanges();

			//When
			await Client.DeleteAsync($"{nameof(TestRecord)}?ids=1");

			//Then
			Assert.Empty(DbContext.TestRecords.ToEnumerable());
		}

		[Fact]
		public async Task WhenWeMakeAQueryRequestWithParameter_TheRecordsAreFilteredOrderedAndPaged()
		{
			// Given
			DbContext.TestRecords.AddRange(
				new TestRecord[] {
					new("0") { IntValue = 2 },
					new("1") { IntValue = 1 },
					new("2") { IntValue = 2 },
					new("3") { IntValue = 1 },
					new("4") { IntValue = 2 },
					new("5"), new("6"), new("7"), new("8"), new("9")
				}
			);
			DbContext.SaveChanges();

			// When
			var result = await Client.GetAsync(
				$"{nameof(TestRecord)}/query" +
				"?filters=Id<>9&filters=Id<>7&filters=Id<>5" +
				$"&{nameof(OrderBys)}=IntValue&{nameof(OrderBys)}=-id" +
				"&paging.skip=1&paging.take=2"
			);

			// Then
			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Query<TestRecord>>(content, JsonOptions);

			Assert.Equal(2, response.Data.Count());
			Assert.Equal("6", response.Data.First().Id);
			Assert.Equal("3", response.Data.Last().Id);
			Assert.Empty(response.Included);
		}

		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
	}
}
