using Microsoft.EntityFrameworkCore;

namespace OLWebApi.Sql.Gateway.Integration
{
	public class TestDbContext : ApiDbContext
	{
		public DbSet<TestRecord> TestRecords { get; set; }
		public DbSet<TestRecordChild> TestRecordChildren { get; set; }
		public DbSet<TestParentRecord> TestParentRecords { get; set; }

		public TestDbContext() { }
		public TestDbContext(string connections) : this(new DbContextOptionsBuilder().UseNpgsql(connections).Options) { }
		public TestDbContext(DbContextOptions options) : base(options) { }
	}
}