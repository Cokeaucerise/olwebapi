using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Docker.DotNet;
using Docker.DotNet.Models;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace OLWebApi.Sql.Gateway.Integration
{

	[CollectionDefinition("Sql")]
	public class DockerSqlFixture : IDisposable
	{
		protected CreateContainerResponse Container { get; set; }
		protected DockerClient Client = new DockerClientConfiguration().CreateClient();
		protected static readonly IConfigurationRoot Configuration = new ConfigurationBuilder()
			.AddEnvironmentVariables()
			.AddJsonFile("appsettings.json")
			.Build();

		public static string ConnectionString => Configuration.GetConnectionString("Sql");

		public DockerSqlFixture()
		{
			try
			{
				Start().Wait();
			}
			catch (Exception) { }
		}

		public async Task Start()
		{
			Container ??= await Client.Containers.CreateContainerAsync(new CreateContainerParameters
			{
				Image = "postgres",
				Name = "Postgres",
				Env = new string[] { "POSTGRES_PASSWORD=password" },
				ExposedPorts = new Dictionary<string, EmptyStruct>() { { $"{5432}", default } },
				HostConfig = CreateHostConfig(5432),
			});

			await Client.Containers.StartContainerAsync(Container.ID, new ContainerStartParameters());
		}

		private static HostConfig CreateHostConfig(int port)
		{
			var portBinding = new PortBinding
			{
				HostIP = "0.0.0.0",
				HostPort = $"{port}"
			};

			return new HostConfig
			{
				PortBindings = new Dictionary<string, IList<PortBinding>>
				{
					{ $"{port}/tcp", new[] { portBinding } }
				}
			};
		}

		public void Dispose()
		{
			if (Container != null)
			{
				Client.Containers.StopContainerAsync(Container.ID, new ContainerStopParameters()).Wait();
				Client.Containers.RemoveContainerAsync(Container.ID, new ContainerRemoveParameters()).Wait();
			}

			GC.SuppressFinalize(this);
		}
	}
}