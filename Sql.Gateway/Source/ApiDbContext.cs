using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Sql.Gateway
{
	public class ApiDbContext : DbContext
	{
		protected ApiDbContext() { }
		public ApiDbContext(DbContextOptions options) : base(options) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.UseValueConverterForType(typeof(ISingleRelation), typeof(SingleRelationConverter<>));
			modelBuilder.UseValueConverterForType(typeof(IManyRelations), typeof(ManyRelationsConverter<>));
		}
	}

	public class SingleRelationConverter<T> : ValueConverter<T, string>
	where T : class, ISingleRelation
	{
		public SingleRelationConverter() : base(mr => mr.Id, ids => new SingleRelation<IApiRecord>(ids) as T) { }
	}

	public class ManyRelationsConverter<T> : ValueConverter<T, string[]>
	where T : class, IManyRelations
	{
		public ManyRelationsConverter() : base(mr => mr.Ids.ToArray(), ids => new ManyRelations<IApiRecord>(ids) as T) { }
	}

	public static class ModelBuilderExtensions
	{
		public static ModelBuilder UseValueConverterForType<T>(this ModelBuilder modelBuilder, Type converter)
		{
			return modelBuilder.UseValueConverterForType(typeof(T), converter);
		}

		public static ModelBuilder UseValueConverterForType(this ModelBuilder modelBuilder, Type type, Type converter)
		{
			foreach (var entityType in modelBuilder.Model.GetEntityTypes())
				foreach (var property in entityType.PropertiesOfType(type))
					modelBuilder
						.Entity(entityType.Name)
						.Property(property.Name)
						.HasConversion(converter.GetValueConverter(property.PropertyType));

			return modelBuilder;
		}

		private static ValueConverter GetValueConverter(this Type converter, Type property)
		{
			if (converter.IsGenericType)
				return Activator.CreateInstance(converter.MakeGenericType(property)) as ValueConverter;
			else
				return Activator.CreateInstance(converter) as ValueConverter;
		}

		private static IEnumerable<PropertyInfo> PropertiesOfType(this Microsoft.EntityFrameworkCore.Metadata.IMutableEntityType entityType, Type type)
		{
			return entityType.ClrType.GetProperties().Where(p => p.PropertyType.IsAssignableTo(type));
		}
	}
}