using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Gateway.Queryable;
using OLWebApi.Query.Gateway;

namespace OLWebApi.Sql.Gateway
{
	[ExcludeFromCodeCoverage]
	public static class DependencyInjection
	{
		public static IServiceCollection AddSqlGateway(this IServiceCollection services)
		{
			services.AddQueryableGateway();
			services.AddConnections();

			services.AddScoped(typeof(ICrudApiGateway<>), typeof(SqlGateway<>));
			services.AddScoped(typeof(IQueryApiGateway<>), typeof(SqlGateway<>));

			return services;
		}
	}
}