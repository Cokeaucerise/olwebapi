using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway;
using OLWebApi.Crud.Gateway.Queryable;

namespace OLWebApi.Sql.Gateway
{
	public class SqlGateway<T> : QueryableGateway<T>, ICrudApiGateway<T> where T : ApiRecord
	{
		public DbContext Context { get; }
		public DbSet<T> Collection { get; }

		static readonly PropertyInfo UpdatedAtProperty = typeof(IApiRecord).GetProperty(nameof(IApiRecord.UpdatedAt));

		public SqlGateway(
			DbContext context,
			QueryableGatewayServices<T> services
		) : base(() => context.Set<T>().AsQueryable(), services)
		{
			Context = context;
			Collection = context.Set<T>();
		}

		public async Task<T> Get(string id)
		{
			return await base.Query(new(new[] { id }))
			.FirstOrDefaultAsync()
			.AsTask();
		}

		public async IAsyncEnumerable<T> Create(IEnumerable<T> records)
		{
			var now = DateTimeOffset.UtcNow;
			records = records.Select(r => r with
			{
				Id = r.Id ?? Guid.NewGuid().ToString(),
				CreatedAt = now,
				UpdatedAt = now,
			}).ToList();

			await Collection.AddRangeAsync(records);
			await Context.SaveChangesAsync();
			var ids = records.Select(r => r.Id);

			await foreach (var r in Query(new(ids)))
				yield return r;
		}

		public async IAsyncEnumerable<T> Update(Updates<T> updates)
		{
			var records = await Query(new(updates.Keys)).ToListAsync();

			foreach (var (record, update) in records.Select(r => (r, updates[r.Id])))
			{
				foreach (var u in update)
					u.Key.SetValue(record, u.Value);

				UpdatedAtProperty.SetValue(record, DateTimeOffset.UtcNow);
			}

			Collection.UpdateRange(records);
			await Context.SaveChangesAsync();

			await foreach (var r in Query(new(updates.Keys)))
				yield return r;
		}

		public async Task<bool> Delete(IEnumerable<string> ids)
		{
			var toRemove = await Query(new(ids)).ToListAsync();

			Collection.RemoveRange(toRemove);
			await Context.SaveChangesAsync();

			return true;
		}
	}
}