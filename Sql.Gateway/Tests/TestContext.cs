using Microsoft.EntityFrameworkCore;

namespace OLWebApi.Sql.Gateway.Tests
{
	public class TestContext : DbContext
	{
		protected TestContext() { }
		public TestContext(DbContextOptions options) : base(options) { }

		public DbSet<TestRecord> TestRecords { get; set; }
	}
}