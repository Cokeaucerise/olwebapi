using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Crud.Gateway.Queryable;
using OLWebApi.Sql.Gateway;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]
namespace OLWebApi.Sql.Gateway.Tests
{
	public class SQLGatewayTest : IDisposable
	{
		protected DbContext Context = new TestContext(Options);
		protected DbSet<TestRecord> Collection => Context.Set<TestRecord>();

		protected IServiceProvider Services = new ServiceCollection()
			.AddQueryableGateway()
			.BuildServiceProvider();

		protected SqlGateway<TestRecord> Gateway => new(Context, Services.GetService<QueryableGatewayServices<TestRecord>>());

		protected static DbContextOptions Options => new DbContextOptionsBuilder().UseInMemoryDatabase("test").Options;

		public SQLGatewayTest()
		{
			Collection.RemoveRange(Collection);
			Context.SaveChanges();
		}

		void IDisposable.Dispose()
		{
			GC.SuppressFinalize(this);
			Context.Dispose();
		}
	}
}