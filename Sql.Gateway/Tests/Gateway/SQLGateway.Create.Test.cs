using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace OLWebApi.Sql.Gateway.Tests
{
	public class SQLGatewayCreateTest : SQLGatewayTest
	{
		[Fact]
		public async Task Create_DefaultRecord_ReturnRecordWithIdAndCreated()
		{
			var before = DateTimeOffset.UtcNow;

			var result = await Gateway.Create(new[] { new TestRecord() }).FirstAsync();

			var after = DateTimeOffset.UtcNow;

			Assert.NotEmpty(result.Id);
			Assert.True(result.CreatedAt >= before);
			Assert.True(result.CreatedAt <= after);
		}

		[Fact]
		public async Task Create_RecordWithId_ReturnRecordWithSameId()
		{
			TestRecord testRecord = new(Guid.NewGuid().ToString());

			var result = await Gateway.Create(new[] { testRecord }).FirstAsync();

			Assert.Equal(testRecord.Id, result.Id);
		}

		[Fact]
		public async Task Create_ManyRecords_ReturnRecords()
		{
			TestRecord a = new("a"), b = new("b"), c = new("c");

			var result = await Gateway.Create(new[] { a, b, c }).ToListAsync();

			Assert.Equal(3, result.Count);
			Assert.NotNull(result.FirstOrDefault(r => r.Id == "a"));
			Assert.NotNull(result.FirstOrDefault(r => r.Id == "b"));
			Assert.NotNull(result.FirstOrDefault(r => r.Id == "c"));
		}

		[Fact]
		public async Task Create_RecordWithExistingId_ThrowException()
		{
			TestRecord testRecord = new(Guid.NewGuid().ToString());

			async Task create() => await Gateway.Create(new[] { testRecord, testRecord }).FirstAsync();

			await Assert.ThrowsAsync<InvalidOperationException>(create);
		}
	}
}
