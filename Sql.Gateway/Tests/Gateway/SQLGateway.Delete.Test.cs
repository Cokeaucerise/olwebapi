using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace OLWebApi.Sql.Gateway.Tests
{
	public class SQLGatewayDeleteTest : SQLGatewayTest
	{
		[Fact]
		public async Task Delete_RecordDoNotExist_ReturnTrue()
		{
			var result = await Gateway.Delete(new string[] { "test" });

			Assert.True(result);
		}

		[Fact]
		public async Task Delete_RecordExist_ReturnTrue()
		{
			await Gateway.Create(new TestRecord[] { new("test") }).ToListAsync();

			var result = await Gateway.Delete(new string[] { "test" });

			Assert.True(result);
		}

		[Fact]
		public async Task Delete_PartialRecordExist_ReturnTrue()
		{
			await Gateway.Create(new TestRecord[] { new("test") }).ToListAsync();

			var result = await Gateway.Delete(new string[] { "test", "test-2" });

			Assert.True(result);
		}
	}
}
