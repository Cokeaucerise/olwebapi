using System;
using System.Threading.Tasks;
using Xunit;

namespace OLWebApi.Sql.Gateway.Tests
{
	public class SQLGatewayGetTest : SQLGatewayTest
	{
		[Fact]
		public async Task Get_DoNotExists_ReturnNull()
		{
			var result = await Gateway.Get("1");

			Assert.Null(result);
		}

		[Fact]
		public async Task Get_NullId_ReturnNull()
		{
			var result = await Gateway.Get(null);

			Assert.Null(result);
		}

		[Fact]
		public async Task Get_Exists_ReturnRecord()
		{
			var record = Collection.Add(new TestRecord { Id = Guid.NewGuid().ToString(), Value = "test" }).Entity;
			Context.SaveChanges();

			var result = await Gateway.Get(record.Id);

			Assert.Equal(record, result);
		}
	}
}
