using System;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Action;
using Xunit;

namespace OLWebApi.Sql.Gateway.Tests
{
	public class SQLGatewayUpdateTest : SQLGatewayTest
	{
		[Fact]
		public async Task Update_UpdateFromRecord_ReturnRecordWithNewValueAndNewUpdatedAt()
		{
			TestRecord record = new("test") { Value = "value" };
			_ = await Gateway.Create(new[] { record }).ToListAsync();

			var before = DateTimeOffset.UtcNow;

			TestRecord updateRecord = record with { Value = "updated" };
			Updates<TestRecord> updates = new() { { record.Id, new Update<TestRecord>(updateRecord) } };
			var updated = await Gateway.Update(updates).FirstAsync();

			var after = DateTimeOffset.UtcNow;

			Assert.Equal(updateRecord.Value, updated.Value);
			Assert.True(before <= updated.UpdatedAt);
			Assert.True(after >= updated.UpdatedAt);
		}

		[Fact]
		public async Task Update_UpdateManyRecords_ReturnRecordsWithNewValues()
		{
			TestRecord record = new("test") { Value = "value" };
			_ = await Gateway.Create(new[] { record }).ToListAsync();

			TestRecord updateRecord = record with { Value = "updated" };
			Updates<TestRecord> updates = new() { { record.Id, new Update<TestRecord>(updateRecord) } };
			var updated = await Gateway.Update(updates).FirstAsync();

			Assert.Equal(updateRecord.Value, updated.Value);
		}

		[Fact]
		public async Task Update_UpdateRemoveId_ThrowException()
		{
			var records = await Gateway
				.Create(new[] { new TestRecord("a"), new TestRecord("b"), new TestRecord("c") })
				.ToListAsync();

			Updates<TestRecord> updates = new(records.ToDictionary(r => r.Id, r => new Update<TestRecord>(r with { Value = "update" })));
			var updated = await Gateway.Update(updates).ToListAsync();

			Assert.Equal("update", updated.Skip(0).First().Value);
			Assert.Equal("update", updated.Skip(1).First().Value);
			Assert.Equal("update", updated.Skip(2).First().Value);
		}

		[Fact]
		public async Task Update_UpdateChangeId_ThrowException()
		{
			TestRecord record = new("test") { Value = "value" };
			_ = await Gateway.Create(new[] { record }).ToListAsync();

			Updates<TestRecord> updates = new() { { record.Id, new Update<TestRecord>(record with { Id = "new" }) } };
			async Task update() => await Gateway.Update(updates).FirstAsync();

			await Assert.ThrowsAsync<InvalidOperationException>(update);
		}


		[Fact]
		public async Task Update_UpdateTargetDoNotExist_ReturnEmpty()
		{
			TestRecord record = new("test") { Value = "value" };

			Updates<TestRecord> updates = new() { { record.Id, new Update<TestRecord>(record with { Value = "updated" }) } };
			var updated = await Gateway.Update(updates).ToListAsync();

			Assert.Empty(updated);
		}

		[Fact]
		public async Task Update_UpdateTargetPartialExist_ReturnExistingRecordUpdates()
		{
			TestRecord record = new("test") { Value = "value" };
			_ = await Gateway.Create(new[] { record }).ToListAsync();

			Updates<TestRecord> updates = new()
			{
				{ record.Id, new Update<TestRecord>(record with { Value = "updated" }) },
				{ record.Id + "invalid", new Update<TestRecord>(record with { Value = "updated" }) }
			};
			var updated = await Gateway.Update(updates).ToListAsync();

			Assert.Single(updated);
		}
	}
}
