using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Sql.Gateway.Tests
{
	public record TestRecord : ApiRecord
	{
		public TestRecord() { }
		public TestRecord(string id) : base(id) { }
		protected TestRecord(ApiRecord original) : base(original) { }

		public int Integer { get; set; }
		public string Value { get; set; }
	}
}