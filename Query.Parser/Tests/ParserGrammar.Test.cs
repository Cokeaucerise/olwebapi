using System.Collections.Generic;
using System.Linq;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Query.Parser;
using Sprache;
using Xunit;

namespace OLWebApi.Query.Tests.Parser
{
	public class ParserGrammarTest
	{
		public ParserOptions Options { get; set; } = new();
		public List<IFilterOperator> FilterOperators { get; set; } = new()
		{
			new FilterEqualOperator(),
			new FilterNotEqualOperator(),
			new FilterGreaterOperator(),
			new FilterGreaterOrEqualOperator(),
			new FilterLessOperator(),
			new FilterLessOrEqualOperator(),
			new FilterInOperator(),
			new FilterNotInOperator()
		};

		public List<IPropertyAccumulator> GroupByAccumulators { get; set; } = new()
		{
			new SumAccumulator(),
			new CountAccumulator(),
		};

		public ParserGrammar Grammar => new(Options, FilterOperators, GroupByAccumulators);

		[Theory]
		[InlineData("=")]
		[InlineData("<>")]
		[InlineData("<")]
		[InlineData(">")]
		[InlineData("<=")]
		[InlineData(">=")]
		[InlineData("><")]
		[InlineData("><><")]
		public void ParseFitler_DifferentOperator(string op)
		{
			//Given
			var input = new Input($"id{op}1");

			//When
			var filter = Grammar.Filter(input).Value;

			//Then
			Assert.Equal(op, filter.Operator.Operator);
		}

		[Fact]
		public void ParseFitler_QuotedText()
		{
			//Given
			var input = new Input(@"id=""qwe asd zxc""");

			//When
			var filter = Grammar.Filter(input).Value;

			//Then
			Assert.Equal("qwe asd zxc", filter.Value);
		}

		[Fact]
		public void ParseFitler_QuotedTextWithControlCharacters()
		{
			//Given
			var input = new Input("id=\"<qwe.asd!zxc>\n\"");

			//When
			var filter = Grammar.Filter(input).Value;

			//Then
			Assert.Equal("<qwe.asd!zxc>\n", filter.Value);
		}

		[Fact]
		public void ParseLink_NoFilter()
		{
			var input = new Input("PropertyA");

			var link = Grammar.Link(input).Value;

			Assert.Equal("PropertyA", link.Name);
		}

		[Fact]
		public void ParseLink_IncludeIdentifier()
		{
			var input = new Input("*PropertyA");

			var link = Grammar.Link(input).Value;

			Assert.True(link.Include);
		}

		[Fact]
		public void ParseLink_SingleFilter()
		{
			var input = new Input("PropertyA!id=1");

			var link = Grammar.Link(input).Value;

			Assert.Single(link.Filters);
			Assert.Equal("id", link.Filters.First().Target);
			Assert.Equal("=", link.Filters.First().Operator.Operator);
			Assert.Equal("1", link.Filters.First().Value);
		}

		[Fact]
		public void ParseLink_ManyFilters()
		{
			var input = new Input("PropertyA!id=1!value=qwe");

			var link = Grammar.Link(input).Value;

			Assert.Equal("id", link.Filters.First().Target);
			Assert.Equal("=", link.Filters.First().Operator.Operator);
			Assert.Equal("1", link.Filters.First().Value);

			Assert.Equal("value", link.Filters.Last().Target);
			Assert.Equal("=", link.Filters.Last().Operator.Operator);
			Assert.Equal("qwe", link.Filters.Last().Value);
		}

		[Fact]
		public void ParseChain_ManyPropertyNoFilter()
		{
			var input = new Input("PropertyA.PropertyB.PropertyC");

			var chain = Grammar.Chain(input).Value;

			Assert.Equal("PropertyA", chain.ElementAt(0).Name);
			Assert.Equal("PropertyB", chain.ElementAt(1).Name);
			Assert.Equal("PropertyC", chain.ElementAt(2).Name);
		}

		[Fact]
		public void ParseChain_ManyPropertySingleFilter()
		{
			var input = new Input("PropertyA!a=1.PropertyB!b=2.PropertyC!c=3");

			var chain = Grammar.Chain(input).Value;

			Assert.Equal("a", chain.ElementAt(0).Filters.First().Target);
			Assert.Equal("1", chain.ElementAt(0).Filters.First().Value);

			Assert.Equal("b", chain.ElementAt(1).Filters.First().Target);
			Assert.Equal("2", chain.ElementAt(1).Filters.First().Value);

			Assert.Equal("c", chain.ElementAt(2).Filters.First().Target);
			Assert.Equal("3", chain.ElementAt(2).Filters.First().Value);
		}

		[Fact]
		public void ParseChain_ManyPropertyManyFilter()
		{
			var input = new Input("PropertyA!a=1!aa=11.PropertyB!b=2!bb=22");

			var chain = Grammar.Chain(input).Value;

			Assert.Equal("a", chain.ElementAt(0).Filters.First().Target);
			Assert.Equal("1", chain.ElementAt(0).Filters.First().Value);
			Assert.Equal("aa", chain.ElementAt(0).Filters.Last().Target);
			Assert.Equal("11", chain.ElementAt(0).Filters.Last().Value);

			Assert.Equal("b", chain.ElementAt(1).Filters.First().Target);
			Assert.Equal("2", chain.ElementAt(1).Filters.First().Value);
			Assert.Equal("bb", chain.ElementAt(1).Filters.Last().Target);
			Assert.Equal("22", chain.ElementAt(1).Filters.Last().Value);
		}

		[Fact]
		public void ParseInclude_SingleChainManyPropertyManyFilter()
		{
			var input = new Input("PropertyA!a=1!aa=11.PropertyB!b=2!bb=22\n");

			var include = Grammar.Include(input).Value;

			Assert.Single(include);
			Assert.Equal(2, include.First().Count);
		}

		[Fact]
		public void ParseInclude_ManyChainManyPropertyManyFilter()
		{
			var input = new Input(
				"PropertyA!a=1!aa=11.PropertyB!b=2!bb=22\n" +
				"PropertyA!a=1!aa=11.PropertyB!b=2!bb=22\n");

			var include = Grammar.Include(input).Value;

			Assert.Equal(2, include.Count);
			Assert.Equal(2, include.First().Count);
		}
	}
}