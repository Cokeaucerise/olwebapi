using System;
using System.Collections.Generic;
using System.Linq;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Entities.Query;
using Sprache;

namespace OLWebApi.Query.Parser
{
	public class ParserGrammar
	{
		protected const char ORDER_BY_ASCENDING = '+';
		protected const char ORDER_BY_DESCENDING = '-';

		protected readonly ParserOptions Options;
		private readonly IEnumerable<IFilterOperator> FilterOperators;
		private readonly IEnumerable<IPropertyAccumulator> GroupByAccumulators;
		protected static readonly Parser<char> Quote = Parse.Char('"');
		protected readonly Parser<string> FilterOperator;
		protected readonly Parser<string> GroupByAccumulator;

		public ParserGrammar(ParserOptions options, IEnumerable<IFilterOperator> filterOperators, IEnumerable<IPropertyAccumulator> groupByAccumulators)
		{
			Options = options;
			FilterOperators = filterOperators;
			GroupByAccumulators = groupByAccumulators;
			FilterOperator = BuildFilterOperator(filterOperators);
			GroupByAccumulator = BuildGroupByOperator(groupByAccumulators);
		}

		public Parser<Includes> Include =>
			from chains in Chain.XMany().End()
			select new Includes(chains.Where(c => c != null));

		public Parser<PropertyChain> Chain =>
			from links in Link.Many()
			from terminator in Terminator
			select !links.Any(l => l == null) ? new PropertyChain(links) : default;

		public Parser<PropertyLink> Link =>
			from include in Parse.String(Options.IncludeIdentifier).Once().Optional()
			from name in PropertyName
			from filters in Filters
			select !string.IsNullOrEmpty(name) ?
				new PropertyLink(name, new PropertyFilters(filters)) { Include = include.IsDefined } :
				default;

		public Parser<IEnumerable<PropertyFilter>> Filters =>
			from filters in Parse.String(Options.FilterSeparator).Then(_ => Filter).Many().Optional()
			from terminator in FilterTerminator
			select filters.GetOrDefault();

		public Parser<PropertyFilter> Filter =>
			from target in PropertyName
			from op in FilterOperator
			from value in QuotedText.Or(Parse.LetterOrDigit.Many()).Text()
			select new PropertyFilter(target, value, FilterOperators.FirstOrDefault(fo => fo.Operator == op));


		public Parser<OrderBy> OrderBy =>
			from direction in Orderedirection.Optional()
			from name in PropertyName
			select new OrderBy { Ascending = direction.GetOrElse(true), Target = name };

		public Parser<GroupBy> GroupBy =>
			from name in QuotedText.Or(Parse.LetterOrDigit.Many()).Text()
			from splitterA in Parse.String(Options.GroupBySeparator).Once()
			from accumulator in GroupByAccumulator
			from splitterB in Parse.String(Options.GroupBySeparator).Once()
			from chain in Chain
			select new GroupBy(name, string.Join(Options.PropertySeparator, chain.Select(c => c.Name)), GroupByAccumulators.First(a => a.Operator == accumulator));

		protected static Parser<bool> Orderedirection =>
			from direction in Parse.Chars(ORDER_BY_DESCENDING, ORDER_BY_ASCENDING).Once().Text()
			select direction == ORDER_BY_ASCENDING.ToString();

		protected Parser<string> FilterTerminator =>
			Parse.String(Options.PropertySeparator).Or(Parse.String(string.Empty)).Text();

		protected static Parser<string> BuildFilterOperator(IEnumerable<IFilterOperator> filterOperators)
		{
			Parser<IEnumerable<char>> parser = default;
			foreach (var op in filterOperators.OrderByDescending(fo => fo.Operator.Length))
				parser = parser == null ?
					Parse.String(op.Operator) :
					parser.Or(Parse.String(op.Operator));

			return parser.Text();
		}

		protected static Parser<string> BuildGroupByOperator(IEnumerable<IPropertyAccumulator> groupByAccumulators)
		{
			Parser<IEnumerable<char>> parser = default;
			foreach (var op in groupByAccumulators.OrderByDescending(fo => fo.Operator.Length))
				parser = parser == null ?
					Parse.String(op.Operator) :
					parser.Or(Parse.String(op.Operator));

			return parser.Text();
		}

		protected Parser<string> PropertyName =>
			Parse.LetterOrDigit
				.Except(Parse.String(Options.FilterSeparator))
				.Except(Parse.String(Options.PropertySeparator))
				.Many()
				.Text();

		protected static readonly Parser<char> QuotedContent =
			Parse.AnyChar.Except(Quote).Or(Escaped(Quote));

		protected static readonly Parser<string> QuotedText =
			from open in Quote
			from content in QuotedContent.Many().Text()
			from end in Quote
			select content;

		protected static Parser<T> Escaped<T>(Parser<T> following) =>
			from escape in Quote
			from f in following
			select f;

		protected static readonly Parser<string> NewLine =
			Parse.String(Environment.NewLine).Text();

		protected static readonly Parser<string> Terminator =
			Parse.Return(string.Empty).End()
			.XOr(NewLine.End())
			.Or(NewLine);
	}
}