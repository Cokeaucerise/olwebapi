using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Parser.ModelBinders;

namespace OLWebApi.Query.Parser.Extensions
{
	[ExcludeFromCodeCoverage]
	public static class DependencyInjectionExtensions
	{
		public static IServiceCollection AddQueryParser(this IServiceCollection services) => services.AddQueryParser(new());
		public static IServiceCollection AddQueryParser(this IServiceCollection services, ParserOptions options)
		{
			services.AddSingleton(options);
			services.AddSingleton(p => new ParserGrammar(
				p.GetService<ParserOptions>(),
				p.GetServices<IFilterOperator>(),
				p.GetServices<IPropertyAccumulator>()
			));

			services.AddMvc().AddMvcOptions(options =>
			{
				options.ModelBinderProviders.Insert(0, new ModelBinderProvider<Includes, IncludesModelBinder>());
				options.ModelBinderProviders.Insert(0, new ModelBinderProvider<PropertyFilters, FiltersModelBinder>());
				options.ModelBinderProviders.Insert(0, new ModelBinderProvider<OrderBys, OrderBysModelBinder>());
				options.ModelBinderProviders.Insert(0, new ModelBinderProvider<GroupBy, GroupByModelBinder>());
			});

			return services;
		}
	}
}