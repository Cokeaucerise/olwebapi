using System.Diagnostics.CodeAnalysis;

namespace OLWebApi.Query.Parser
{
	[ExcludeFromCodeCoverage]
	public class ParserOptions
	{
		public string PropertySeparator { get; set; } = ".";
		public string FilterSeparator { get; set; } = "!";
		public string PagingSeparator { get; set; } = ":";
		public string GroupBySeparator { get; set; } = ":";
		public string IncludeIdentifier { get; set; } = "*";
	}
}