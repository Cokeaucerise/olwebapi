using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace OLWebApi.Query.Parser.ModelBinders
{
	public class ModelBinderProvider<TModel, TModelBinder> : IModelBinderProvider
	where TModel : class, new()
	where TModelBinder : IModelBinder<TModel>
	{
		public IModelBinder GetBinder(ModelBinderProviderContext context)
		{
			if (context == null)
				throw new ArgumentNullException(nameof(context));

			if (context.Metadata.ModelType == typeof(TModel))
				return new BinderTypeModelBinder(typeof(TModelBinder));

			return null;
		}
	}
}