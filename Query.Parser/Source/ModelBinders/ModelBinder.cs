using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace OLWebApi.Query.Parser.ModelBinders
{
	public interface IModelBinder<T> : IModelBinder
	where T : class, new()
	{ }

	public abstract class ModelBinder<T> : IModelBinder<T>
	where T : class, new()
	{
		protected readonly ParserGrammar Grammar = default;
		public ModelBinder(ParserGrammar grammar)
		{
			Grammar = grammar;
		}

		public abstract Task BindModelAsync(ModelBindingContext bindingContext);

		protected Task BindModelAsync(ModelBindingContext bindingContext, Func<ValueProviderResult, T> modelFactory)
		{
			if (bindingContext == null)
				throw new ArgumentNullException(nameof(bindingContext));

			var modelName = bindingContext.ModelName;

			var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
			if (valueProviderResult == ValueProviderResult.None)
				return Task.CompletedTask;

			bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

			var model = modelFactory.Invoke(valueProviderResult) ?? new T();

			bindingContext.Result = ModelBindingResult.Success(model);
			return Task.CompletedTask;
		}
	}
}