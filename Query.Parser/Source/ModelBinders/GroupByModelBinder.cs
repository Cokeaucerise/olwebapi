using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OLWebApi.Query.Entities.Query;
using Sprache;

namespace OLWebApi.Query.Parser.ModelBinders
{
	public class GroupByModelBinder : ModelBinder<GroupBy>
	{
		public GroupByModelBinder(ParserGrammar grammar) : base(grammar) { }

		public override Task BindModelAsync(ModelBindingContext bindingContext)
		{
			return BindModelAsync(bindingContext, value => Grammar.GroupBy(new Input(value.FirstValue)).Value);
		}
	}
}