using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OLWebApi.Query.Entities.Includes;
using Sprache;

namespace OLWebApi.Query.Parser.ModelBinders
{
	public class IncludesModelBinder : ModelBinder<Includes>
	{
		public IncludesModelBinder(ParserGrammar grammar) : base(grammar) { }

		public override Task BindModelAsync(ModelBindingContext bindingContext)
		{
			return BindModelAsync(bindingContext, value => new(value.Values.Select(v => Grammar.Chain(new Input(v)).Value)));
		}
	}
}