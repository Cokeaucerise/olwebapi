using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using OLWebApi.Query.Entities.Query;
using Sprache;

namespace OLWebApi.Query.Parser.ModelBinders
{
	public class OrderBysModelBinder : ModelBinder<OrderBys>
	{
		public OrderBysModelBinder(ParserGrammar grammar) : base(grammar) { }

		public override Task BindModelAsync(ModelBindingContext bindingContext)
		{
			return BindModelAsync(bindingContext, value => new(value.Values.Select(v => Grammar.OrderBy(new Input(v)).Value)));
		}
	}
}