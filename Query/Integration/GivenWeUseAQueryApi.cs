using System.Net.Http;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using OLWebApi.Core;
using OLWebApi.Core.JsonConverters;
using OLWebApi.Query.Gateway;
using Xunit;

namespace OLWebApi.Query.Integration
{
	public class GivenWeUseAQueryApi : IClassFixture<ApiFactory>
	{
		protected WebApplicationFactory<Startup> Factory;
		protected readonly Mock<IApiInteractor<TestRecord>> Interactor = new();
		protected readonly Mock<IQueryApiGateway<TestRecord>> Gateway = new();
		protected readonly Mock<IQueryApiGateway<TestRecordChild>> GatewayChild = new();

		protected static JsonSerializerOptions JsonOptions
		{
			get
			{
				JsonSerializerOptions options = new() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
				options.Converters.Add(new UpdateJsonConverterfactory());
				return options;
			}
		}

		protected static StringContent CreateStringContent(object body) => new(JsonSerializer.Serialize(body, JsonOptions), Encoding.UTF8, "application/json");

		public GivenWeUseAQueryApi(ApiFactory factory)
		{
			Factory = factory.WithWebHostBuilder(builder =>
			{
				builder.ConfigureServices(services =>
				{
					services.AddTransient(p => Gateway.Object);         //Overide open type service
					services.AddTransient(p => GatewayChild.Object);    //Overide open type service
				});
			});
		}
	}
}