using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Query.Integration
{
	public class GivenWeMakeAQueryController : GivenWeUseAQueryApi
	{
		public GivenWeMakeAQueryController(ApiFactory factory) : base(factory) { }

		[Fact]
		public async Task WhenWeMakeAGetQueryRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var client = Factory.CreateClient();

			// When
			var result = await client.GetAsync($"{nameof(TestRecord)}/query?ids=1");

			// Then
			Gateway.Verify(g => g.Query(It.IsAny<QueryParameter>()), Times.Once());
		}

		[Fact]
		public async Task WhenWeMakeAPostQueryRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var client = Factory.CreateClient();
			QueryParameter parameter = new()
			{
				Ids = new[] { "1" },
				Paging = new(0, 10),
				Filters = new() { new PropertyFilter<FilterInOperator>(nameof(TestRecord.TestValidationValue), "-1,0,1") },
				OrderBys = new() { new OrderBy { Ascending = false, Target = nameof(TestRecord.Id) } }
			};

			// When
			var result = await client.PostAsync($"{nameof(TestRecord)}/query", CreateStringContent(parameter));

			// Then
			Gateway.Verify(g => g.Query(It.IsAny<QueryParameter>()), Times.Once());
		}

		[Fact]
		public async Task WhenWeMakeAGetReportRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var client = Factory.CreateClient();

			// When
			string requestUri = $"{nameof(TestRecord)}/Report" +
				$"?groupbies.keys={nameof(TestRecord.Id)}" +
				$"&groupbies.groups=Count:count:{nameof(TestRecord.Id)}";
			var result = await client.GetAsync(requestUri);

			// Then
			Gateway.Verify(g => g.Report(It.IsAny<ReportParameter>()), Times.Once());
		}

		[Fact]
		public async Task WhenWeMakeAPostReportRequest_TheRequestsGoToTheGateway()
		{
			// Given
			var client = Factory.CreateClient();

			// When
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new HashSet<string> { nameof(TestRecord.Id) },
					Groups = new[] { new GroupBy<CountAccumulator>("Count", nameof(TestRecord.Id)) }
				}
			};
			var result = await client.PostAsync($"{nameof(TestRecord)}/Report", CreateStringContent(parameter));

			// Then
			Gateway.Verify(g => g.Report(It.IsAny<ReportParameter>()), Times.Once());
		}

		[Fact]
		public async Task WhenWeMakeAQueryRequestWithInclude_TheIncludesAreCalled()
		{
			// Given
			Gateway
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(new TestRecord[] { new() { Id = "1", Childs = new(new[] { "2" }) } }.ToAsyncEnumerable());

			GatewayChild
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(new TestRecordChild[] { new("2") }.ToAsyncEnumerable());

			var client = Factory.CreateClient();

			// When
			var result = await client.GetAsync($"{nameof(TestRecord)}/query?ids=1&includes={nameof(TestRecord.Childs)}");

			// Then
			Gateway.Verify(g => g.Query(It.IsAny<QueryParameter>()), Times.Once());
			GatewayChild.Verify(g => g.Query(It.IsAny<QueryParameter>()), Times.Once());

			var content = await result.Content.ReadAsStringAsync();
			var response = JsonSerializer.Deserialize<Query<TestRecord>>(content, JsonOptions);

			Assert.Equal("1", response.Data.First().Id);
			Assert.Equal("2", response.Included.First().Id);
		}
	}
}