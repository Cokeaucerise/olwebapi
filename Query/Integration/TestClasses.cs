using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Gateway;

namespace OLWebApi.Query.Integration
{
	public record TestRecord : ApiRecord
	{
		public TestRecord() { }
		public TestRecord(string id) : base(id) { }
		public ManyRelations<TestRecordChild> Childs { get; set; }
		public int TestValidationValue { get; set; }
	}

	public record TestRecordChild : ApiRecord
	{
		public TestRecordChild() { }
		public TestRecordChild(string id) : base(id) { }
	}

	public class TestApiGateway<T> : IQueryApiGateway<T>
	where T : IApiRecord
	{
		public IAsyncEnumerable<T> Query(QueryParameter parameter)
		{
			throw new NotImplementedException();
		}

		public Task<Report> Report(ReportParameter parameter)
		{
			throw new NotImplementedException();
		}
	}
}