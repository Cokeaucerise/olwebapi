using System.Collections.Generic;
using System.Linq;

namespace OLWebApi.Query.Entities.Includes
{
	public class PropertyChain : List<PropertyLink>
	{
		public PropertyChain() { }

		public PropertyChain(IEnumerable<PropertyLink> links) : base(links) { }

		public static implicit operator PropertyChain(string[] names) => new(names.Select(name => (PropertyLink)name));
	}
}