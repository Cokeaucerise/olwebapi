using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Query.Entities.Includes
{
	public class PropertyLink
	{
		public string Name { get; set; }
		public bool Include { get; set; }
		public PropertyFilters Filters { get; set; } = new();

		public PropertyLink() { }

		public PropertyLink(string name)
		{
			Name = name;
		}

		public PropertyLink(string name, PropertyFilters filters)
		{
			Name = name;
			Filters = filters ?? new();
		}

		public static implicit operator PropertyLink(string name) => new(name);
	}
}