using System.Collections.Generic;

namespace OLWebApi.Query.Entities.Includes
{
	public class Includes : List<PropertyChain>
	{
		public Includes() { }

		public Includes(IEnumerable<PropertyChain> chains) : base(chains) { }

		public void Add(params string[] names) => Add((PropertyChain)names);

		public static implicit operator Includes(PropertyChain[] chains) => chains != null ? new(chains) : null;
	}
}