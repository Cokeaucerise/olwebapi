using System.Collections.Generic;
using OLWebApi.Core.Entities.Action;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Query.Entities.Action
{
	public class Query<T> : Many<T> where T : IApiRecord
	{
		public Query()
		{
			Data = new List<T>();
			Included = new List<IApiRecord>();
		}

		public Query(IEnumerable<T> data)
		{
			Data = data ?? new List<T>();
			Included = new List<IApiRecord>();
		}

		public Query(IEnumerable<T> data, IEnumerable<IApiRecord> included)
		{
			Data = data ?? new List<T>();
			Included = included ?? new List<IApiRecord>();
		}

		public IEnumerable<IApiRecord> Included { get; set; }
	}
}