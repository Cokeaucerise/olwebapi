using System;
using System.Collections.Generic;
using System.Linq;

namespace OLWebApi.Query.Entities.Action
{
	public class Report : List<ReportEntry>
	{
		public Report() { }
		public Report(IEnumerable<ReportEntry> collection) : base(collection) { }
	}

	public class ReportEntry
	{
		public Dictionary<string, object> Keys { get; set; }
		public Dictionary<string, object> Values { get; set; }

		public ReportEntry() { }

		public ReportEntry(ReportGroupKey key, ReportGroupValue value)
		{
			Keys = key;
			Values = value;
		}
	}

	public class ReportGroupKey : Dictionary<string, object>
	{
		public ReportGroupKey() { }
		public ReportGroupKey(IEnumerable<KeyValuePair<string, object>> collection) : base(collection) { }

		public ReportGroupKey(IDictionary<string, object> dictionary) : base(dictionary)
		{
		}

		public override bool Equals(object obj)
		{
			if (obj is ReportGroupKey other)
				return other.Keys.SequenceEqual(Keys) && other.Values.SequenceEqual(Values);

			return false;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(
				Keys.Aggregate(0, (a, b) => HashCode.Combine(a, b.GetHashCode())),
				Values.Aggregate(0, (a, b) => HashCode.Combine(a, b.GetHashCode()))
			);
		}
	}

	public class ReportGroupValue : Dictionary<string, object>
	{
		public ReportGroupValue() { }
		public ReportGroupValue(IEnumerable<KeyValuePair<string, object>> collection) : base(collection) { }
	}
}