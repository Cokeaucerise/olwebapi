using System;
using System.Collections.Generic;
using System.Linq;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Provider;
using OLWebApi.Query.Provider;

namespace OLWebApi.Query.Entities.Query
{
	public class OrderBys : List<OrderBy>
	{
		public OrderBys() { }
		public OrderBys(IEnumerable<OrderBy> collection) : base(collection) { }
		public OrderBys(int capacity) : base(capacity) { }

		public IEnumerable<T> Order<T>(IEnumerable<T> records) where T : IApiRecord => Order<T>(records, new CaseInsensitivePropertyValueProvider());
		public IEnumerable<T> Order<T>(IEnumerable<T> records, IPropertyValueProvider valueProvider) where T : IApiRecord
		{
			foreach (var orderBy in this)
			{
				object selector(T record) => valueProvider.GetPropertyValue(orderBy.Target, record);
				records = orderBy.Order(records, selector);
			}

			return records;
		}

		public static implicit operator OrderBys(string[] targets) => new(targets.Cast<OrderBy>());
	}

	public record OrderBy
	{

		public string Target { get; set; }
		public bool Ascending { get; set; } = true;

		public OrderBy() { }

		public IEnumerable<T> Order<T>(IEnumerable<T> records, Func<T, object> selector) where T : IApiRecord
		{
			return OrderBy.Order(records, selector, Ascending);
		}

		private static IEnumerable<T> Order<T>(IEnumerable<T> records, Func<T, object> selector, bool ascending) where T : IApiRecord
		{
			if (records is IOrderedEnumerable<T> ordered)
				return ascending ? ordered.ThenBy(selector) : ordered.ThenByDescending(selector);
			else
				return ascending ? records.OrderBy(selector) : records.OrderByDescending(selector);
		}

		public static implicit operator OrderBy(string target) => new() { Target = target };
	}
}