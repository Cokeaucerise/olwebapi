using System.Collections.Generic;
using OLWebApi.Core.Entities.Filters;

namespace OLWebApi.Query.Entities.Query
{
	public class PropertyFilters : List<PropertyFilter>
	{
		public PropertyFilters() { }
		public PropertyFilters(IEnumerable<PropertyFilter> collection) : base(collection) { }
	}
}