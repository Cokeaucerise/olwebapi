using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Provider;

namespace OLWebApi.Query.Entities.Query
{
	public class GroupBies
	{
		[Required]
		public HashSet<string> Keys { get; set; }

		[Required]
		public IEnumerable<GroupBy> Groups { get; set; }
	}

	public class GroupBy<TGroupByOperator> : GroupBy
	where TGroupByOperator : class, IPropertyAccumulator, new()
	{
		public GroupBy(string name, string field) : base(name, field, new TGroupByOperator()) { }
	}

	public class GroupBy
	{
		public string Name { get; set; }
		public string Field { get; set; }
		public IPropertyAccumulator Accumulator { get; set; }

		public GroupBy() { }
		public GroupBy(string name, string field, IPropertyAccumulator accumulator)
		{
			Name = name;
			Field = field;
			Accumulator = accumulator;
		}
	}
}