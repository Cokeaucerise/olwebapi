using System.ComponentModel.DataAnnotations;

namespace OLWebApi.Query.Entities.Query
{
	public record ReportParameter : QueryParameter
	{
		[Required]
		public GroupBies GroupBies { get; set; }
	}
}