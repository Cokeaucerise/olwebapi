using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace OLWebApi.Query.Entities.Query
{
	[ExcludeFromCodeCoverage]
	public record QueryParameter
	{
		public IEnumerable<string> Ids { get; set; } = Array.Empty<string>();
		public Paging Paging { get; set; } = new();
		public PropertyFilters Filters { get; set; } = new();
		public OrderBys OrderBys { get; set; } = new();

		public QueryParameter() { }
		public QueryParameter(IEnumerable<string> ids) : this(ids, new(ids.Count()), new(), new()) { }
		public QueryParameter(Paging paging, PropertyFilters filters, OrderBys orderBys) : this(Array.Empty<string>(), paging, filters, orderBys) { }
		public QueryParameter(IEnumerable<string> ids, Paging paging, PropertyFilters filters, OrderBys orderBys)
		{
			Ids = ids;
			Paging = paging;
			Filters = filters;
			OrderBys = orderBys;
		}
	}
}
