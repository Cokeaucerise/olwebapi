using System.Diagnostics.CodeAnalysis;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Query.Entities.Query
{
	[ExcludeFromCodeCoverage]
	public record ControllerQueryParameter : QueryParameter
	{
		public Includes.Includes Includes { get; set; }
	}
}