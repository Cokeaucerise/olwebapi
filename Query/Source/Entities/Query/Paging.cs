using System.Collections.Generic;
using System.Linq;

namespace OLWebApi.Query.Entities.Query
{
	public record Paging
	{
		public int? Skip { get; set; }
		public int? Take { get; set; }

		public Paging() { }
		public Paging(int? take)
		{
			if (take.HasValue) Take = take.Value;
		}

		public Paging(int? skip, int? take)
		{
			if (skip.HasValue) Skip = skip.Value;
			if (take.HasValue) Take = take.Value;
		}

		public IEnumerable<T> GetPage<T>(IEnumerable<T> records)
		{
			return records.Skip(Skip ?? default).Take(Take ?? int.MaxValue);
		}
	}
}