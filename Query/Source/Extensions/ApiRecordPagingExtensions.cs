using System.Collections.Generic;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Query.Extensions
{
	public static class ApiRecordPagingExtensions
	{
		public static IEnumerable<T> GetPage<T>(this IEnumerable<T> records, Paging paging)
		=> paging?.GetPage(records) ?? records;
	}
}