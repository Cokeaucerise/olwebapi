using System;
using System.Collections.Generic;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Provider;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Query.Extensions
{
	public static class ApiRecordOrdererExtensions
	{
		public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> records, OrderBys orderBys, IPropertyValueProvider valueLoader) where T : IApiRecord
			=> orderBys?.Order(records, valueLoader) ?? records;

		public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> records, OrderBy orderBy, Func<T, object> selector) where T : IApiRecord
			=> orderBy?.Order(records, selector) ?? records;
	}

}