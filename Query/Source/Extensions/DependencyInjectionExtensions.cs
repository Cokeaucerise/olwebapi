using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using OLWebApi.Core.Server;
using OLWebApi.Query.Controller;
using OLWebApi.Query.Gateway;
using OLWebApi.Query.Interactor;
using OLWebApi.Query.Provider;
using OLWebApi.Query.Provider.InclusionResolver;
using OLWebApi.Query.Repository;

namespace OLWebApi.Query.Server.Extensions
{
	[ExcludeFromCodeCoverage]
	public static class DependencyInjection
	{
		public static void AddScopedQueryApiGateway(this IServiceCollection services, Type implementationGateway)
		{
			services.AddScoped(typeof(IQueryApiGateway<>), implementationGateway);
		}

		public static IServiceCollection AddQueryWebApi(this IServiceCollection services)
		{
			services.AddWebApi();

			services.TryAddTransient<IApiRecordProvider, ApiRecordProvider>();

			services.TryAddScoped(typeof(IQueryApiController<>), typeof(QueryApiController<>));
			services.TryAddScoped(typeof(IQueryApiInteractor<>), typeof(QueryApiInteractor<>));
			services.TryAddScoped(typeof(IQueryApiRepository<>), typeof(QueryApiRepository<>));

			services.AddInclusionResolvers();

			return services;
		}

		public static IServiceCollection AddInclusionResolvers(this IServiceCollection services)
		{
			services.AddTransient<IInclusionResolver, ObjectInclusionResolver>();
			services.AddTransient<IInclusionResolver, EnumerableInclusionResolver>();
			services.AddTransient<IInclusionResolver, SingleRelationsInclusionResolver>();
			services.AddTransient<IInclusionResolver, ManyRelationsInclusionResolver>();

			return services;
		}
	}
}