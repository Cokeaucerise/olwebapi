using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Repository;

namespace OLWebApi.Query.Provider.InclusionResolver
{
	public abstract class RelationsInclusionResolver : IInclusionResolver
	{
		protected readonly IServiceProvider Provider;

		public RelationsInclusionResolver(IServiceProvider provider)
		{
			Provider = provider;
		}

		public abstract bool CanResolveInclusion(Type type, PropertyLink link);

		public abstract Task<IEnumerable<object>> Resolve(IEnumerable<object> objects, PropertyLink link);

		protected virtual Task<IEnumerable<IApiRecord>> GetRelationsRecords(Type type, IEnumerable<string> ids, PropertyLink link)
		{
			return QueryApiRecord(
				type,
				new QueryParameter
				{
					Ids = ids,
					Paging = new(ids.Count()),
					Filters = link.Filters,
					OrderBys = new()
				}
			);
		}

		protected virtual async Task<IEnumerable<IApiRecord>> QueryApiRecord(Type type, QueryParameter parameter)
		{
			return (await FindRepository<IQueryApiRepository>(type)?.Query(parameter)?.ToListAsync().AsTask()) ?? default;
		}

		private TRepository FindRepository<TRepository>(Type recordType)
		where TRepository : class
		{
			var repoType = typeof(IQueryApiRepository<>).MakeGenericType(recordType);
			return Provider.GetService(repoType) as TRepository;
		}
	}
}
