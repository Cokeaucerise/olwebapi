using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Query.Provider.InclusionResolver
{
	public class ObjectInclusionResolver : IInclusionResolver
	{
		public bool CanResolveInclusion(Type type, PropertyLink link)
		{
			return type.IsSubclassOf(typeof(object));
		}

		public Task<IEnumerable<object>> Resolve(IEnumerable<object> objects, PropertyLink link)
		{
			return Task.FromResult(objects);
		}
	}
}