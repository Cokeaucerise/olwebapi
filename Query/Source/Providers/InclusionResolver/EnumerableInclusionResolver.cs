using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Query.Provider.InclusionResolver
{
	public class EnumerableInclusionResolver : IInclusionResolver
	{
		public bool CanResolveInclusion(Type type, PropertyLink link)
		{
			return type.IsAssignableTo(typeof(IEnumerable));
		}

		public Task<IEnumerable<object>> Resolve(IEnumerable<object> objects, PropertyLink link)
		{
			return Task.FromResult(
				objects
				.Cast<IEnumerable>()
				.SelectMany(o => o.OfType<object>())
			);
		}
	}
}