using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Query.Provider.InclusionResolver
{
	public class SingleRelationsInclusionResolver : RelationsInclusionResolver
	{
		public SingleRelationsInclusionResolver(IServiceProvider provider) : base(provider) { }

		public override bool CanResolveInclusion(Type type, PropertyLink link)
		{
			return type.IsAssignableTo(typeof(ISingleRelation));
		}

		public override async Task<IEnumerable<object>> Resolve(IEnumerable<object> objects, PropertyLink link)
		{
			List<object> values = new();

			var relsDict = objects
				.Cast<ISingleRelation>()
				.GroupBy(r => r.Type)
				.ToDictionary(g => g.Key, g => g.Select(relation => relation.Id));

			foreach ((var key, var ids) in relsDict)
				values.AddRange(await GetRelationsRecords(key, ids, link));

			return values;
		}
	}
}
