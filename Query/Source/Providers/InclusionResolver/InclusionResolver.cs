using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Query.Provider.InclusionResolver
{
	public interface IInclusionResolver
	{
		bool CanResolveInclusion(Type type, PropertyLink link);
		Task<IEnumerable<object>> Resolve(IEnumerable<object> objects, PropertyLink link);
	}
}