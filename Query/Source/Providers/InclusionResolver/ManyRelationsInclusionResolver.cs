using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Query.Provider.InclusionResolver
{
	public class ManyRelationsInclusionResolver : RelationsInclusionResolver
	{
		public ManyRelationsInclusionResolver(IServiceProvider provider) : base(provider) { }

		public override bool CanResolveInclusion(Type type, PropertyLink link)
		{
			return type.IsAssignableTo(typeof(IManyRelations));
		}

		public override async Task<IEnumerable<object>> Resolve(IEnumerable<object> objects, PropertyLink link)
		{
			List<object> values = new();

			var relations = objects
				.Cast<IManyRelations>()
				.GroupBy(r => r.Type)
				.ToDictionary(g => g.Key, g => g.SelectMany(valRel => valRel.Ids));

			foreach ((var type, var ids) in relations)
				values.AddRange(await GetRelationsRecords(type, ids.Distinct(), link));

			return values;
		}
	}
}
