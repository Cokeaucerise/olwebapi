using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Provider;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Provider.InclusionResolver;

namespace OLWebApi.Query.Provider
{
	public interface IApiRecordProvider
	{
		Task<IEnumerable<IApiRecord>> GetRelationsRecords(IApiRecord record, Includes include);
		Task<IEnumerable<IApiRecord>> GetRelationsRecords(IEnumerable<IApiRecord> records, Includes include);
	}

	public class ApiRecordProvider : IApiRecordProvider
	{
		protected IEnumerable<IInclusionResolver> InclusionResolvers { get; }
		protected IPropertyValueProvider PropertyLinkValueLoader { get; }

		public ApiRecordProvider(IPropertyValueProvider propertyLinkValueLoader, IEnumerable<IInclusionResolver> inclusionResolvers)
		{
			InclusionResolvers = inclusionResolvers.Reverse();
			PropertyLinkValueLoader = propertyLinkValueLoader;
		}

		public Task<IEnumerable<IApiRecord>> GetRelationsRecords(IApiRecord record, Includes includes)
		{
			return GetRelationsRecords(new[] { record }, includes);
		}

		public async Task<IEnumerable<IApiRecord>> GetRelationsRecords(IEnumerable<IApiRecord> records, Includes includes)
		{
			if (!records.Any() || !includes.Any()) return Array.Empty<IApiRecord>();

			List<IApiRecord> allIncludes = new();

			foreach (var chain in includes)
				allIncludes.AddRange(await GetIncludedRelationsForChain(records, chain).ToListAsync());

			return allIncludes.Distinct();
		}


		private IAsyncEnumerable<IApiRecord> GetIncludedRelationsForChain(IEnumerable<IApiRecord> records, PropertyChain chain)
		{
			return GetIncludedRelationsForChain(records.Cast<object>(), chain);
		}

		private async IAsyncEnumerable<IApiRecord> GetIncludedRelationsForChain(IEnumerable<object> records, PropertyChain chain)
		{
			var link = chain.FirstOrDefault();
			if (link == null)
				yield break;

			var values = await GetValuesOfLink(records, link).ToListAsync();
			if (!values.Any())
				yield break;

			if (link.Include || chain.Count <= 1)
				foreach (var value in values.Select(v => v as IApiRecord).Where(v => v != null))
					yield return value;

			await foreach (var innerValue in GetIncludedRelationsForChain(values, new PropertyChain(chain.Skip(1).ToList())))
				yield return innerValue;
		}

		private async IAsyncEnumerable<object> GetValuesOfLink(IEnumerable<object> records, PropertyLink link)
		{
			if (GetIncludedPropertyValues(records, link) is IEnumerable<object> objects)
				await foreach (var value in ResolveInclusions(link, objects))
					yield return value;
		}

		private async IAsyncEnumerable<object> ResolveInclusions(PropertyLink link, IEnumerable<object> objects)
		{
			foreach (var typeGroup in objects.GroupBy(o => o.GetType()))
			{
				var inclusionResolver = InclusionResolvers
					.FirstOrDefault(ir => ir.CanResolveInclusion(typeGroup.Key, link));

				foreach (var value in (await inclusionResolver?.Resolve(objects, link)) ?? Array.Empty<object>())
					yield return value;
			}
		}

		private IEnumerable<object> GetIncludedPropertyValues(IEnumerable<object> records, PropertyLink link)
		{
			return records
				?.Select(r => PropertyLinkValueLoader.GetPropertyValue(link.Name, r))
				?.Where(vr => vr != null)
				?? Array.Empty<object>();
		}
	}
}
