using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Entities.Action;

namespace OLWebApi.Query.Gateway
{
	public interface IQueryApiGateway<T> : IApiGateway<T> where T : IApiRecord
	{
		IAsyncEnumerable<T> Query(QueryParameter parameter);
		Task<Report> Report(ReportParameter parameter);
	}
}