using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Provider;
using OLWebApi.Query.Repository;

namespace OLWebApi.Query.Interactor
{
	public class QueryApiInteractor<T> : ApiInteractor<T>, IQueryApiInteractor<T>
	where T : ApiRecord
	{
		protected new IQueryApiRepository<T> Repository => base.Repository as IQueryApiRepository<T>;

		public IApiRecordProvider Provider { get; private set; }

		public QueryApiInteractor(IQueryApiRepository<T> repository, IApiRecordProvider provider) : base(repository)
		{
			Provider = provider;
		}

		public virtual async Task<Query<T>> Query(QueryParameter parameter, Includes includes)
		{
			var records = await Repository.Query(parameter).ToListAsync();

			return new()
			{
				Data = records,
				Included = await Provider.GetRelationsRecords(records, includes)
			};
		}

		public virtual async Task<Report> Report(ReportParameter parameter)
		{
			return await Repository.Report(parameter);
		}
	}
}