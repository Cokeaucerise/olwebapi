using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Includes;

namespace OLWebApi.Query.Interactor
{
	public interface IQueryApiInteractor<T> : IApiInteractor<T>
	where T : IApiRecord
	{
		Task<Query<T>> Query(QueryParameter parameter, Includes includes);
		Task<Report> Report(ReportParameter parameter);
	}
}
