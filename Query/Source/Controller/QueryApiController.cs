using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Interactor;

namespace OLWebApi.Query.Controller
{
	public class QueryApiController<T> : ApiController<T>, IQueryApiController<T> where T : ApiRecord
	{
		protected new IQueryApiInteractor<T> Interactor => base.Interactor as IQueryApiInteractor<T> ?? throw new InvalidOperationException(typeof(IQueryApiInteractor<T>).Name);

		public QueryApiController(IQueryApiInteractor<T> interactor) : base(interactor) { }


		[HttpGet(nameof(Query))]
		public virtual Task<ActionResult<Query<T>>> Query(
			[FromQuery] IEnumerable<string> ids,
			[FromQuery] Paging paging,
			[FromQuery] PropertyFilters filters,
			[FromQuery] OrderBys orderBys,
			[FromQuery] Includes includes)
		{
			return Query(
				new ControllerQueryParameter
				{
					Ids = ids ?? Array.Empty<string>(),
					Paging = paging ?? new(),
					Filters = filters ?? new(),
					OrderBys = orderBys ?? new(),
					Includes = includes ?? new()
				}
			);
		}

		[HttpPost(nameof(Query))]
		public virtual async Task<ActionResult<Query<T>>> Query([FromBody] ControllerQueryParameter parameter)
		{
			if (!ModelState.IsValid) return BadRequest(ModelState);

			Query<T> result = await Interactor.Query(parameter, parameter.Includes ?? new());
			return Ok(result ?? new Query<T>());
		}

		[HttpGet(nameof(Report))]
		public Task<ActionResult<Report>> Report(
			[FromQuery] IEnumerable<string> ids,
			[FromQuery] Paging paging,
			[FromQuery] PropertyFilters filters,
			[FromQuery] OrderBys orderBys,
			[FromQuery] GroupBies groupBies)
		{
			return Report(
				new ReportParameter
				{
					Ids = ids,
					Paging = paging,
					Filters = filters,
					OrderBys = orderBys,
					GroupBies = groupBies
				}
			);
		}

		[HttpPost(nameof(Report))]
		public virtual async Task<ActionResult<Report>> Report([FromBody] ReportParameter parameter)
		{
			if (!ModelState.IsValid)
				return BadRequest(ModelState);

			return await Interactor.Report(parameter);
		}
	}
}
