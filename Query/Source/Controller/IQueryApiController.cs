using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Query.Controller
{
	public interface IQueryApiController<T> where T : IApiRecord
	{
		Task<ActionResult<Query<T>>> Query(IEnumerable<string> ids, Paging paging, PropertyFilters filters, OrderBys orderBy, Entities.Includes.Includes includes);
		Task<ActionResult<Query<T>>> Query(ControllerQueryParameter parameter);
		Task<ActionResult<Report>> Report(IEnumerable<string> ids, Paging paging, PropertyFilters filters, OrderBys orderBy, GroupBies groupBies);
		Task<ActionResult<Report>> Report(ReportParameter parameter);
	}
}
