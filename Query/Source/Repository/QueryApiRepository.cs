using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Extensions;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Gateway;

namespace OLWebApi.Query.Repository
{
	public class QueryApiRepository<T> : ApiRepository<T>, IQueryApiRepository<T>
	where T : IApiRecord
	{
		protected new IQueryApiGateway<T> Gateway => base.Gateway as IQueryApiGateway<T>;

		public QueryApiRepository(IQueryApiGateway<T> gateway) : base(gateway)
		{ }

		async IAsyncEnumerable<IApiRecord> IQueryApiRepository.Query(QueryParameter parameter)
		{
			await foreach (var record in Query(parameter))
				yield return record;
		}

		public virtual IAsyncEnumerable<T> Query(QueryParameter parameter)
		{
			parameter.Ids = parameter?.Ids.NotNullOrDefault() ?? Array.Empty<string>();
			return Gateway.Query(parameter) ?? AsyncEnumerable.Empty<T>();
		}

		public virtual async Task<Report> Report(ReportParameter parameter)
		{
			return await Gateway.Report(parameter);
		}

	}
}
