using System.Collections.Generic;
using System.Threading.Tasks;
using OLWebApi.Core;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Entities.Action;

namespace OLWebApi.Query.Repository
{
	public interface IQueryApiRepository
	{
		IAsyncEnumerable<IApiRecord> Query(QueryParameter parameter);
	}

	public interface IQueryApiRepository<T> : IApiRepository<T>, IQueryApiRepository
	where T : IApiRecord
	{
		new IAsyncEnumerable<T> Query(QueryParameter parameter);
		Task<Report> Report(ReportParameter parameter);
	}
}
