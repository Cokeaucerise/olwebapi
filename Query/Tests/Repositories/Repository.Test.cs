using System;

namespace OLWebApi.Query.Tests.Repository
{
	public class RepositoryTest
	{
		public TestRecord TestModel;
		public static TestRecord FakeGet => new();

		public RepositoryTest()
		{
			TestModel = new(Guid.NewGuid().ToString());
		}
	}
}
