using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Core.Provider;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Gateway;
using OLWebApi.Query.Repository;
using Xunit;

namespace OLWebApi.Query.Tests.Repository
{
	public class QueryApiRepositoryQueryTest : RepositoryTest
	{
		protected readonly Mock<IQueryApiGateway<TestRecord>> Gateway = new();
		protected QueryApiRepository<TestRecord> Repository => new(Gateway.Object);

		protected TestRecord[] Records = BuildRecords();
		protected IEnumerable<string> Ids => Records.Select(r => r.Id);

		public static TestRecord[] BuildRecords() =>
			new[] {
				new TestRecord { Id = Guid.NewGuid().ToString(), CreatedAt = DateTimeOffset.UtcNow, UpdatedAt = DateTimeOffset.UtcNow },
				new TestRecord { Id = Guid.NewGuid().ToString(), CreatedAt = DateTimeOffset.UtcNow, UpdatedAt = DateTimeOffset.UtcNow },
				new TestRecord { Id = Guid.NewGuid().ToString(), CreatedAt = DateTimeOffset.UtcNow, UpdatedAt = DateTimeOffset.UtcNow }
			};

		[Fact]
		public async Task Query_ManyIds_LoadFromGatewayOnce()
		{
			//Given

			//When
			IEnumerable<TestRecord> result = await Repository.Query(new() { Ids = Ids }).ToListAsync();

			//Then
			Gateway.Verify(
				c => c.Query(It.Is<QueryParameter>(arg => arg.Ids.Count() == Records.Length)),
				Times.Once()
			);
		}
	}
}
