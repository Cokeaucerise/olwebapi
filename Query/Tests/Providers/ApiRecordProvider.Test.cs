using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Core.Provider;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Provider;
using OLWebApi.Query.Provider.InclusionResolver;
using OLWebApi.Query.Repository;
using OLWebApi.Query.Server.Extensions;
using Xunit;

namespace OLWebApi.Query.Tests.Providers
{

	public class TestQueryApiRepository<T> : IQueryApiRepository<T> where T : IApiRecord
	{
		protected IQueryApiRepository<T> Repository;

		public TestQueryApiRepository(IQueryApiRepository<T> repo)
		{
			Repository = repo;
		}

		public async IAsyncEnumerable<IApiRecord> Query(QueryParameter parameter)
		{
			await foreach (var record in Repository.Query(parameter))
				yield return record;
		}

		async IAsyncEnumerable<IApiRecord> IQueryApiRepository.Query(QueryParameter parameter)
		{
			await foreach (var record in Query(parameter))
				yield return record;
		}

		IAsyncEnumerable<T> IQueryApiRepository<T>.Query(QueryParameter parameter)
		{
			throw new NotImplementedException();
		}

		public Task<Report> Report(ReportParameter parameter)
		{
			throw new NotImplementedException();
		}

	}

	public class ApiRecordProviderTest
	{
		public TestParentRecord Parent = new(Guid.NewGuid().ToString());
		public TestRecord Child = new(Guid.NewGuid().ToString());
		public IEnumerable<TestRecordChild> Childs = new TestRecordChild[]
			{
				new(Guid.NewGuid().ToString()),
				new(Guid.NewGuid().ToString()),
				new(Guid.NewGuid().ToString())
			};

		protected Mock<IQueryApiRepository<TestRecord>> ModelRepository = new();
		protected Mock<IQueryApiRepository<TestParentRecord>> ParentRepository = new();
		protected Mock<IQueryApiRepository<TestRecordChild>> ChildRepository = new();
		protected Mock<IQueryApiRepository<TestRecordNested>> NestedRepository = new();

		protected IApiRecordProvider Provider
		{
			get
			{
				var services = new ServiceCollection();
				services.AddTransient<IQueryApiRepository<TestRecord>>(p => new TestQueryApiRepository<TestRecord>(ModelRepository.Object));
				services.AddTransient<IQueryApiRepository<TestParentRecord>>(p => new TestQueryApiRepository<TestParentRecord>(ParentRepository.Object));
				services.AddTransient<IQueryApiRepository<TestRecordChild>>(p => new TestQueryApiRepository<TestRecordChild>(ChildRepository.Object));
				services.AddTransient<IQueryApiRepository<TestRecordNested>>(p => new TestQueryApiRepository<TestRecordNested>(NestedRepository.Object));

				services.AddTransient<IPropertyValueProvider, CaseInsensitivePropertyValueProvider>();

				services.AddInclusionResolvers();

				return new ApiRecordProvider(
					services.BuildServiceProvider().GetService<IPropertyValueProvider>(),
					services.BuildServiceProvider().GetServices<IInclusionResolver>()
				);
			}
		}

		public ApiRecordProviderTest()
		{
			IEnumerable<string> childsIds = Childs.Select(c => c.Id);

			ModelRepository
				.Setup(r => r.Query(It.Is<QueryParameter>(arg => arg.Ids.SequenceEqual(new[] { Child.Id }))))
				.Returns(new[] { Child }.ToAsyncEnumerable());

			ChildRepository
				.Setup(r => r.Query(It.Is<QueryParameter>(arg => arg.Ids.SequenceEqual(childsIds))))
				.Returns(Childs.ToAsyncEnumerable());

			ParentRepository
				.Setup(r => r.Query(It.Is<QueryParameter>(arg => arg.Ids.SequenceEqual(new[] { Parent.Id }))))
				.Returns(new[] { Parent }.ToAsyncEnumerable());
		}

		[Fact]
		public async Task GetRelationsRecords_SingleRelationNullIncludeEmpty_ReturnEmpty()
		{
			//Given
			Parent.Child = null;

			//When
			var result = await Provider.GetRelationsRecords(Parent, new Includes());

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_ManyRelationsNullIncludeEmpty_ReturnEmpty()
		{
			//Given
			Parent.Childs = null;

			//When
			var result = await Provider.GetRelationsRecords(Parent, new Includes());

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_BothRelationsNullIncludeEmpty_ReturnEmpty()
		{
			//Given
			Parent.Child = null;
			Parent.Childs = null;

			//When
			var result = await Provider.GetRelationsRecords(Parent, new Includes());

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_BothRelationsEmptyIncludeSingleRelation_ReturnEmpty()
		{
			//Given
			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) } };

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.NotNull(result);
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_BothRelationsEmptyIncludeManyRelations_ReturnEmpty()
		{
			//Given
			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) } };

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_BothRelationsEmptyIncludeBothRelations_ReturnEmpty()
		{
			//Given
			Includes include = new()
			{
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) },
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) }
			};

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_SingleRelationIncludeSingleRelation_ReturnSingleRelation()
		{
			//Given
			Parent.Child = new SingleRelation<TestRecord>(Child);
			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) } };

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Equal(Child.Id, result.First().Id);
		}

		[Fact]
		public async Task GetRelationsRecords_SingleRelationIncludeSingleRelation_CallGetManyOnce()
		{
			//Given
			Parent.Child = new SingleRelation<TestRecord>(Child.Id);
			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) } };

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			ModelRepository.Verify(r => r.Query(It.IsAny<QueryParameter>()), Times.Exactly(1));
		}

		[Fact]
		public async Task GetRelationsRecords_ManyRelationsNoInclude_ReturnEmpty()
		{
			//Given
			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);
			Includes include = new();

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_ManyRelationsIncludeManyRelations_ReturnManyRelation()
		{
			//Given
			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);
			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) } };

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Equal(Childs.Count(), result.Count());

			foreach (TestRecordChild c in Childs)
				Assert.NotNull(result.FirstOrDefault(x => x.Id.Equals(c.Id, StringComparison.Ordinal)));
		}

		[Fact]
		public async Task GetRelationsRecords_BothRelationsIncludeSingleRelation_ReturnSingleRelation()
		{
			//Given
			Parent.Child = new SingleRelation<TestRecord>(Child);
			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);
			Includes include = new()
			{
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) },
			};

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Single(result);
			Assert.Equal(Child.Id, result.First().Id);
		}

		[Fact]
		public async Task GetRelationsRecords_BothRelationsIncludeManyRelations_ReturnManyRelations()
		{
			//Given
			Parent.Child = new SingleRelation<TestRecord>(Child);
			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);
			Includes include = new()
			{
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) }
			};


			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Equal(Childs.Count(), result.Count());

			foreach (TestRecordChild c in Childs)
				Assert.NotNull(result.FirstOrDefault(x => x.Id.Equals(c.Id, StringComparison.Ordinal)));
		}

		[Fact]
		public async Task GetRelationsRecords_BothRelationsIncludeBothRelations_ReturnBothRelations()
		{
			//Given
			Parent.Child = new SingleRelation<TestRecord>(Child);
			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);
			Includes include = new()
			{
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) },
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) }
			};


			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Equal(Childs.Count() + 1, result.Count());
			Assert.NotNull(result.FirstOrDefault(x => x.Id.Equals(Child.Id, StringComparison.Ordinal)));

			foreach (TestRecordChild c in Childs)
				Assert.NotNull(result.FirstOrDefault(x => x.Id.Equals(c.Id, StringComparison.Ordinal)));
		}

		[Fact]
		public async Task GetRelationsRecords_RecursiveRelationsIncludeChild_ReturnChild()
		{
			//Given
			TestRecordChild child = new()
			{
				Id = Guid.NewGuid().ToString(),
				Parent = new SingleRelation<TestParentRecord>(Parent)
			};
			Childs = new[] { child };
			var ids = Childs.Select(c => c.Id);

			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);

			ChildRepository
				.Setup(r => r.Query(It.Is<QueryParameter>(arg => arg.Ids.SequenceEqual(ids))))
				.Returns(Childs.ToAsyncEnumerable());

			Includes include = new()
			{
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) }
			};


			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Single(result);
			Assert.Equal(child.Id, result.First().Id);
		}

		[Fact]
		public async Task GetRelationsRecords_RecursiveRelationsIncludeChildParent_ReturnParent()
		{
			//Given
			TestRecordChild child = new()
			{
				Id = Guid.NewGuid().ToString(),
				Parent = new SingleRelation<TestParentRecord>(Parent)
			};
			Childs = new[] { child };
			var ids = Childs.Select(c => c.Id);

			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);

			ChildRepository
				.Setup(r => r.Query(It.Is<QueryParameter>(arg => arg.Ids.SequenceEqual(ids))))
				.Returns(Childs.ToAsyncEnumerable());

			Includes include = new()
			{
				new PropertyChain {
					 new PropertyLink(nameof(TestParentRecord.Childs)),
					 new PropertyLink(nameof(TestRecordChild.Parent))
				}
			};


			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Single(result);
			Assert.Equal(Parent.Id, result.First().Id);
		}

		[Fact]
		public async Task GetRelationsRecords_RecursiveRelationsIncludeChildAndChildParent_ReturnChildAndParent()
		{
			//Given
			TestRecordChild child = new()
			{
				Id = Guid.NewGuid().ToString(),
				Parent = new SingleRelation<TestParentRecord>(Parent)
			};
			Childs = new[] { child };
			var ids = Childs.Select(c => c.Id);

			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);

			ChildRepository
				.Setup(r => r.Query(It.Is<QueryParameter>(arg => arg.Ids.SequenceEqual(ids))))
				.Returns(Childs.ToAsyncEnumerable());

			Includes include = new()
			{
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) },
				new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)), new PropertyLink(nameof(TestRecordChild.Parent)) }
			};

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Equal(Childs.Count() + 1, result.Count());

			Assert.NotNull(result.FirstOrDefault(i => i.Id.Equals(child.Id, StringComparison.Ordinal)));
			Assert.NotNull(result.FirstOrDefault(i => i.Id.Equals(Parent.Id, StringComparison.Ordinal)));
		}

		[Fact]
		public async Task GetRelationsRecords_RecursiveRelationsIncludeFullCircleRelation_ReturnChild()
		{
			//Given
			TestRecordChild child = new()
			{
				Id = Guid.NewGuid().ToString(),
				Parent = new SingleRelation<TestParentRecord>(Parent)
			};
			Childs = new[] { child };
			var ids = Childs.Select(c => c.Id);

			Parent.Childs = new ManyRelations<TestRecordChild>(Childs);

			ChildRepository
				.Setup(r => r.Query(It.Is<QueryParameter>(arg => arg.Ids.SequenceEqual(ids))))
				.Returns(Childs.ToAsyncEnumerable());

			Includes include = new()
			{
				new PropertyChain {
					new PropertyLink(nameof(TestParentRecord.Childs)),
					new PropertyLink(nameof(TestRecordChild.Parent)),
					new PropertyLink(nameof(TestParentRecord.Childs))
				}
			};

			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.Single(result);
			Assert.Equal(child.Id, result.FirstOrDefault().Id);
		}

		[Fact]
		public async Task QueryRequestWithIncluded_RecordsEmpty_ReturnEmpty()
		{
			//Given
			TestRecord[] records = Array.Empty<TestRecord>();

			//When
			var result = await Provider.GetRelationsRecords(records, new());

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task QueryRequestWithIncluded_IncludeEmpty_ReturnEmpty()
		{
			//Given
			TestParentRecord[] records = new[] { Parent };

			//When
			var result = await Provider.GetRelationsRecords(records, new());

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task QueryRequestWithIncluded_OneRecordSingleRelation_ReturnSingleRelation()
		{
			//Given
			Parent.Child = new SingleRelation<TestRecord>(Child);
			TestParentRecord[] records = new[] { Parent };

			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) } };


			//When
			var result = await Provider.GetRelationsRecords(records, include);

			//Then
			Assert.Single(result);
		}

		[Fact]
		public async Task QueryRequestWithIncluded_ManyRecordManySingleRelation_ReturnManySingleRelation()
		{
			//Given
			TestRecord c1 = new(Guid.NewGuid().ToString());
			TestRecord c2 = new(Guid.NewGuid().ToString());
			TestRecord c3 = new(Guid.NewGuid().ToString());

			TestParentRecord[] records = new[] {
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Child = new SingleRelation<TestRecord>(c1) },
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Child = new SingleRelation<TestRecord>(c2) },
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Child = new SingleRelation<TestRecord>(c3) }
			};

			ModelRepository
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(() => (new[] { c1, c2, c3 } as IEnumerable<TestRecord>).ToAsyncEnumerable());

			ParentRepository
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(records.ToAsyncEnumerable());

			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) } };

			//When
			var result = await Provider.GetRelationsRecords(records, include);

			//Then
			Assert.Equal(records.Length, result.Count());
		}

		[Fact]
		public async Task QueryRequestWithIncluded_ManyRecordManySameSingleRelation_ReturnSingleRelation()
		{
			//Given
			TestRecord c = new(Guid.NewGuid().ToString());

			TestParentRecord[] records = new[] {
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Child = new SingleRelation<TestRecord>(c) },
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Child = new SingleRelation<TestRecord>(c) },
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Child = new SingleRelation<TestRecord>(c) }
			};

			ModelRepository
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(() => (new[] { c } as IEnumerable<TestRecord>).ToAsyncEnumerable());

			ParentRepository
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(records.ToAsyncEnumerable());

			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) } };


			//When
			var result = await Provider.GetRelationsRecords(records, include);

			//Then
			Assert.Single(result);
		}

		[Fact]
		public async Task QueryRequestWithIncluded_ManyRecordManyManyRelation_ReturnManyManyRelation()
		{
			//Given
			List<TestRecordChild> c = new();
			for (int i = 0; i < 9; i++)
			{
				c.Add(new(Guid.NewGuid().ToString()));
			}

			TestRecordChild[] childs1 = new[] { c[0], c[1], c[2] };
			TestRecordChild[] childs2 = new[] { c[3], c[4], c[5] };
			TestRecordChild[] childs3 = new[] { c[6], c[7], c[8] };

			ChildRepository
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(childs1.Concat(childs2).Concat(childs3).ToAsyncEnumerable());

			TestParentRecord[] records = new[] {
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Childs = new ManyRelations<TestRecordChild>(childs1) },
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Childs = new ManyRelations<TestRecordChild>(childs2) },
				new TestParentRecord { Id = Guid.NewGuid().ToString(), Childs = new ManyRelations<TestRecordChild>(childs3) }
			};
			ParentRepository
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(records.ToAsyncEnumerable());

			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) } };

			//When
			var result = await Provider.GetRelationsRecords(records, include);

			//Then
			Assert.Equal(c.Count, result.Count());
		}

		[Fact]
		public async Task GetRelationsRecords_SingleUnloadedRelationIncludeSingleRelation_ReturnSingleRelation()
		{
			//Given
			Parent.Child = new SingleRelation<TestRecord>(Child.Id);
			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Child)) } };


			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			Assert.NotNull(result);
			Assert.Single(result);
			Assert.Equal(Child.Id, result.First().Id);
			Assert.Equal(Child.CreatedAt, result.First().CreatedAt);
			Assert.Equal(Child.UpdatedAt, result.First().UpdatedAt);
		}

		[Fact]
		public async Task GetRelationsRecords_ManyUnloadedRelationsIncludeManyRelation_ReturnManyRelation()
		{
			//Given
			Parent.Childs = new ManyRelations<TestRecordChild>(Childs.Select(c => c.Id));
			Includes include = new() { new PropertyChain { new PropertyLink(nameof(TestParentRecord.Childs)) } };

			ChildRepository
				.Setup(r => r.Query(It.IsAny<QueryParameter>()))
				.Returns(Childs.ToAsyncEnumerable());


			//When
			var result = await Provider.GetRelationsRecords(Parent, include);

			//Then
			ChildRepository.Verify(r => r.Query(It.IsAny<QueryParameter>()), Times.Once());

			for (int i = 0; i < Childs.Count(); i++)
			{
				Assert.Equal(Childs.ElementAt(i).Id, result.ElementAt(i).Id);
				Assert.Equal(Childs.ElementAt(i).CreatedAt, result.ElementAt(i).CreatedAt);
				Assert.Equal(Childs.ElementAt(i).UpdatedAt, result.ElementAt(i).UpdatedAt);
			}
		}

		[Fact]
		public async Task GetRelationsRecords_TargetNotIApiRecord_ReturnEmpty()
		{
			//Given
			TestRecordNested nested = new("1");
			nested.NestedObject = new { Id = "2" };

			Includes include = new() { { nameof(TestRecordNested.NestedObject) } };

			//When
			var result = await Provider.GetRelationsRecords(nested, include);

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_TargetNestedIApiRecord_ReturnEmpty()
		{
			//Given
			TestRecordNested nested = new("1");
			nested.NestedApiRecord = new TestRecordChild("2") { Parent = new(Parent.Id) };

			Includes include = new() { { nameof(TestRecordNested.NestedApiRecord) } };

			//When
			var result = await Provider.GetRelationsRecords(nested, include);

			//Then
			Assert.NotEmpty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_RecordWithNestedObject_ReturnNestedObjectRelation()
		{
			//Given
			TestRecordNested nested = new("1");
			nested.NestedObject = new { Parent = new SingleRelation<TestParentRecord>(Parent.Id) };
			Includes include = new() { { nameof(TestRecordNested.NestedObject), nameof(Parent) } };

			//When
			var result = await Provider.GetRelationsRecords(nested, include);

			//Then
			ParentRepository.Verify(r => r.Query(It.IsAny<QueryParameter>()), Times.Once());

			Assert.Equal(Parent, result.First());
		}

		[Fact]
		public async Task GetRelationsRecords_RecordWithFilteredNestedObject_ReturnFilteredNestedObjectRelation()
		{
			//Given
			TestRecordNested nested = new("1");
			nested.NestedObject = new { Value = 0, Parent = new SingleRelation<TestParentRecord>(Parent.Id) };

			Includes include = new()
			{
				new PropertyChain {
					new(
						nameof(TestRecordNested.NestedApiRecord),
						new PropertyFilters { new PropertyFilter<FilterGreaterOperator>("Value", "0") }
					),
					new(nameof(Parent))
				}
			};

			//When
			var result = await Provider.GetRelationsRecords(nested, include);

			//Then
			ParentRepository.Verify(r => r.Query(It.IsAny<QueryParameter>()), Times.Never());

			Assert.Empty(result);
		}

		[Fact]
		public async Task GetRelationsRecords_RecordWithNestedEnumerable_ReturnFilteredNestedObjectRelation()
		{
			//Given
			TestRecordNested nested = new("1");
			nested.NestedArray = new object[] { new { Parent = new SingleRelation<TestParentRecord>(Parent.Id) } };
			Includes include = new() { { nameof(TestRecordNested.NestedArray), nameof(Parent) } };

			//When
			var result = await Provider.GetRelationsRecords(nested, include);

			//Then
			ParentRepository.Verify(r => r.Query(It.IsAny<QueryParameter>()), Times.Once());

			Assert.Equal(Parent, result.First());
		}
	}
}
