using OLWebApi.Query.Entities.Action;
using Xunit;

namespace OLWebApi.Query.Tests.Entities.Action
{
	public class ReportEntryTest
	{
		[Fact]
		public void Constructor_WithGroupKeyAndGroupValue_KeysAndValuesAreSet()
		{
			ReportGroupKey keys = new() { { "test", "key" } };
			ReportGroupValue values = new() { { "test", "values" } };

			ReportEntry entry = new(keys, values);

			Assert.Equal(keys, entry.Keys);
			Assert.Equal(values, entry.Values);
		}
	}
}