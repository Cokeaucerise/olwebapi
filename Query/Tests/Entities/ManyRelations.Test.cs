using System;
using System.Collections.Generic;
using System.Linq;
using OLWebApi.Core.Entities.Model;
using Xunit;

namespace OLWebApi.Query.Tests.Model
{

	public class ManyRelationsTest
	{
		[Fact]
		public void Contructor_IdsNull_ArgumentNullException()
		{
			//Given

			//When
			static ManyRelations<TestRecord> ctr() => new(null as IEnumerable<string>);

			//Then
			Assert.Throws<ArgumentNullException>("ids", ctr);
		}

		[Fact]
		public void Contructor_Ids_IdsNotNull()
		{
			//Given
			string guid = Guid.NewGuid().ToString();

			//When
			ManyRelations<TestRecord> rels = new(new[] { guid });

			//Then

			Assert.NotNull(rels.Ids);
			Assert.Equal(new[] { guid }, rels.Ids);

			Assert.Equal(typeof(TestRecord).Name, rels.Type.Name);
		}

		[Fact]
		public void Contructor_Dtos_IdsNotNullModelsNotNull()
		{
			//Given
			TestRecord record = new();
			TestRecord[] records = new[] { record };

			//When
			ManyRelations<TestRecord> rels = new(records);

			//Then

			Assert.NotNull(rels.Ids);
			Assert.Equal(records.Select(d => d.Id), rels.Ids);

			Assert.Equal(typeof(TestRecord).Name, rels.Type.Name);
		}

		[Fact]
		public void Contructor_DtosNull_ArgumentNullException()
		{
			//Given

			//When
			static ManyRelations<TestRecord> ctor() => new(null as IEnumerable<TestRecord>);

			//Then
			Assert.Throws<ArgumentNullException>("records", ctor);
		}
	}
}
