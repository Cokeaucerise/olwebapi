using System;
using System.Collections.Generic;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Action;
using Xunit;

namespace OLWebApi.Query.Tests.Entities
{
	public class QueryTest
	{

		public QueryTest()
		{

		}

		[Fact]
		public void Constructor_EmptyCtr_PropsAreEmptyArray()
		{
			//Given

			//When
			Query<TestRecord> q = new();

			//Then
			Assert.NotNull(q.Data);
			Assert.NotNull(q.Included);

			Assert.Empty(q.Data);
			Assert.Empty(q.Included);
		}

		[Fact]
		public void Constructor_DataNull_PropsAreEmptyArray()
		{
			//Given

			//When
			Query<TestRecord> q = new(null);

			//Then
			Assert.NotNull(q.Data);
			Assert.NotNull(q.Included);

			Assert.Empty(q.Data);
			Assert.Empty(q.Included);
		}

		[Fact]
		public void Constructor_WithData_QueryDataIsSame()
		{
			//Given
			List<TestRecord> data = new() { new(Guid.NewGuid().ToString()) };

			//When
			Query<TestRecord> q = new(data);

			//Then
			Assert.NotNull(q.Data);
			Assert.NotNull(q.Included);

			Assert.NotEmpty(q.Data);
			Assert.Empty(q.Included);
		}

		[Fact]
		public void Constructor_DataNullIncludedNull_QueryDataEmptyIncludedNotNull()
		{
			//Given

			//When
			Query<TestRecord> q = new(null, null);

			//Then
			Assert.NotNull(q.Data);
			Assert.NotNull(q.Included);

			Assert.Empty(q.Data);
			Assert.Empty(q.Included);
		}

		[Fact]
		public void Constructor_WithDataWithIncluded_QueryPropsAreSame()
		{
			//Given
			List<TestRecord> data = new() { new(Guid.NewGuid().ToString()) };
			List<ApiRecord> inc = new() { new TestRecord(Guid.NewGuid().ToString()) };

			//When
			Query<TestRecord> q = new(data, inc);

			//Then
			Assert.NotNull(q.Data);
			Assert.NotNull(q.Included);

			Assert.Single(q.Data);
			Assert.Single(q.Included);
		}
	}
}