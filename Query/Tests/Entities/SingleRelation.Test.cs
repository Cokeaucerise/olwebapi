using System;
using OLWebApi.Core.Entities.Model;
using Xunit;

namespace OLWebApi.Query.Tests.Model
{

	public class SingleRelationTest
	{
		[Fact]
		public void Contructor_IdParam_IdNotNull()
		{
			//Given
			string guid = Guid.NewGuid().ToString();

			//When
			SingleRelation<TestRecord> rels = new(guid);

			//Then
			Assert.NotNull(rels.Id);
			Assert.Equal(guid, rels.Id);

			Assert.Equal(typeof(TestRecord).Name, rels.Type.Name);
		}

		[Fact]
		public void Contructor_DtoParam_IdNotNullModelNotNull()
		{
			//Given
			TestRecord record = new(Guid.NewGuid().ToString());

			//When
			SingleRelation<TestRecord> rels = new(record);

			//Then
			Assert.NotNull(rels.Id);
			Assert.Equal(record.Id, rels.Id);

			Assert.Equal(typeof(TestRecord).Name, rels.Type.Name);
		}

		[Fact]
		public void Contructor_IdNull_ArgumentNullException()
		{
			//Given

			//When
			static SingleRelation<TestRecord> func() => new(null as string);

			//Then
			Assert.Throws<ArgumentNullException>("id", func);
		}

		[Fact]
		public void Contructor_DtoNull_ArgumentNullException()
		{
			//Given

			//When
			static SingleRelation<TestRecord> ctor() => new(null as TestRecord);

			//Then
			Assert.Throws<ArgumentNullException>("record", ctor);
		}
	}
}
