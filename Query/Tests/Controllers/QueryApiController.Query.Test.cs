using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Includes;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Query.Tests.Controllers
{
	public class QueryApiControllerReadTest : QueryApiControllerTest
	{

		[Fact]
		public async Task Query_InteractorReturnNull_EmptyQuery()
		{
			//Given
			Interactor
				.Setup(s => s.Query(It.IsAny<QueryParameter>(), It.IsAny<Includes>()))
				.Returns(Task.FromResult(null as Query<TestRecord>));

			//When
			ActionResult<Query<TestRecord>> response = await QueryApiController.Query(new() { Ids = new[] { TestModel.Id } });

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Query<TestRecord> query = ok.Value as Query<TestRecord>;
			Assert.NotNull(query);

			Assert.NotNull(query.Data);
			Assert.Empty(query.Data);

			Assert.NotNull(query.Included);
			Assert.Empty(query.Included);
		}

		[Fact]
		public async Task Query_InteractorReturnDataNull_EmptyQueryData()
		{
			//Given
			Interactor
				.Setup(s => s.Query(It.IsAny<QueryParameter>(), It.IsAny<Includes>()))
				.Returns(Task.FromResult(null as Query<TestRecord>));

			//When
			ActionResult<Query<TestRecord>> response = await QueryApiController.Query(new() { Ids = new[] { TestModel.Id } });

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Query<TestRecord> query = ok.Value as Query<TestRecord>;
			Assert.NotNull(query);
			Assert.NotNull(query.Data);
			Assert.Empty(query.Data);
		}

		[Fact]
		public async Task Query_InteractorReturnValidModel_QueryWithDataAndEmptyInclude()
		{
			//Given
			Interactor
				.Setup(s => s.Query(It.IsAny<QueryParameter>(), It.IsAny<Includes>()))
				.Returns(Task.FromResult(FakeQuery));

			//When
			ActionResult<Query<TestRecord>> response = await QueryApiController.Query(new() { Ids = new[] { TestModel.Id } });

			//Then
			Assert.NotNull(response);

			OkObjectResult ok = response.Result as OkObjectResult;
			Assert.NotNull(ok);

			Query<TestRecord> get = ok.Value as Query<TestRecord>;
			Assert.NotNull(get);
			Assert.Equal(TestModel.Id, get.Data.FirstOrDefault()?.Id);
			Assert.NotNull(get.Included);
			Assert.Empty(get.Included);
		}
	}
}