using System;
using Moq;
using OLWebApi.Query.Controller;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Interactor;

namespace OLWebApi.Query.Tests.Controllers
{
	public class QueryApiControllerTest
	{
		public TestRecord TestModel;
		public Query<TestRecord> FakeQuery => new(new[] { TestModel });

		public readonly Mock<IQueryApiInteractor<TestRecord>> Interactor;

		public IQueryApiController<TestRecord> QueryApiController => new QueryApiController<TestRecord>(Interactor.Object);

		public QueryApiControllerTest()
		{
			TestModel = new();
			TestModel = TestModel with { Id = Guid.NewGuid().ToString() };
			Interactor = new Mock<IQueryApiInteractor<TestRecord>>();
		}
	}
}