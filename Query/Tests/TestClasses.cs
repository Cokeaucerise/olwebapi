using System.Collections.Generic;
using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Query.Tests
{
	public record TestRecord : ApiRecord
	{
		public TestRecord() { }

		public TestRecord(string id) : base(id) { }

		public int Value { get; set; }
		public ManyRelations<TestRecordChild> Childs { get; set; }
	}

	public record TestRecordChild : ApiRecord
	{
		public TestRecordChild() { }
		public TestRecordChild(string id) : base(id) { }
		public int Value { get; set; }
		public SingleRelation<TestParentRecord> Parent { get; set; }
	}

	public record TestParentRecord : ApiRecord
	{
		public TestParentRecord() { }

		public TestParentRecord(string id) : base(id) { }

		public SingleRelation<TestRecord> Child { get; set; }
		public ManyRelations<TestRecordChild> Childs { get; set; }
	}

	public record TestRecordNested : ApiRecord
	{
		public TestRecordNested() { }

		public TestRecordNested(string id) : base(id) { }

		public TestRecordChild NestedApiRecord { get; set; }
		public object NestedObject { get; set; }
		public IEnumerable<object> NestedArray { get; set; }
	}

}