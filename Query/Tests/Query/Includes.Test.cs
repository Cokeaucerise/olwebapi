using System;
using System.Linq;
using OLWebApi.Query.Entities.Includes;
using Xunit;

namespace OLWebApi.Query.Tests.Query
{
	public class IncludesTest
	{
		public string TestTargetName { get; set; } = "TestProperty";

		[Fact]
		public void Add_StrignArray_NewPropertyChain()
		{
			//Given
			Includes include = new();
			string[] chain = { "p1", "p2" };

			//When
			include.Add(chain);

			//Then
			Assert.Equal("p1", include.First().First().Name);
			Assert.Equal("p2", include.First().Last().Name);
		}

		[Fact]
		public void Add_NullStrignArray_ThrowsArgumentNullException()
		{
			//Given
			Includes include = new();
			string[] chain = null;

			//When
			void func() => include.Add(chain);

			//Then
			Assert.Throws<ArgumentNullException>(func);
		}

		[Fact]
		public void Operator_PropertyChainArray_NewIncludes()
		{
			//Given
			PropertyChain[] chain = new PropertyChain[] {
				new () { "p1" },
				new () { "p1", "p2" }
			};

			//When
			Includes includes = chain;

			//Then
			Assert.Equal("p1", includes.First().First().Name);
			Assert.Equal("p1", includes.Last().First().Name);
			Assert.Equal("p2", includes.Last().Last().Name);
		}

		[Fact]
		public void Operator_Null_Null()
		{
			//Given
			PropertyChain[] chain = null;

			//When
			Includes includes = chain;

			//Then
			Assert.Null(includes);
		}
	}
}