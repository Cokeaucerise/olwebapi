using System;
using System.Collections.Generic;
using System.Linq;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Query.Tests.Query
{
	public class OrderBysTest
	{
		protected IEnumerable<TestRecord> Records = new List<TestRecord>();
		protected OrderBys OrderBys = new();

		[Fact]
		public void Order_EmptyOrderAndBysEmptyRecords_ReturnEmpty()
		{
			//Given

			//When
			var result = OrderBys.Order(Records);

			//Then
			Assert.Empty(result);
		}

		[Fact]
		public void Order_EmptyOrderAndUnorganizedRecords_ReturnUnorganizedRecords()
		{
			//Given
			Records = new TestRecord[] {
				new(Guid.NewGuid().ToString()),
				new(Guid.NewGuid().ToString()),
				new(Guid.NewGuid().ToString()),
			};

			//When
			var result = OrderBys.Order(Records);

			//Then
			Assert.Equal(Records.ElementAt(0), result.ElementAt(0));
			Assert.Equal(Records.ElementAt(1), result.ElementAt(1));
			Assert.Equal(Records.ElementAt(2), result.ElementAt(2));
		}

		[Fact]
		public void Order_SingleOrderBy_ReturnOrganizedRecords()
		{
			//Given
			Records = new TestRecord[] {
				new("2") {CreatedAt = new DateTimeOffset(2, new TimeSpan(0))},
				new("0") {CreatedAt = new DateTimeOffset(0, new TimeSpan(0))},
				new("1") {CreatedAt = new DateTimeOffset(1, new TimeSpan(0))},
			};

			OrderBys = new(new OrderBy[] { new() { Target = nameof(TestRecord.CreatedAt) } });

			//When
			var result = OrderBys.Order(Records);

			//Then
			Assert.Equal("0", result.ElementAt(0).Id);
			Assert.Equal("1", result.ElementAt(1).Id);
			Assert.Equal("2", result.ElementAt(2).Id);
		}

		[Fact]
		public void Order_SingleOrderByDescending_ReturnOrganizedRecords()
		{
			//Given
			Records = new TestRecord[] {
				new("2"),
				new("0"),
				new("1"),
			};

			OrderBys = new(new OrderBy[] { new() { Target = nameof(TestRecord.Id), Ascending = false } });

			//When
			var result = OrderBys.Order(Records);

			//Then
			Assert.Equal("2", result.ElementAt(0).Id);
			Assert.Equal("1", result.ElementAt(1).Id);
			Assert.Equal("0", result.ElementAt(2).Id);
		}

		[Fact]
		public void Order_ManyOrderBy_ReturnOrganizedRecords()
		{
			//Given
			Records = new TestRecord[] {
				new("0") { Value = 2 },
				new("1") { Value = 1 },
				new("2") { Value = 2 },
				new("3") { Value = 1 },
			};

			OrderBys = new(new OrderBy[] {
				new() { Target = nameof(TestRecord.Value), Ascending = true },
				new() { Target = nameof(TestRecord.Id), Ascending = false },
			});

			//When
			var result = OrderBys.Order(Records);

			//Then
			Assert.Equal("3", result.ElementAt(0).Id);
			Assert.Equal("1", result.ElementAt(1).Id);
			Assert.Equal("2", result.ElementAt(2).Id);
			Assert.Equal("0", result.ElementAt(3).Id);
		}

		[Fact]
		public void Order_OrderByTargetInvalidProperty_ReturnUnorganizedRecords()
		{
			//Given
			Records = new TestRecord[] {
				new("2"),
				new("0"),
				new("1"),
			};

			OrderBys = new(new OrderBy[] { new() { Target = "Invalid" } });

			//When
			var result = OrderBys.Order(Records);

			//Then
			Assert.Equal("2", result.ElementAt(0).Id);
			Assert.Equal("0", result.ElementAt(1).Id);
			Assert.Equal("1", result.ElementAt(2).Id);
		}
	}
}