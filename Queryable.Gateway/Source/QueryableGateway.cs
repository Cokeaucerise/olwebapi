﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway.Queryable.Extensions;
using OLWebApi.Query.Entities.Action;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Query.Gateway;
using OLWebApi.Queryable.Gateway.Accumulators;

namespace OLWebApi.Crud.Gateway.Queryable
{
	public class QueryableGatewayServices<T> where T : IApiRecord
	{
		public IQueryFilterBuilder<T> FilterBuilder { get; set; }
		public IQueryOrderByBuilder<T> OrderByBuilder { get; set; }
		public IEnumerable<IQueryablePropertyAccumulator> Accumulators { get; set; }

		public QueryableGatewayServices(
			IQueryFilterBuilder<T> filterBuilder,
			IQueryOrderByBuilder<T> orderByBuilder,
			IEnumerable<IQueryablePropertyAccumulator> accumulators)
		{
			FilterBuilder = filterBuilder;
			OrderByBuilder = orderByBuilder;
			Accumulators = accumulators;
		}

	}

	public class QueryableGateway<T> : IQueryApiGateway<T> where T : IApiRecord
	{
		protected virtual Func<IQueryable<T>> Source { get; }
		protected IQueryFilterBuilder<T> FilterBuilder { get; }
		protected IQueryOrderByBuilder<T> OrderByBuilder { get; }
		protected IEnumerable<IQueryablePropertyAccumulator> Accumulators { get; }

		public QueryableGateway(
			IQueryable<T> source,
			QueryableGatewayServices<T> services
		) : this(() => source, services)
		{ }

		public QueryableGateway(
			Func<IQueryable<T>> source,
			QueryableGatewayServices<T> services)
		{
			Source = source;
			FilterBuilder = services.FilterBuilder;
			OrderByBuilder = services.OrderByBuilder;
			Accumulators = services.Accumulators.Reverse();
		}

		public virtual IAsyncEnumerable<T> Query(QueryParameter parameter)
		{
			return Query(Source(), parameter);
		}

		protected virtual IAsyncEnumerable<T> Query(IQueryable<T> queryable, QueryParameter parameter)
		{
			return queryable
				.Where(FilterBuilder.Build(parameter))
				.OrderBy(OrderByBuilder, parameter.OrderBys)
				.Page(parameter?.Paging)
				.ToAsyncEnumerable();
		}

		public virtual async Task<Report> Report(ReportParameter parameter)
		{
			var records = await Query(parameter).ToListAsync();

			var groups = parameter.GroupBies.Keys;
			var entries = records.GroupBy(
				r => new ReportGroupKey(groups.ToDictionary(
					k => k,
					k => r.GetType().GetProperty(k).GetValue(r)
				)),
				(key, values) => new ReportEntry(
					key,
					new ReportGroupValue(parameter.GroupBies.Groups.ToDictionary(
						g => g.Name,
						g => AccumulateValues(values, g)
					))
				)
			);

			return new Report(entries.ToList());
		}

		private object AccumulateValues(IEnumerable<T> values, GroupBy gb)
		{
			return Accumulators.FirstOrDefault(a => a.CanAccumulate(gb.Accumulator))?.Accumulate(values, gb);
		}
	}
}
