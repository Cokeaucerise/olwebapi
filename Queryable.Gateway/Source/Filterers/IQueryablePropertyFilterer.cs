using System.Linq.Expressions;
using OLWebApi.Core.Entities.Filters;

namespace OLWebApi.Crud.Gateway.Queryable.Filterers
{
	public interface IQueryablePropertyFilterer
	{
		bool CanFilter(PropertyFilter filter);
		BinaryExpression Filter<T>(PropertyFilter filter, ParameterExpression parameter);
	}
}