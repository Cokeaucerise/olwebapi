using System;
using System.Linq.Expressions;
using OLWebApi.Core.Entities.Filters;

namespace OLWebApi.Crud.Gateway.Queryable.Filterers
{
	public abstract class QueryablePropertyFilterer<TFilterOperator> : IQueryablePropertyFilterer
	where TFilterOperator : IFilterOperator
	{
		public bool CanFilter(PropertyFilter filter) => filter.Operator.GetType() == typeof(TFilterOperator);

		public BinaryExpression Filter<T>(PropertyFilter filter, ParameterExpression sourceParameter)
		{
			var property = SourceValueExpression(filter, sourceParameter);
			var value = FilterValueExpression(filter, property.Type);

			return Filter(property, value);
		}

		private static Expression SourceValueExpression(PropertyFilter filter, ParameterExpression parameter)
		{
			return Expression.Property(parameter, filter.Target);
		}

		private static Expression FilterValueExpression(PropertyFilter filter, Type type)
		{
			return Expression.Constant(Convert.ChangeType(filter.Value, type));
		}

		protected abstract BinaryExpression Filter(Expression property, Expression value);

		protected static Expression CompareTo(Expression property, Expression value)
		{
			var compareToMethod = typeof(IComparable).GetMethod(nameof(IComparable.CompareTo));
			return Expression.Call(property, compareToMethod, Expression.Convert(value, typeof(object)));
		}
	}

	public class EqualFilterer : QueryablePropertyFilterer<FilterEqualOperator>
	{
		protected override BinaryExpression Filter(Expression property, Expression value)
		{
			return Expression.Equal(property, value);
		}
	}

	public class NotEqualFilterer : QueryablePropertyFilterer<FilterNotEqualOperator>
	{
		protected override BinaryExpression Filter(Expression property, Expression value)
		{
			return Expression.NotEqual(property, value);
		}
	}

	public class LessThanFilterer : QueryablePropertyFilterer<FilterLessOperator>
	{
		protected override BinaryExpression Filter(Expression property, Expression value)
		{
			// if (property.Type.IsAssignableTo(typeof(IComparable)))
			// return Expression.LessThan(CompareTo(property, value), Expression.Constant(0));

			return Expression.LessThan(property, value);
		}
	}

	public class LessThanOrEqualFilterer : QueryablePropertyFilterer<FilterLessOrEqualOperator>
	{
		protected override BinaryExpression Filter(Expression property, Expression value)
		{
			// if (property.Type.IsAssignableTo(typeof(IComparable)))
			// return Expression.LessThanOrEqual(CompareTo(property, value), Expression.Constant(0));

			return Expression.LessThanOrEqual(property, value);
		}
	}

	public class GreaterThanFilterer : QueryablePropertyFilterer<FilterGreaterOperator>
	{
		protected override BinaryExpression Filter(Expression property, Expression value)
		{
			// if (property.Type.IsAssignableTo(typeof(IComparable)))
			// return Expression.GreaterThan(CompareTo(property, value), Expression.Constant(0));

			return Expression.GreaterThan(property, value);
		}
	}

	public class GreaterThanOrEqualFilterer : QueryablePropertyFilterer<FilterGreaterOrEqualOperator>
	{
		protected override BinaryExpression Filter(Expression property, Expression value)
		{
			// if (property.Type.IsAssignableTo(typeof(IComparable)))
			// return Expression.GreaterThanOrEqual(CompareTo(property, value), Expression.Constant(0));

			return Expression.GreaterThanOrEqual(property, value);
		}
	}
}