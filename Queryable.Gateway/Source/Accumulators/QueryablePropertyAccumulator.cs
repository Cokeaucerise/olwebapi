using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Core.Provider;

namespace OLWebApi.Queryable.Gateway.Accumulators
{
	public abstract class QueryablePropertyAccumulator<TAccumulator> : IQueryablePropertyAccumulator
	where TAccumulator : IPropertyAccumulator
	{
		public IPropertyValueProvider ValueProvider { get; set; }

		protected QueryablePropertyAccumulator(IPropertyValueProvider valueProvider)
		{
			ValueProvider = valueProvider;
		}

		public bool CanAccumulate(IPropertyAccumulator accumulator) => accumulator.GetType() == typeof(TAccumulator);

		public abstract object Accumulate<T>(IEnumerable<T> values, GroupBy groupBy);

		protected double? AsNumerical<T>(T r, GroupBy groupBy)
		{
			try
			{
				return (ValueProvider.GetPropertyValue(groupBy.Field, r) as IConvertible)?.ToDouble(CultureInfo.CurrentCulture.NumberFormat);
			}
			catch (FormatException)
			{
				return default;
			}
		}
	}

	public class QueryableSumAccumulator : QueryablePropertyAccumulator<SumAccumulator>
	{
		public QueryableSumAccumulator(IPropertyValueProvider valueProvider) : base(valueProvider)
		{ }

		public override object Accumulate<T>(IEnumerable<T> values, GroupBy groupBy)
		{
			return values.Select(v => AsNumerical(v, groupBy)).Sum() ?? default;
		}
	}

	public class QueryableCountAccumulator : QueryablePropertyAccumulator<CountAccumulator>
	{
		public QueryableCountAccumulator(IPropertyValueProvider valueProvider) : base(valueProvider)
		{ }

		public override object Accumulate<T>(IEnumerable<T> values, GroupBy groupBy)
		{
			return values?.Count() ?? default;
		}
	}

	public class QueryableMaxAccumulator : QueryablePropertyAccumulator<MaxAccumulator>
	{
		public QueryableMaxAccumulator(IPropertyValueProvider valueProvider) : base(valueProvider)
		{ }

		public override object Accumulate<T>(IEnumerable<T> values, GroupBy groupBy)
		{
			return values.Select(v => AsNumerical(v, groupBy)).Max() ?? default;
		}
	}

	public class QueryableMinAccumulator : QueryablePropertyAccumulator<MinAccumulator>
	{
		public QueryableMinAccumulator(IPropertyValueProvider valueProvider) : base(valueProvider)
		{ }

		public override object Accumulate<T>(IEnumerable<T> values, GroupBy groupBy)
		{
			return values.Select(v => AsNumerical(v, groupBy)).Min() ?? default;
		}
	}
}
