using System.Collections.Generic;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Queryable.Gateway.Accumulators
{
	public interface IQueryablePropertyAccumulator
	{
		bool CanAccumulate(IPropertyAccumulator filter);
		object Accumulate<T>(IEnumerable<T> values, GroupBy groupBy);
	}
}