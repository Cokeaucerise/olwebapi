using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using OLWebApi.Core.Provider;
using OLWebApi.Crud.Gateway.Queryable;
using OLWebApi.Crud.Gateway.Queryable.Filterers;
using OLWebApi.Queryable.Gateway.Accumulators;

namespace OLWebApi.Crud.Gateway.Queryable
{
	[ExcludeFromCodeCoverage]
	public static class DependencyInjection
	{
		public static IServiceCollection AddQueryableGateway(this IServiceCollection services)
		{
			services.TryAddTransient<IPropertyValueProvider, CaseSensitivePropertyValueProvider>();

			services.TryAddSingleton(typeof(IQueryFilterBuilder<>), typeof(QueryFilterBuilder<>));
			services.TryAddSingleton(typeof(IQueryOrderByBuilder<>), typeof(QueryOrderByBuilder<>));

			services.AddQueryablePropertyFilters();
			services.AddQueryablePropertyAccumulators();

			services.TryAddScoped(typeof(QueryableGatewayServices<>));

			return services;
		}

		private static IServiceCollection AddQueryablePropertyFilters(this IServiceCollection services)
		{
			services.AddTransient<IQueryablePropertyFilterer, EqualFilterer>();
			services.AddTransient<IQueryablePropertyFilterer, NotEqualFilterer>();
			services.AddTransient<IQueryablePropertyFilterer, LessThanFilterer>();
			services.AddTransient<IQueryablePropertyFilterer, LessThanOrEqualFilterer>();
			services.AddTransient<IQueryablePropertyFilterer, GreaterThanFilterer>();
			services.AddTransient<IQueryablePropertyFilterer, GreaterThanOrEqualFilterer>();

			return services;
		}

		private static IServiceCollection AddQueryablePropertyAccumulators(this IServiceCollection services)
		{
			services.AddTransient<IQueryablePropertyAccumulator, QueryableSumAccumulator>();
			services.AddTransient<IQueryablePropertyAccumulator, QueryableCountAccumulator>();
			services.AddTransient<IQueryablePropertyAccumulator, QueryableMaxAccumulator>();
			services.AddTransient<IQueryablePropertyAccumulator, QueryableMinAccumulator>();

			return services;
		}
	}
}