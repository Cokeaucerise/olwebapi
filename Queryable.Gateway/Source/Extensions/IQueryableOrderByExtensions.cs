using System.Linq;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Crud.Gateway.Queryable.Extensions
{
	public static class IQueryableOrderByExtensions
	{
		public static IQueryable<T> OrderBy<T>(this IQueryable<T> queryable, IQueryOrderByBuilder<T> builder, OrderBys orderBys)
		where T : IApiRecord
		{
			if (!(orderBys?.Any() ?? false))
				return queryable;

			queryable = queryable.OrderBy(builder, orderBys.FirstOrDefault());

			foreach (var orderBy in orderBys.Skip(1))
				queryable = queryable.ThenOrderBy(builder, orderBy);

			return queryable;
		}

		private static IQueryable<T> OrderBy<T>(this IQueryable<T> queryable, IQueryOrderByBuilder<T> builder, OrderBy orderBy) where T : IApiRecord
		{
			if (builder.Build(orderBy) is var expression)
				queryable = orderBy.Ascending ? queryable.OrderBy(expression) : queryable.OrderByDescending(expression);

			return queryable;
		}

		private static IQueryable<T> ThenOrderBy<T>(this IQueryable<T> queryable, IQueryOrderByBuilder<T> builder, OrderBy orderBy) where T : IApiRecord
		{
			if (queryable is IOrderedQueryable<T> ordered && builder.Build(orderBy) is var expression)
				queryable = orderBy.Ascending ? ordered.ThenBy(expression) : ordered.ThenByDescending(expression);

			return queryable;
		}
	}
}