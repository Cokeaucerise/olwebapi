using System.Collections.Generic;
using System.Linq;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Crud.Gateway.Queryable.Extensions
{
	public static class IQueryablePagingExtensions
	{
		public static IEnumerable<T> Page<T>(this IEnumerable<T> enumerable, Paging paging)
		{
			return enumerable
				.Skip(paging?.Skip ?? default)
				.Take(paging?.Take ?? int.MaxValue);
		}
	}
}