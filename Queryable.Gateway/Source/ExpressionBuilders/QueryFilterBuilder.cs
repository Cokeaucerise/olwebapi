using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Core.Entities.Model;
using OLWebApi.Crud.Gateway.Queryable.Filterers;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Crud.Gateway.Queryable
{
	public interface IQueryFilterBuilder<TSource>
	{
		Expression<Func<TSource, bool>> Build(QueryParameter parameter);
	}

	public class QueryFilterBuilder<TSource> : IQueryFilterBuilder<TSource>
	{
		protected IEnumerable<IQueryablePropertyFilterer> PropertyFilterers { get; }

		public QueryFilterBuilder(IEnumerable<IQueryablePropertyFilterer> propertyFilterers)
		{
			PropertyFilterers = propertyFilterers.Reverse();
		}

		public Expression<Func<TSource, bool>> Build(QueryParameter parameter)
		{
			var pe = Expression.Parameter(typeof(TSource), "source");

			var queryExpression = Expression.AndAlso(
				GetIdsExpression(parameter.Ids.ToArray(), pe),
				GetFiltersExpression(parameter.Filters, pe)
			);

			return GetQueryExpression(queryExpression, pe);
		}

		private static Expression<Func<TSource, bool>> GetQueryExpression(Expression filtersExpression, ParameterExpression parameter)
		{
			return Expression.Lambda<Func<TSource, bool>>(filtersExpression, parameter);
		}

		private Expression GetIdsExpression(string[] ids, ParameterExpression parameter)
		{
			if (!ids.Any())
				return Expression.Constant(true);

			PropertyFilters idsFilters = new(ids.Select(id => new PropertyFilter<FilterEqualOperator>(nameof(IApiRecord.Id), id)));

			return CombineFiltersExpression(idsFilters, parameter, Expression.OrElse);
		}

		private Expression GetFiltersExpression(PropertyFilters filters, ParameterExpression parameter)
		{
			return !(filters?.Any() ?? false)
				? Expression.Constant(true)
				: CombineFiltersExpression(filters, parameter, Expression.AndAlso);
		}

		private Expression CombineFiltersExpression(PropertyFilters filters, ParameterExpression parameter, Func<Expression, Expression, Expression> combiner)
		{
			var expressions = FilterExpressions(filters, parameter);
			return expressions.Any() ? expressions.Aggregate(combiner) : Expression.Constant(true);
		}

		private IEnumerable<Expression> FilterExpressions(PropertyFilters filters, ParameterExpression parameter)
		{
			foreach (var filter in filters)
				if (PropertyFilterers.FirstOrDefault(f => f.CanFilter(filter)) is IQueryablePropertyFilterer filterer)
					yield return ApplyFilter(parameter, filter, filterer);
		}

		private static Expression ApplyFilter(ParameterExpression parameter, PropertyFilter filter, IQueryablePropertyFilterer filterer)
		{
			return filterer.Filter<TSource>(filter, parameter);
		}
	}
}