using System;
using System.Linq.Expressions;
using OLWebApi.Query.Entities.Query;

namespace OLWebApi.Crud.Gateway.Queryable
{
	public interface IQueryOrderByBuilder<TSource>
	{
		Expression<Func<TSource, object>> Build(OrderBy orderBy);
	}

	public class QueryOrderByBuilder<TSource> : IQueryOrderByBuilder<TSource>
	{
		public Expression<Func<TSource, object>> Build(OrderBy orderBy)
		{
			var source = Expression.Parameter(typeof(TSource));
			var property = Expression.Convert(Expression.Property(source, orderBy.Target), typeof(object));

			return Expression.Lambda<Func<TSource, object>>(property, source);
		}
	}
}