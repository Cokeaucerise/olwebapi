using System.Collections.Generic;
using System.Linq;
using OLWebApi.Core.Provider;
using OLWebApi.Crud.Gateway.Queryable.Filterers;
using OLWebApi.Query.Gateway;
using OLWebApi.Queryable.Gateway.Accumulators;

namespace OLWebApi.Crud.Gateway.Queryable.Tests
{
	public abstract class QueryableGatewayTest
	{
		protected IQueryable<TestRecord> Source;
		protected IQueryFilterBuilder<TestRecord> FilterBuilder;
		protected IQueryOrderByBuilder<TestRecord> OrderByBuilder;
		protected IEnumerable<IQueryablePropertyFilterer> Filterers;
		protected IEnumerable<IQueryablePropertyAccumulator> Accumulators;
		protected QueryableGatewayServices<TestRecord> GatewayServices => new(FilterBuilder, OrderByBuilder, Accumulators);

		protected IQueryApiGateway<TestRecord> Gateway => new QueryableGateway<TestRecord>(() => Source, GatewayServices);

		protected IPropertyValueProvider ValueProvider = new CaseInsensitivePropertyValueProvider();

		public QueryableGatewayTest()
		{
			Filterers = new IQueryablePropertyFilterer[] {
				new EqualFilterer(),
				new NotEqualFilterer(),
				new LessThanFilterer(),
				new GreaterThanFilterer(),
				new LessThanOrEqualFilterer(),
				new GreaterThanOrEqualFilterer()
			};
			FilterBuilder = new QueryFilterBuilder<TestRecord>(Filterers);

			OrderByBuilder = new QueryOrderByBuilder<TestRecord>();

			Accumulators = new IQueryablePropertyAccumulator[] {
				new QueryableSumAccumulator(new CaseInsensitivePropertyValueProvider()),
				new QueryableCountAccumulator(new CaseInsensitivePropertyValueProvider()),
				new QueryableMaxAccumulator(new CaseInsensitivePropertyValueProvider()),
				new QueryableMinAccumulator(new CaseInsensitivePropertyValueProvider()),
			};
		}
	}
}
