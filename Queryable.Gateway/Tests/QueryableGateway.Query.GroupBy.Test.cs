using System;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Accumulators;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Crud.Gateway.Queryable.Tests
{
	public class QueryableGatewayQueryGroupByTest : QueryableGatewayTest
	{
		[Fact]
		public async Task GroupBy_SingleKeySingleSum_SumOnSimgleProperty()
		{
			Source = new[] {
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "b", Double = 3 },
				new TestRecord { String = "c", Double = 4 },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String) }.ToHashSet(),
					Groups = new[] { new GroupBy("sum", nameof(TestRecord.Double), new SumAccumulator()) }
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String).Distinct().Count(), result.Count);
			Assert.Equal(2d, result.ElementAt(0).Values.First().Value);
			Assert.Equal(3d, result.ElementAt(1).Values.First().Value);
			Assert.Equal(4d, result.ElementAt(2).Values.First().Value);
		}

		[Fact]
		public async Task GroupBy_ManyKeysSingleSum_SumOnManyProperties()
		{
			Source = new[] {
				new TestRecord { String = "a", String2 = "a", Double = 1 },
				new TestRecord { String = "a", String2 = "b", Double = 2 },
				new TestRecord { String = "a", String2 = "a", Double = 3 },
				new TestRecord { String = "a", String2 = "b", Double = 4 },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String), nameof(TestRecord.String2) }.ToHashSet(),
					Groups = new[] { new GroupBy("sum", nameof(TestRecord.Double), new SumAccumulator()) }
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String2).Distinct().Count(), result.Count);
			Assert.Equal(4d, result.ElementAt(0).Values.First().Value);
			Assert.Equal(6d, result.ElementAt(1).Values.First().Value);
		}


		[Fact]
		public async Task GroupBy_ManyKeysManySum_SumManyPropertiesManyProperties()
		{
			Source = new[] {
				new TestRecord { String = "a", String2 = "a", Double = 1, Float = 1 },
				new TestRecord { String = "a", String2 = "b", Double = 2, Float = 2 },
				new TestRecord { String = "a", String2 = "a", Double = 3, Float = 3 },
				new TestRecord { String = "a", String2 = "b", Double = 4, Float = 4 },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String), nameof(TestRecord.String2) }.ToHashSet(),
					Groups = new[] {
						new GroupBy("double", nameof(TestRecord.Double), new SumAccumulator()),
						new GroupBy("float", nameof(TestRecord.Float), new SumAccumulator())
					}
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String2).Distinct().Count(), result.Count);
			Assert.Equal(4d, result.ElementAt(0).Values.First().Value);
			Assert.Equal(4d, result.ElementAt(0).Values.Last().Value);
			Assert.Equal(6d, result.ElementAt(1).Values.First().Value);
			Assert.Equal(6d, result.ElementAt(1).Values.Last().Value);
		}

		[Fact]
		public async Task GroupBy_GroupNonNumerical_ReturnDefaultAsValue()
		{
			Source = new[] {
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "a", Double = 2 },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String) }.ToHashSet(),
					Groups = new[] {
						new GroupBy("string", nameof(TestRecord.String), new SumAccumulator()),
					}
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String).Distinct().Count(), result.Count);
			Assert.Equal(0d, result.ElementAt(0).Values.First().Value);
		}

		[Fact]
		public async Task GroupBy_GroupStringNumerical_ReturnSum()
		{
			Source = new[] {
				new TestRecord { String = "a", String2 = "1.1" },
				new TestRecord { String = "a", String2 = "2.1" },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String) }.ToHashSet(),
					Groups = new[] {
						new GroupBy("double", nameof(TestRecord.String2), new SumAccumulator()),
					}
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String).Distinct().Count(), result.Count);
			Assert.Equal(3.2d, result.ElementAt(0).Values.First().Value);
		}

		[Fact]
		public async Task GroupBy_Count_ReturnCount()
		{
			Source = new[] {
				new TestRecord { String = "a" },
				new TestRecord { String = "a" },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String) }.ToHashSet(),
					Groups = new[] {
						new GroupBy("count", nameof(TestRecord.String), new CountAccumulator()),
					}
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String).Distinct().Count(), result.Count);
			Assert.Equal(2, result.ElementAt(0).Values.First().Value);
		}

		[Fact]
		public async Task GroupBy_Max_ReturnMax()
		{
			Source = new[] {
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "a", Double = 3 },
				new TestRecord { String = "a", Double = 2 },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String) }.ToHashSet(),
					Groups = new[] { new GroupBy("max", nameof(TestRecord.Double), new MaxAccumulator()) }
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String).Distinct().Count(), result.Count);
			Assert.Equal(3d, result.ElementAt(0).Values.First().Value);
		}

		[Fact]
		public async Task GroupBy_Min_ReturnMin()
		{
			Source = new[] {
				new TestRecord { String = "a", Double = 3 },
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "a", Double = 2 },
			}.AsQueryable();
			ReportParameter parameter = new()
			{
				GroupBies = new()
				{
					Keys = new[] { nameof(TestRecord.String) }.ToHashSet(),
					Groups = new[] { new GroupBy("min", nameof(TestRecord.Double), new MinAccumulator()) }
				}
			};

			var result = await Gateway.Report(parameter);

			Assert.Equal(Source.Select(s => s.String).Distinct().Count(), result.Count);
			Assert.Equal(1d, result.ElementAt(0).Values.First().Value);
		}
	}
}
