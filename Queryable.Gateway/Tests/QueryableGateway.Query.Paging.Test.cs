using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Query.Entities.Query;
using OLWebApi.Crud.Gateway.Queryable.Tests;
using Xunit;

namespace OLWebApi.Crud.Gateway.Queryable
{
	public class QueryableGatewayQueryPagingTest : QueryableGatewayTest
	{
		public QueryableGatewayQueryPagingTest()
		{
			Source = new[] {
				new TestRecord { String = "b", Double = 1 },
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "b", Double = 2 },
				new TestRecord { String = "a", Double = 2 },
			}.AsQueryable();
		}

		[Fact]
		public async Task Paging_Take1_ReturnFirst()
		{
			QueryParameter parameter = new()
			{
				Paging = new(0, 1)
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Single(result);
		}

		[Fact]
		public async Task Paging_Skip1_ReturnAllExceptFirst()
		{
			QueryParameter parameter = new()
			{
				Paging = new() { Skip = 1 }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(3, result.Count);
			Assert.Equal(new TestRecord { String = "a", Double = 1 }, result.First());
		}

		[Fact]
		public async Task Paging_Skip1Take1_ReturnSecondOnly()
		{
			QueryParameter parameter = new()
			{
				Paging = new(1, 1)
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Single(result);
			Assert.Equal(new TestRecord { String = "a", Double = 1 }, result.First());
		}
	}
}