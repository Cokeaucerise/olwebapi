using OLWebApi.Core.Entities.Model;

namespace OLWebApi.Crud.Gateway.Queryable.Tests
{
	public record TestRecord : ApiRecord
	{
		public string String { get; set; }
		public string String2 { get; set; }
		public double Double { get; set; }
		public float Float { get; set; }
	}
}
