using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Core.Entities.Filters;
using OLWebApi.Crud.Gateway.Queryable.Tests;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Crud.Gateway.Queryable
{
	public class QueryableGatewayQueryFilterTest : QueryableGatewayTest
	{
		public QueryableGatewayQueryFilterTest()
		{
			Source = new[] {
				new TestRecord { String = "b", Double = 1 },
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "b", Double = 2 },
				new TestRecord { String = "a", Double = 2 },
			}.AsQueryable();
		}

		// [Fact]
		// public async Task Filters_StringEqual_ReturnMatchs()
		// {
		// 	QueryParameter parameter = new()
		// 	{
		// 		Filters = new PropertyFilters { new PropertyFilter<FilterEqualOperator>(nameof(TestRecord.String), "a") }
		// 	};

		// 	var result = await Gateway.Query(parameter).ToListAsync();

		// 	Assert.Equal(2, result.Count);
		// 	Assert.Equal(new[] { "a", "a" }, result.Select(r => r.String));
		// }

		// [Fact]
		// public async Task Filters_StringNotEqual_ReturnMatchs()
		// {
		// 	QueryParameter parameter = new()
		// 	{
		// 		Filters = new PropertyFilters { new PropertyFilter<FilterNotEqualOperator>(nameof(TestRecord.String), "a") }
		// 	};

		// 	var result = await Gateway.Query(parameter).ToListAsync();

		// 	Assert.Equal(2, result.Count);
		// 	Assert.Equal(new[] { "b", "b" }, result.Select(r => r.String));
		// }

		// [Fact]
		// public async Task Filters_StringLessThan_ReturnMatchs()
		// {
		// 	QueryParameter parameter = new()
		// 	{
		// 		Filters = new PropertyFilters { new PropertyFilter<FilterLessOperator>(nameof(TestRecord.String), "b") }
		// 	};

		// 	var result = await Gateway.Query(parameter).ToListAsync();

		// 	Assert.Equal(2, result.Count);
		// 	Assert.Equal(new[] { "a", "a" }, result.Select(r => r.String));
		// }

		// [Fact]
		// public async Task Filters_StringLessThanOrEqual_ReturnMatchs()
		// {
		// 	QueryParameter parameter = new()
		// 	{
		// 		Filters = new PropertyFilters { new PropertyFilter<FilterLessOrEqualOperator>(nameof(TestRecord.String), "b") }
		// 	};

		// 	var result = await Gateway.Query(parameter).ToListAsync();

		// 	Assert.Equal(Source.Count(), result.Count);
		// }

		// [Fact]
		// public async Task Filters_StringGreaterThan_ReturnMatchs()
		// {
		// 	QueryParameter parameter = new()
		// 	{
		// 		Filters = new PropertyFilters { new PropertyFilter<FilterGreaterOperator>(nameof(TestRecord.String), "a") }
		// 	};

		// 	var result = await Gateway.Query(parameter).ToListAsync();

		// 	Assert.Equal(2, result.Count);
		// 	Assert.Equal(new[] { "b", "b" }, result.Select(r => r.String));
		// }

		// [Fact]
		// public async Task Filters_StringGreaterThanOrEqual_ReturnMatchs()
		// {
		// 	QueryParameter parameter = new()
		// 	{
		// 		Filters = new PropertyFilters { new PropertyFilter<FilterGreaterOrEqualOperator>(nameof(TestRecord.String), "a") }
		// 	};

		// 	var result = await Gateway.Query(parameter).ToListAsync();

		// 	Assert.Equal(Source.Count(), result.Count);
		// }

		[Fact]
		public async Task Filters_NumericalEqual_ReturnMatchs()
		{
			QueryParameter parameter = new()
			{
				Filters = new PropertyFilters { new PropertyFilter<FilterEqualOperator>(nameof(TestRecord.Double), 1d.ToString()) }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(2, result.Count);
			Assert.Equal(new[] { 1d, 1d }, result.Select(r => r.Double));
		}

		[Fact]
		public async Task Filters_NumericalNotEqual_ReturnMatchs()
		{
			QueryParameter parameter = new()
			{
				Filters = new PropertyFilters { new PropertyFilter<FilterNotEqualOperator>(nameof(TestRecord.Double), 1d.ToString()) }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(2, result.Count);
			Assert.Equal(new[] { 2d, 2d }, result.Select(r => r.Double));
		}

		[Fact]
		public async Task Filters_NumericalGreater_ReturnMatchs()
		{
			QueryParameter parameter = new()
			{
				Filters = new PropertyFilters { new PropertyFilter<FilterGreaterOperator>(nameof(TestRecord.Double), 1d.ToString()) }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(2, result.Count);
			Assert.Equal(new[] { 2d, 2d }, result.Select(r => r.Double));
		}

		[Fact]
		public async Task Filters_NumericalGreaterOrEqual_ReturnMatchs()
		{
			QueryParameter parameter = new()
			{
				Filters = new PropertyFilters { new PropertyFilter<FilterGreaterOrEqualOperator>(nameof(TestRecord.Double), 1d.ToString()) }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(Source.Count(), result.Count);
		}

		[Fact]
		public async Task Filters_NumericalLess_ReturnMatchs()
		{
			QueryParameter parameter = new()
			{
				Filters = new PropertyFilters { new PropertyFilter<FilterLessOperator>(nameof(TestRecord.Double), 2d.ToString()) }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(2, result.Count);
			Assert.Equal(new[] { 1d, 1d }, result.Select(r => r.Double));
		}

		[Fact]
		public async Task Filters_NumericalLessOrEqual_ReturnMatchs()
		{
			QueryParameter parameter = new()
			{
				Filters = new PropertyFilters { new PropertyFilter<FilterLessOrEqualOperator>(nameof(TestRecord.Double), 2d.ToString()) }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(Source.Count(), result.Count);
		}
	}
}