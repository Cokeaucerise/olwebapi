using System;
using System.Linq;
using System.Threading.Tasks;
using OLWebApi.Query.Entities.Query;
using Xunit;

namespace OLWebApi.Crud.Gateway.Queryable.Tests
{
	public class QueryableGatewayQueryOrderByTest : QueryableGatewayTest
	{
		[Fact]
		public async Task OrderBy_ManyPropertiesMixed_OrderedAlpha()
		{
			Source = new[] {
				new TestRecord { String = "b", Double = 1 },
				new TestRecord { String = "a", Double = 1 },
				new TestRecord { String = "b", Double = 2 },
				new TestRecord { String = "a", Double = 2 },
			}.AsQueryable();
			QueryParameter parameter = new()
			{
				OrderBys = new()
				{
					new() { Target = nameof(TestRecord.String), Ascending = true },
					new() { Target = nameof(TestRecord.Double), Ascending = false },
				}
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(result.Select(r => r.String), new[] { "a", "a", "b", "b" });
			Assert.Equal(result.Select(r => r.Double), new[] { 2d, 1d, 2d, 1d });
		}


		[Fact]
		public async Task OrderBy_SingleNullProperty_OrderedAlpha()
		{
			Source = new[] { new TestRecord { String = "b" }, new TestRecord { String = null } }.AsQueryable();
			QueryParameter parameter = new()
			{
				OrderBys = new() { new() { Target = nameof(TestRecord.String), Ascending = true } }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(result.Select(r => r.String), new string[] { null, "b" });
		}

		[Fact]
		public async Task OrderBy_SingleNumericalPropertyAscending_OrderedNumerical()
		{
			Source = new[] { new TestRecord { Double = 1.1d }, new TestRecord { Double = 0.1d } }.AsQueryable();
			QueryParameter parameter = new()
			{
				OrderBys = new() { new() { Target = nameof(TestRecord.Double), Ascending = true } }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(result.Select(r => r.Double), new[] { 0.1d, 1.1d });
		}

		[Fact]
		public async Task OrderBy_SingleNumericalPropertyDescending_OrderedNumerical()
		{
			Source = new[] { new TestRecord { Double = 0.1d }, new TestRecord { Double = 1.1d } }.AsQueryable();
			QueryParameter parameter = new()
			{
				OrderBys = new() { new() { Target = nameof(TestRecord.Double), Ascending = false } }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(result.Select(r => r.Double), new[] { 1.1d, 0.1d });
		}

		[Fact]
		public async Task OrderBy_SingleStringPropertyAscending_OrderedAlpha()
		{
			Source = new[] { new TestRecord { String = "b" }, new TestRecord { String = "a" } }.AsQueryable();
			QueryParameter parameter = new()
			{
				OrderBys = new() { new() { Target = nameof(TestRecord.String), Ascending = true } }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(result.Select(r => r.String), new string[] { "a", "b" });
		}

		[Fact]
		public async Task OrderBy_SingleStringPropertyDescending_OrderedAlpha()
		{
			Source = new[] { new TestRecord { String = "a" }, new TestRecord { String = "b" } }.AsQueryable();
			QueryParameter parameter = new()
			{
				OrderBys = new() { new() { Target = nameof(TestRecord.String), Ascending = false } }
			};

			var result = await Gateway.Query(parameter).ToListAsync();

			Assert.Equal(result.Select(r => r.String), new string[] { "b", "a" });
		}
	}
}
